﻿using Incodix.Wedel.SPRuchMatTow.Classes;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.WebControls;
using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Incodix.Wedel.SPRuchMatTow.ControlTemplates.Incodix.Wedel.SPRuchMatTow
{
    public partial class SPRuchMatTowPrintForm : UserControl
    {
        #region Properties

        static Logger LogWriter = Logger.Create("kategoria", "SPRuchMatTowPrintForm");
        static LogLevel lvlVerbose = LogLevel.Verbose;
        static SPDiagnosticsCategory categVerbose = new SPDiagnosticsCategory("SPRuchMatTowPrintForm", TraceSeverity.VerboseEx, EventSeverity.Verbose);
        static LogLevel lvlUnexp = LogLevel.Unexpected;
        static SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("SPRuchMatTowPrintForm", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        private int DocID;

        #endregion

        #region ProtectedMethods

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!Page.IsPostBack)
            {
                SPWeb currentWeb = SPContext.Current.Web;
                // Add CSS url to page
                SPRuchMatTowFormCSS.Attributes["href"] = currentWeb.Url + "/_layouts/15/Incodix.Wedel.SPRuchMatTow/SPRuchMatTowForm.css";
                bootstrapCSS.Attributes["href"] = currentWeb.Url + "/_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.css";
                responsiveCSS.Attributes["href"] = currentWeb.Url + "/_layouts/15/Incodix.Wedel.SPRuchMatTow/responsive.css";
                SPRuchMatTowRibbonScriptsPL.Name = currentWeb.Url + "/_layouts/15/Incodix.Wedel.SPRuchMatTow/SPRuchMatTowRibbonScriptsPL.js";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DocID = int.Parse(Page.Request.QueryString["DocID"].ToString());
                SPWeb currentWeb = SPContext.Current.Web;
                SPListItem currentItem = currentWeb.GetList(currentWeb.Url + "/Lists/GoodsMovementList").GetItemById(DocID);

                DateTime _createdDate = DateTime.Parse(currentItem[SPBuiltInFieldId.Created].ToString());
                fldCreated.InnerText = _createdDate.ToString("yyyy-MM-dd hh:mm");

                if (currentItem.Fields.ContainsField("CategoryLookup") && currentItem["CategoryLookup"] != null)
                {
                    SPFieldLookupValue userValue = new SPFieldLookupValue(currentItem["CategoryLookup"].ToString());
                    fldCategoryLookup.InnerText = userValue.LookupValue;
                }
                if (currentItem.Fields.ContainsField("Author") && currentItem["Author"] != null)
                {
                    SPFieldUserValue userValue = new SPFieldUserValue(SPContext.Current.Web, currentItem["Author"].ToString());
                    fldRequestor.InnerText = userValue.LookupValue;
                }
                if (currentItem.Fields.ContainsField("RequestFor") && currentItem["RequestFor"] != null)
                {
                    fldRequestFor.InnerText = currentItem["RequestFor"].ToString();
                }
                if (currentItem.Fields.ContainsField("RequestState") && currentItem["RequestState"] != null)
                {
                    fldRequestState.InnerText = currentItem["RequestState"].ToString();
                }
                if (currentItem.Fields.ContainsField("ApprovalDate") && currentItem["ApprovalDate"] != null)
                {
                    DateTime _approvalDate = DateTime.Parse(currentItem["ApprovalDate"].ToString());
                    fldApprovalDate.InnerText = _approvalDate.ToString("yyyy-MM-dd hh:mm");
                }
                else
                {
                    lblApprovalDate.Visible = false;
                    fldApprovalDate.Visible = false;
                }
                if (currentItem.Fields.ContainsField("Signature") && currentItem["Signature"] != null)
                {
                    lblSignature.InnerHtml = currentItem["Signature"].ToString();
                }
                if (currentItem.Fields.ContainsField("Approver") && currentItem["Approver"] != null)
                {
                    SPFieldUserValue _ApproverValue = new SPFieldUserValue(SPContext.Current.Web, currentItem["Approver"].ToString());
                    fldApprover.InnerText = _ApproverValue.LookupValue;
                }
                if (currentItem.Fields.ContainsField("RequestorVehicle") && currentItem["RequestorVehicle"] != null)
                {
                    fldRequestorVehicle.InnerText = currentItem["RequestorVehicle"].ToString();
                }
                if (currentItem.Fields.ContainsField("PurposeOfGoodsMovement") && currentItem["PurposeOfGoodsMovement"] != null)
                {
                    fldPurposeOfGoodsMovement.Text = currentItem["PurposeOfGoodsMovement"].ToString();
                }
                if (currentItem.Fields.ContainsField("ToReturn"))
                {
                    if (bool.Parse(currentItem["ToReturn"].ToString()))
                    {
                        fldToReturn.InnerText = "Tak";
                    }
                    else
                    {
                        fldToReturn.InnerText = "Nie";
                    }
                }
                // Prepare Requested Grid View
                try
                {
                    RequestedGoodsGrid.DataSource = RequestedGoodsGridData(SPContext.Current.Web, currentItem);
                    RequestedGoodsGrid.DataBind();
                    RequestedGoodsGrid.Rows[0].Visible = false;
                    RequestedGoodsGrid.Enabled = false;
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Unable to generate RequestedGoods Table for form with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
                }
            }
        }

        #endregion

        #region GridMethods

        protected DataTable RequestedGoodsGridNoData()
        {
            DataTable dt = new DataTable();
            DataColumn ObjectiveIdRequestedMaterialId = new DataColumn();
            ObjectiveIdRequestedMaterialId.ColumnName = "RequestedMaterialId";
            ObjectiveIdRequestedMaterialId.DataType = System.Type.GetType("System.Int32");
            ObjectiveIdRequestedMaterialId.AutoIncrement = true;
            ObjectiveIdRequestedMaterialId.AutoIncrementSeed = 1;
            ObjectiveIdRequestedMaterialId.AutoIncrementStep = 1;
            ObjectiveIdRequestedMaterialId.ReadOnly = true;
            ObjectiveIdRequestedMaterialId.Unique = true;
            dt.Columns.Add(ObjectiveIdRequestedMaterialId);
            dt.Columns.Add("MaterialName", typeof(string));
            dt.Columns.Add("UnitOfMeasure", typeof(string));
            dt.Columns.Add("Quantity", typeof(string));
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            LogWriter.Log("SPRuchMatTowPrintForm prepared DataTable draft for RequestedGoods DT", lvlVerbose, categVerbose);
            return dt;
        }
        protected DataTable RequestedGoodsGridData(SPWeb currentWeb, SPListItem currentItem)
        {
            DataTable dt = RequestedGoodsGridNoData();
            if (currentItem.Fields.ContainsField("RequestedGoods") && currentItem["RequestedGoods"] != null)
            {
                XDocument requestedGoodsXML = XDocument.Parse(currentItem["RequestedGoods"].ToString());
                var booksQuery = from books in requestedGoodsXML.Elements("NewDataSet").Elements("Table1")
                                 select books;

                dt = booksQuery.ToDataTable();
                LogWriter.Log("SPRuchMatTowPrintForm get currentItem XML data for RequestedGoods DT", lvlVerbose, categVerbose);
            }           
            return dt;
        }
        protected DataTable RequestedGoodsGridAddedData()
        {
            DataTable dt = RequestedGoodsGridNoData();
            DataRow dr;
            GridViewRowCollection rows = RequestedGoodsGrid.Rows;
            foreach (GridViewRow row in rows)
            {
                if (row.RowType != DataControlRowType.Header || row.RowType != DataControlRowType.Footer || row.RowType != DataControlRowType.EmptyDataRow)
                {
                    Label lblMaterialName = (Label)row.FindControl("lblMaterialName");
                    Label lblUnitOfMeasure = (Label)row.FindControl("lblUnitOfMeasure");
                    Label lblQuantity = (Label)row.FindControl("lblQuantity");

                    if (!string.IsNullOrEmpty(lblMaterialName.Text))
                    {
                        dr = dt.NewRow();
                        dr["MaterialName"] = lblMaterialName.Text;
                        dr["UnitOfMeasure"] = lblUnitOfMeasure.Text;
                        dr["Quantity"] = lblQuantity.Text;
                        dt.Rows.Add(dr);
                    }
                }
            }
            LogWriter.Log("SPRuchMatTowPrintForm got updated DataTable for RequestedGoods save process", lvlVerbose, categVerbose);
            return dt;
        }
        //protected void RequestedGoodsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    SPWeb currentWeb = SPContext.Current.Web;
        //    int _FormID = SPContext.Current.ListItem.ID;
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        try
        //        {
        //            Label lblMaterialName = (Label)e.Row.FindControl("lblMaterialName");
        //            lblMaterialName.

        //        }
        //        catch (Exception ex)
        //        {
        //            LogWriter.Log("Unable to fill in field label fields for form with ID " + _FormID + " due to: " + ex.Message, lvlUnexp, categUnexp);
        //        }
        //    }
        //}
        #endregion
    }
}
