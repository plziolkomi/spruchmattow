﻿using Incodix.Wedel.SPRuchMatTow.Classes;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Incodix.Wedel.SPRuchMatTow.ControlTemplates.Incodix.Wedel.SPRuchMatTow
{
    public partial class PerformAction : UserControl, IDialogBox
    {
        #region Properties
        private static uint language;
        static Logger LogWriter = Logger.Create("kategoria", "PerformAction");
        static LogLevel lvlVerbose = LogLevel.Verbose;
        static SPDiagnosticsCategory categVerbose = new SPDiagnosticsCategory("PerformAction", TraceSeverity.VerboseEx, EventSeverity.Verbose);
        static LogLevel lvlUnexp = LogLevel.Unexpected;
        static SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("PerformAction", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        private bool hasPermissions = false;
        private int DocID;
        private string FormAction;

        public event AddEventHandlerToSPDialogEvent ResultOk;

        #endregion

        #region ProtectedMethods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            DocID = int.Parse(Page.Request.QueryString["DocID"].ToString());
            SPWeb currentWeb = SPContext.Current.Web;
            SPListItem currentItem = currentWeb.GetList(currentWeb.Url + "/Lists/GoodsMovementList").GetItemById(DocID);
            hasPermissions = currentItem.DoesUserHavePermissions(SPContext.Current.Web.CurrentUser, SPBasePermissions.FullMask);
            LogWriter.Log("PerformAction UserControl verified current user priviledges", lvlVerbose, categVerbose);
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            SPWeb currentWeb = SPContext.Current.Web;
            // Add CSS url to page
            SPRuchMatTowFormCSS.Attributes["href"] = currentWeb.Url + "/_layouts/15/Incodix.Wedel.SPRuchMatTow/SPRuchMatTowForm.css";
            bootstrapCSS.Attributes["href"] = currentWeb.Url + "/_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.css";
            LogWriter.Log("PerformAction UserControl updated CSS Links", lvlVerbose, categVerbose);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SPWeb currentWeb = SPContext.Current.Web;
            language = currentWeb != null ? currentWeb.Language : 1045;
            DocID = int.Parse(Page.Request.QueryString["DocID"].ToString());
            FormAction = Page.Request.QueryString["FormAction"].ToString();
            LogWriter.Log("PerformAction UserControl obtained parameters from URL: DocID:" + DocID + "; FormAction: " + FormAction, lvlVerbose, categVerbose);

            string currentFormState = string.Empty;
            string userMessage = string.Empty;
            SPListItem currentItem = currentWeb.GetList(currentWeb.Url + "/Lists/GoodsMovementList").GetItemById(DocID);

            // Get form state
            if (currentItem.Fields.ContainsField("RequestState") && currentItem["RequestState"] != null)
            {
                currentFormState = currentItem["RequestState"].ToString();
                LogWriter.Log("PerformAction UserControl verified form status", lvlVerbose, categVerbose);
                switch (FormAction)
                {
                    case "SendForApproval":
                        userMessage = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,SendForApprovalPrompt", null, language);
                        break;
                    case "Approve":
                        userMessage = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ApprovePrompt", null, language);
                        break;
                    case "Reject":
                        userMessage = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RejectPrompt", null, language);
                        ConfirmButton.Text = "Odrzuć";
                        break;
                    case "ConfirmReleaseWithoutApproval":
                        userMessage = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ConfirmReleaseWithoutApprovalPrompt", null, language);
                        break;
                    case "ConfirmRelease":
                        userMessage = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ConfirmReleasePrompt", null, language);
                        ConfirmButton.Text = "Potwierdź";
                        break;
                    case "ConfirmReturn":
                        userMessage = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ConfirmReturnPrompt", null, language);
                        ConfirmButton.Text = "Potwierdź";
                        break;
                }
                AdditionalDataLabel.InnerText = userMessage;
                LogWriter.Log("PerformAction UserControl displayed prompt to user", lvlVerbose, categVerbose);
            }
        }

        protected void ConfirmButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool result = SaveForm_Click();
                if (result)
                {
                    ResultOk(this, new SPDialogEventHandler(1, "Akcja zrealizowana. Odśwież stronę"));
                }
                else
                {
                    ResultOk(this, new SPDialogEventHandler(0, ""));
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to save Action Form due to " + ex.Message, lvlUnexp, categUnexp);
                lblErrorMessage.Text = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,PerformActionSaveError", null, language);
            }
        }
        protected void AbortButton_Click(object sender, EventArgs e)
        {
            try
            {
                int dialogResult = 2;
                if (this.ResultOk != null)
                {
                    ResultOk(this, new SPDialogEventHandler(dialogResult, SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,PerformActionCancelMessage", null, language)));
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("PerformAction UC is unable to close the dialogBox due to: " + ex.Message, lvlUnexp, categUnexp);
            }
        }
        
        #endregion

        #region PrivateMethods

        private bool SaveForm_Click()
        {
            bool result = false;
            try
            {
                LogWriter.Log("PerformAction UserControl is starting workflow action confirmation", lvlVerbose, categVerbose);
                SPWeb currentWeb = SPContext.Current.Web;
                SPFieldUserValue currUserVal = new SPFieldUserValue(currentWeb, currentWeb.CurrentUser.ID, currentWeb.CurrentUser.Name);

                using (SPSite elevatedSite = new SPSite(currentWeb.Url, SPUserToken.SystemAccount))
                {
                    using (SPWeb elevatedWeb = elevatedSite.OpenWeb())
                    {
                        PerformActionUtility actionUtility = new PerformActionUtility();
                        SPListItem currentItem = elevatedWeb.GetList(elevatedWeb.Url + "/Lists/GoodsMovementList").GetItemById(DocID);
                        // Get form state and move to next action
                        if (currentItem.Fields.ContainsField("RequestState") && currentItem["RequestState"] != null)
                        {
                            actionUtility.PerformWorkflowAction(currentWeb, elevatedWeb, currUserVal, currentItem, FormAction, fldAdditionalComments.Value);
                            elevatedWeb.AllowUnsafeUpdates = true;
                            currentItem.Update();
                            elevatedWeb.AllowUnsafeUpdates = false;
                            result = true;
                            LogWriter.Log("PerformAction UserControl updated workflow status of form with ID " + currentItem.ID, lvlVerbose, categVerbose);
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("PerformAction UserControl did not update workflow status of form with ID " + DocID + " due to " + ex.Message, lvlUnexp, categUnexp);
            }
            return result;
        }

        #endregion
    }
}
