﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SPRuchMatTowPrintForm.ascx.cs" Inherits="Incodix.Wedel.SPRuchMatTow.ControlTemplates.Incodix.Wedel.SPRuchMatTow.SPRuchMatTowPrintForm" %>
<link runat="server" id="SPRuchMatTowFormCSS" href="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/SPRuchMatTowForm.css" rel="stylesheet" type="text/css" />
<link runat="server" id="bootstrapCSS" href="/_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link runat="server" id="responsiveCSS" href="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/responsive.css" rel="stylesheet" type="text/css" />

<script src="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/jquery.js"></script>
<SharePoint:ScriptLink ID="SPRuchMatTowRibbonScriptsPL" Name="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/SPRuchMatTowRibbonScriptsPL.js" runat="server" />
<script src="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        ResizeTextAreas();

        if (navigator.appName == 'Microsoft Internet Explorer') {
            window.print();
        }
    });
</script>
<section id="printFormMain">
    <div id="form" class="container">
        <!-- Form Header -->
        <div class="row">
            <div class="col-sm-10">
                <div class="section-title page-header">
                    <h2>Przepustka Materiałowo-Towarowa</h2>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="section-signature page-header text-right">
                    <h2><span id="lblSignature" runat="server" class="label label-default"> - </span></h2>
                </div>
            </div>
        </div>
        <!-- 1st Row -->
        <div class="row">
            <div class="col-sm-2">
                <label id="lblCreated" runat="server" for="fldCreated">Data utworzenia:</label>
            </div>
            <div class="col-sm-3">
                <label id="fldCreated" runat="server" class="form-control formControlExtended"></label>
            </div>
            <div class="col-sm-2 col-sm-offset-1">
                <label id="lblCategoryLookup" runat="server" for="fldCategoryLookup">Kategoria:</label>
            </div>
            <div class="col-sm-4">
                <label id="fldCategoryLookup" runat="server" class="form-control formControlExtended" />
            </div>
        </div>
        <!-- 2nd Row -->
        <div class="row">
            <div class="col-sm-2">
                <label id="lblRequestor" runat="server" for="fldRequestor">Wnioskodawca:</label>
            </div>
            <div class="col-sm-3">
                <label id="fldRequestor" runat="server" class="form-control formControlExtended"></label>
            </div>
            <div class="col-sm-2 col-sm-offset-1">
                <label id="lblApprover" runat="server" for="fldApprover">Akceptujący:</label>
            </div>
            <div class="col-sm-4">
                <label id="fldApprover" runat="server" class="form-control formControlExtended"></label>
            </div>
        </div>
            <!-- 3ard Row -->
        <div class="row">
            <div class="col-sm-2">
                <label id="lblRequestFor" runat="server" for="fldRequestFor">Przepustka dla:</label>
            </div>
            <div class="col-sm-3 RequestFor">
                <label id="fldRequestFor" runat="server" class="form-control RequestFor formControlExtended" />
            </div>
            <div class="col-sm-2 col-sm-offset-1">
                <label id="lblToReturn" runat="server">Do ponownego wniesienia/zwrotu</label>
            </div>
            <div class="col-sm-4">
                <label id="fldToReturn" runat="server" class="form-control ToReturn formControlExtended" />
            </div>
        </div>
        <!-- 3brd Row -->
        <div class="row">
            <div class="col-sm-2">
                <label id="lblRequestorVehicle" runat="server" for="fldRequestorVehicle">Samochód (marka/nr rej.):</label>
            </div>
            <div class="col-sm-3 RequestorVehicle">
                <label id="fldRequestorVehicle" runat="server" class="form-control formControlExtended" />
            </div>
        </div>
        <!-- 4st Row -->
        <div class="row">
            <div class="col-sm-12">
                <label id="lblRequestedGoods" runat="server">Wnioskuję o wyniesienie/wywiezienie następujących przedmiotów:</label>
            </div>
        </div>
        <!-- 5st Row -->
        <div class="row">
            <div class="col-sm-12 center-block">
                <asp:UpdatePanel ID="RequestedGoodsPanel" runat="server" UpdateMode="Conditional" EnableViewState="true">
                    <ContentTemplate>
                        <asp:GridView ID="RequestedGoodsGrid" runat="server" AutoGenerateColumns="False" DataKeyNames="RequestedMaterialId" ShowFooter="false" EnableModelValidation="True"
                            CssClass="table table-striped table-bordered table-hover table-condensed RequestedGoodsGrid" Width="100%">
                            <HeaderStyle />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5>
                                            <asp:Label runat="server" ID="hdrMaterialName" Text="Nazwa" CssClass="ms-bold" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblMaterialName" runat="server" CssClass="MaterialName formControlExtended form-control" BorderStyle="None" Text=''>
                                        <%#Eval("MaterialName") %>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="60%" />
                                    <FooterStyle Width="60%" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5>
                                            <asp:Label runat="server" ID="hdrUnitOfMeasure" Text="j.m." CssClass="ms-bold" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUnitOfMeasure" CssClass="form-control GridControl formControlExtended" runat="server" Text='<%#Eval("UnitOfMeasure") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="20%" />
                                    <FooterStyle Width="20%" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5>
                                            <asp:Label runat="server" ID="hdrQuantity" Text="Ilość" CssClass="ms-bold" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblQuantity" runat="server" CssClass="Quantity form-control GridControl formControlExtended" BorderStyle="None" Text='<%#Eval("Quantity") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="20%" />
                                    <FooterStyle Width="20%" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <br />
        <!-- 6st Row -->
        <div class="row">
            <div class="col-sm-4">
                <label id="lblPurposeOfGoodsMovement" runat="server" for="fldPurposeOfGoodsMovement">Cel wyniesienia/wywiezienia:</label>
            </div>
            <div class="col-sm-8 PurposeOfGoodsMovement">
                <asp:Label ID="fldPurposeOfGoodsMovement" runat="server" CssClass="formControlExtended form-control" />
            </div>
        </div>
        <br />
        <!-- 8nd Row -->
        <div class="row">
            <div class="col-sm-2">
                <label id="lblRequestState" runat="server" for="fldRequestState">Status wniosku:</label>
            </div>
            <div class="col-sm-4">
                <label id="fldRequestState" runat="server" class="form-control formControlExtended"></label>
            </div>
            <div class="col-sm-2 col-sm-offset-1">
                <label id="lblApprovalDate" runat="server" for="fldApprovalDate">Data zatwierdzenia:</label>
            </div>
            <div class="col-sm-3">
                <label id="fldApprovalDate" runat="server" class="form-control formControlExtended"></label>
            </div>
        </div>
    </div>
</section>
<footer id="printFormFooter">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <label id="lblfooterLabel">Wydruk z modułu przepustek materiałowo-towarowych, LOTTE Wedel</label>
            </div>
        </div>
    </div>
</footer>
