﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PerformAction.ascx.cs" Inherits="Incodix.Wedel.SPRuchMatTow.ControlTemplates.Incodix.Wedel.SPRuchMatTow.PerformAction" %>
<link runat="server" id="SPRuchMatTowFormCSS" href="../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/SPRuchMatTowForm.css" rel="stylesheet" type="text/css" />
<link runat="server" id="bootstrapCSS" href="../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.css" rel="stylesheet" type="text/css" />
<div id="formContainer"  class="form-group">
<table style="width: 100%">
    <tr class="additionalDataRow">
        <td colspan="4">
            <strong><label id="AdditionalDataLabel" runat="server"></label>
        </strong>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <br />
        </td>
    </tr>
    <tr class="additionalDataRow">
        <td class="dialogFormCenter" style="font-size:18px" runat="server" id="Td1" colspan="4">
            <textarea class="form-control fldAdditionalComments" id="fldAdditionalComments" runat="server" rows="4" cols="10" placeholder="Dodatkowy komentarz ..."></textarea>
            <br />
        </td>
    </tr>
    <tr>
    <td class="dialogFormCenter" colspan="2">
        <asp:Button ID="AbortButton" runat="server" Text="Anuluj" OnClick="AbortButton_Click" 
            style="font-weight: 700;font-size:14px" Width="140px" CssClass="btn btn-default" />
    </td>
    <td class="dialogFormCenter" colspan="2">
        <asp:Button ID="ConfirmButton" runat="server" Text="Zatwierdź" OnClick="ConfirmButton_Click" 
            style="font-weight: 700;font-size:14px" Width="140px" CssClass="btn btn-success" />
    </td>
    </tr>
    </table>
</div>
<div>
    <div>
        <asp:Label ID="lblErrorMessage" runat="server" CssClass="ErrorMessageBig"></asp:Label>
    </div>
</div>