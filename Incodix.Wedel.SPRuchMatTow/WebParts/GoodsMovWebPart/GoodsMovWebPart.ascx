﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoodsMovWebPart.ascx.cs" Inherits="Incodix.Wedel.SPRuchMatTow.WebParts.GoodsMovWebPart.GoodsMovWebPart" %>
<link href="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.css" rel="stylesheet" type="text/css"  />
<link rel="stylesheet" type="text/css" href="../../_layouts/15/Incodix.Wedel.SPRuchMatTow/SPRuchMatTowForm.css" />
<link type="text/css" href="../../_layouts/15/Incodix.Wedel.SPRuchMatTow/responsive.css" rel="stylesheet" />
<SharePoint:ScriptLink ID="SPRuchMatTowFormScripts" Name="~sitecollection/AppAssets/SPRuchMatTowFormScriptsPL.js" runat="server" />
<script src="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        HideDefaultWebPartForm();

        InjectBootstrapStyles();

        getApproversPresence();

        InsertScriptToDropDown();

        InjectCurrentUser();

        ResizeTextAreas();

        $('div.fldApproverProgressBar').hide();

    });
</script>
<div id="form" class="container form-group GoodsMovWebPartForm">
    <!-- Form Header -->
    <div class="row">
        <div class="col-md-10">
            <div class="section-title page-header">
                <h2>Przepustka Materiałowo-Towarowa</h2>
            </div>
        </div>
        <div class="col-md-2">
            <div class="section-signature page-header text-right">
                    <h2><span id="lblSignature" runat="server" class="label label-default">Nowy formularz</span></h2>
            </div>
        </div>
    </div>
    <!-- 1st Row -->
    <div class="row">
        <div class="col-md-2">
            <label id="lblCreated" runat="server">Data utworzenia</label>
        </div>
        <div class="col-md-3">
            <label id="fldCreated" runat="server" class="form-control fldCreated formControlExtended"></label>
        </div>
        <div class="col-md-2 col-md-offset-1">
            <label id="lblCategoryLookup" runat="server">Kategoria</label>
        </div>
        <div class="col-md-4 fldCategoryLookup">
            <SharePoint:FormField ID="fldCategoryLookup" runat="server" FieldName="CategoryLookup" CssClass="form-control formControlExtended" />
        </div>
    </div>
    <!-- 2nd Row -->
    <div class="row">
        <div class="col-md-2">
            <label id="lblRequestor" runat="server" for="fldRequestor">Wnioskodawca</label>
        </div>
        <div class="col-md-3">
            <label id="fldRequestor" runat="server" class="form-control fldRequestor disabled"></label>
            <label id="fldRequestorHidden" runat="server" class="fldRequestorHidden hidden"></label>
        </div>
        <div class="col-md-2 col-md-offset-1">
            <label id="lblApprover" runat="server" for="fldApprover">Akceptujący</label>
        </div>
        <div class="col-md-4">
            <select id="fldApprover" class="fldApprover form-control formControlExtended" runat="server">
            </select>
            <div class="progress fldApproverProgressBar">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
            </div>
            <label id="lblApproverDisplay" runat="server" class="lblApproverDisplay form-control"></label>
            <label id="fldApproverHidden" runat="server" class="fldApproverHidden hidden"></label>
        </div>
    </div>
    <!-- 3ard Row -->
    <div class="row">
        <div class="col-md-2">
            <label id="lblRequestFor" runat="server" for="fldRequestFor">Przepustka dla</label>
        </div>
        <div class="col-md-3 RequestFor">
            <SharePoint:FormField ID="fldRequestFor" runat="server" FieldName="RequestFor" CssClass="form-control RequestFor formControlExtended" />
        </div>
        <div class="col-md-2 col-md-offset-1">
            <label id="lblToReturn" runat="server">Do ponownego wniesienia/zwrotu</label>
        </div>
        <div class="col-md-4 ToReturnRow">
            <SharePoint:FormField ID="fldToReturn" runat="server" FieldName="ToReturn" CssClass="form-control" />
        </div>
    </div>
    <!-- 3brd Row -->
    <div class="row">
        <div class="col-md-2">
            <label id="lblRequestorVehicle" runat="server" for="fldRequestorVehicle">Samochód (marka/nr rej.)</label>
        </div>
        <div class="col-md-3 RequestorVehicle">
            <SharePoint:FormField ID="fldRequestorVehicle" runat="server" FieldName="RequestorVehicle" CssClass="form-control formControlExtended" />
        </div>
    </div>
    <!-- 4st Row -->
    <div class="row">
        <div class="col-md-12">
            <label id="lblRequestedGoods" runat="server">Wnioskuję o wyniesienie/wywiezienie następujących przedmiotów:</label>
        </div>
    </div>
    <!-- 5st Row -->
    <div class="row">
        <div class="col-md-12 center-block">
            <asp:UpdatePanel ID="RequestedGoodsPanel" runat="server" UpdateMode="Conditional" EnableViewState="true">
                    <ContentTemplate>
                        <asp:GridView ID="RequestedGoodsGrid" runat="server"  AutoGenerateColumns="False" OnRowDataBound="RequestedGoodsGrid_RowDataBound" DataKeyNames="RequestedMaterialId" ShowFooter="true" OnRowCommand="RequestedGoodsGrid_RowCommand" OnRowDeleting="RequestedGoodsGrid_RowDeleting" EnableModelValidation="True"                      
                            CssClass="table table-striped table-bordered table-hover table-condensed RequestedGoodsGrid" Width="100%">
                            <HeaderStyle />
                                <Columns>
                                <asp:TemplateField>
                                   <HeaderTemplate>
                                        <h3><asp:Label runat="server" ID="headerButtons" /></h3>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="lbtnDelete" runat="server" CssClass="lbtnDelete" CommandName="Delete" ImageAlign="Middle" ImageUrl="../../_layouts/15/images/Incodix.Wedel.SPRuchMatTow/Delete_Doc16x16.gif"
                                        OnClientClick="javascript: return RowDeleter();" CausesValidation="false" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:ImageButton ID="lbtnAdd" CssClass="lbtnAdd" runat="server" OnClientClick="javascript:SmallProgressBarEnabler();" 
                                            CommandName="Add" ImageAlign="Middle" ImageUrl="../../_layouts/15/images/Incodix.Wedel.SPRuchMatTow/AddNew16x16.png" />
                                            <div class="progress SmallProgressBarContainer hiddenSection">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                </div>
                                            </div>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle  Width="10%" HorizontalAlign="Center" />
                                    <FooterStyle Width="10%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5><asp:Label runat="server" ID="hdrMaterialName" Text="Nazwa" CssClass="ms-bold" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="fldMaterialName" runat="server" TextMode="MultiLine" CssClass="MaterialName form-control GridControl" BorderStyle="None" Text='<%#Eval("MaterialName") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="60%" HorizontalAlign="Left" />
                                    <FooterStyle Width="60%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5><asp:Label runat="server" ID="hdrUnitOfMeasure" Text="j.m." CssClass="ms-bold" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUnitOfMeasure" Visible="false" runat="server" Text='<%#Eval("UnitOfMeasure") %>' CssClass="GridControl"></asp:Label>
                                        <asp:DropDownList ID="fldUnitOfMeasure" CssClass="UnitOfMeasure form-control" runat="server">
                                            <asp:ListItem Value="szt">szt</asp:ListItem>
                                        </asp:DropDownList>                                    
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                    <FooterStyle Width="15%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5><asp:Label runat="server" ID="hdrQuantity" Text="Ilość" CssClass="ms-bold" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="fldQuantity" runat="server" CssClass="Quantity form-control GridControl" BorderStyle="None" Text='<%#Eval("Quantity") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                    <FooterStyle Width="15%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                               </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            <br />
            <label id="RequestedGoodsGridOutputLabel" runat="server" class="label label-default" />
        </div>
    </div>
    <br />
    <!-- 6st Row -->
    <div class="row">
        <div class="col-md-4">
            <label id="lblPurposeOfGoodsMovement" runat="server" for="fldPurposeOfGoodsMovement">Cel wyniesienia/wywiezienia:</label>
        </div>
        <div class="col-md-8 PurposeOfGoodsMovement">
            <SharePoint:FormField ID="fldPurposeOfGoodsMovement" runat="server" FieldName="PurposeOfGoodsMovement" CssClass="form-control" />
        </div>
    </div>
    <br />
    <!-- 8nd Row -->
    <div class="row">
        <div class="col-md-2">
            <label id="lblRequestState" runat="server" for="fldRequestState">Status wniosku: </label>
        </div>
        <div class="col-md-4 fldRequestState">
            <label id="fldRequestState" runat="server" class="form-control formControlExtended disabled fldRequestState"></label>
        </div>
        <div class="col-md-2 col-md-offset-1">
            <label id="lblApprovalDate" runat="server" for="fldApprovalDate">Data zatwierdzenia</label>
        </div>
        <div class="col-md-3">
            <label id="fldApprovalDate" runat="server" class="form-control disabled fldApprovalDate"></label>
        </div>
    </div>
    <!-- Buttons Row -->
    <div id="buttonsRow" class="row">
        <div class="col-md-3 col-md-offset-5">
             <SharePoint:GoBackButton runat="server" ControlMode="Edit" id="GoodsMovGoBackButton" />
        </div>
        <div class="col-md-2">
             <SharePoint:SaveButton runat="server" id="GoodsMovSaveButton" ControlMode="Edit" CssClass="btn btn-success btn-lg" Text="Zapisz i prześlij do akceptacji"  />
        </div>
    </div>
    <!-- Footer Row -->
    <div class="row paddingRow">
        <div class="col-md-12">
            <SharePoint:CreatedModifiedInfo ID="CreatedModifiedInfo" runat="server" ControlMode="Display" />
            <label id="fldReceptionistsHidden" runat="server" class="fldReceptionistsHidden hidden"></label>
        </div>
    </div>
</div>