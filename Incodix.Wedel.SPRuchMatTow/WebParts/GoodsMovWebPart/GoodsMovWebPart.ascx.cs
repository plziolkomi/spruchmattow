﻿using Incodix.Wedel.SPRuchMatTow.Classes;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Incodix.Wedel.SPRuchMatTow.WebParts.GoodsMovWebPart
{
    [ToolboxItemAttribute(false)]
    public partial class GoodsMovWebPart : WebPart
    {
        #region Properties
        private static uint language;
        static Logger LogWriter = Logger.Create("kategoria", "GoodsMovWebPart");
        static LogLevel lvlVerbose = LogLevel.Verbose;
        static SPDiagnosticsCategory categVerbose = new SPDiagnosticsCategory("GoodsMovWebPart", TraceSeverity.VerboseEx, EventSeverity.Verbose);
        static LogLevel lvlUnexp = LogLevel.Unexpected;
        static SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("GoodsMovWebPart", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);

        #endregion

        #region Constructors
        public GoodsMovWebPart()
        {
        }

        #endregion

        #region ProtectedMethods

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();

            if (SPContext.Current.FormContext.FormMode == SPControlMode.New || SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
                SPContext.Current.FormContext.OnSaveHandler = new EventHandler(SaveItemHandler);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!Page.IsPostBack)
            {
                SPWeb currentWeb = SPContext.Current.Web;
                // Add CSS url to page
                SPRuchMatTowFormScripts.Name = currentWeb.Url + "/AppAssets/SPRuchMatTowFormScriptsPL.css";
                if (SPContext.Current.FormContext.FormMode == SPControlMode.New)
                {
                    GoodsMovSaveButton.Text = "Zapisz i prześlij do akceptacji";
                }
                else
                {
                    GoodsMovSaveButton.Text = "Zapisz i zamknij";
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                using (SPSite currentSite = new SPSite(SPContext.Current.Site.ID, SPUserToken.SystemAccount))
                {
                    using (SPWeb currentWeb = currentSite.OpenWeb())
                    {
                        SPListItem currentItem = SPContext.Current.ListItem;
                        // Prepare form Labels
                        GetFormLabels(currentWeb, currentItem);
                        if (SPContext.Current.FormContext.FormMode == SPControlMode.New)
                        {
                            // Fill in CreateDate label
                            fldCreated.InnerText = DateTime.Now.ToString("yyyy-MM-dd hh:mm");

                            // Fill in Requestor label
                            fldRequestor.InnerText = SPContext.Current.Web.CurrentUser.Name;
                            fldRequestFor.Value = SPContext.Current.Web.CurrentUser.Name;

                            fldRequestState.InnerText = "Nowy wniosek";

                            // Fill in Approvers List
                            GetDropDownOptions(currentWeb, currentItem, false);

                            lblApproverDisplay.Visible = false;
                        }
                        else if(SPContext.Current.FormContext.FormMode == SPControlMode.Edit || SPContext.Current.FormContext.FormMode == SPControlMode.Display)
                        {
                            if (currentItem.Fields.ContainsField("Created") && currentItem[SPBuiltInFieldId.Created] != null)
                            {
                                DateTime _createdDate = DateTime.Parse(currentItem[SPBuiltInFieldId.Created].ToString());
                                fldCreated.InnerText = _createdDate.ToString("yyyy-MM-dd hh:mm"); 
                            }

                            if (currentItem.Fields.ContainsField("Author") && currentItem["Author"] != null)
                            {
                                SPFieldUserValue userValue = new SPFieldUserValue(currentWeb, currentItem["Author"].ToString());
                                fldRequestor.InnerText = userValue.LookupValue;
                                fldRequestorHidden.InnerText = userValue.LookupId.ToString();
                            }
                            if (currentItem.Fields.ContainsField("RequestState") && currentItem["RequestState"] != null)
                            {
                                fldRequestState.InnerText = currentItem["RequestState"].ToString();
                            }
                            if (currentItem.Fields.ContainsField("ApprovalDate") && currentItem["ApprovalDate"] != null)
                            {
                                DateTime _approvalDate = DateTime.Parse(currentItem["ApprovalDate"].ToString());
                                fldApprovalDate.InnerText = _approvalDate.ToString("yyyy-MM-dd hh:mm");
                            }
                            if (currentItem.Fields.ContainsField("Signature") && currentItem["Signature"] != null)
                            {
                                lblSignature.InnerHtml = currentItem["Signature"].ToString();
                            }
                            if (currentItem.Fields.ContainsField("Approver") && currentItem["Approver"] != null)
                            {
                                SPFieldUserValue _ApproverValue = new SPFieldUserValue(currentWeb, currentItem["Approver"].ToString());

                                lblApproverDisplay.InnerText = _ApproverValue.LookupValue;
                                fldApproverHidden.InnerText = _ApproverValue.LookupId.ToString();
                                fldApprover.Visible = false;
                                lblApproverDisplay.Visible = true;
                            }
                        }

                        // Get Recepcionists IDs and put on form
                        string groupReceptionists = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Receptionists", null, currentWeb.Language);
                        SPGroup group = currentWeb.Groups.GetByName(groupReceptionists);
                        string userIDs = string.Empty;
                        if (group != null)
                        {
                            foreach (SPUser user in group.Users)
                            {
                                userIDs += user.ID + ";";
                            }
                            fldReceptionistsHidden.InnerText = userIDs;
                        }
                        // Prepare Requested Grid View
                        try
                        {
                            RequestedGoodsGrid.DataSource = RequestedGoodsGridData(currentWeb, currentItem);
                            RequestedGoodsGrid.DataBind();
                            RequestedGoodsGrid.Rows[0].Visible = false;
                            LogWriter.Verbose("RequestedGoods gridView successfully created", categVerbose);

                            if (SPContext.Current.FormContext.FormMode == SPControlMode.Display || SPContext.Current.FormContext.FormMode == SPControlMode.Invalid)
                            {
                                RequestedGoodsGrid.Columns[0].Visible = false;
                                RequestedGoodsGrid.Enabled = false;

                            }
                        }
                        catch (Exception ex)
                        {
                            LogWriter.Log("Unable to generate RequestedGoods Table for form with ID " + SPContext.Current.ListItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
                        }
                    }
                }
            }
            // Register startup script for UpdatePanel postbacks
            string GridFunctionStarter = @"$(document).ready(function() {GridFunctionStarter();});";
            ScriptManager.RegisterStartupScript(RequestedGoodsPanel, RequestedGoodsPanel.GetType(), Guid.NewGuid().ToString(), GridFunctionStarter, true);
        }

        #endregion

        #region PrivateMethods

        private void GetFormLabels(SPWeb currentWeb, SPListItem currentItem)
        {
            language = currentWeb != null ? currentWeb.Language : 1033;
            try
            {
                lblCreated.InnerText = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,lblCreated", null, language) + ":";
                lblCategoryLookup.InnerText = currentItem.Fields.ContainsField("CategoryLookup") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,CategoryLookup", null, language)].Title + ":" : "Kategoria:";
                lblRequestor.InnerText = currentItem.Fields.ContainsField("Requestor") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Requestor", null, language)].Title + ":" : "Wnioskodawca:";
                lblRequestFor.InnerText = currentItem.Fields.ContainsField("RequestFor") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestFor", null, language)].Title + ":" : "Przepustka dla:";
                lblToReturn.InnerText = currentItem.Fields.ContainsField("ToReturn") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ToReturn", null, language)].Title + ":" : "Do ponownego wniesienia/zwrotu:";
                lblApprover.InnerText = currentItem.Fields.ContainsField("Approver") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Approver", null, language)].Title + ":" : "Akceptujący:";
                lblRequestorVehicle.InnerText = currentItem.Fields.ContainsField("RequestorVehicle") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestorVehicle", null, language)].Title + ":" : "Samochód (marka/nr rej.):";
                lblRequestedGoods.InnerText = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,lblRequestedGoods", null, language) + ":";
                lblPurposeOfGoodsMovement.InnerText = currentItem.Fields.ContainsField("PurposeOfGoodsMovement") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,PurposeOfGoodsMovement", null, language)].Title + ":" : "Cel wyniesienia/wywiezienia:";
                lblRequestState.InnerText = currentItem.Fields.ContainsField("RequestState") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestState", null, language)].Title + ":" : "Status wniosku:";
                lblApprovalDate.InnerText = currentItem.Fields.ContainsField("ApprovalDate") ? currentItem.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ApprovalDate", null, language)].Title + ":" : "Data zatwierdzenia:";

                LogWriter.Verbose("Form Labels successfully inserted", categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Unexpected("GoosMovWebPart is unable to insert form labels due to: " + ex.Message, categUnexp);
            }
            // Hide buttons on dispform
            if (SPContext.Current.FormContext.FormMode == SPControlMode.Invalid || SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                GoodsMovSaveButton.Visible = false;
                GoodsMovGoBackButton.Visible = false;
            }
        }
        private void SaveItemHandler(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                SaveItem();
            }
        }
        private void SaveItem()
        {
            SPWeb currentWeb = SPContext.Current.Web;
            bool unsafeUpdates = currentWeb.AllowUnsafeUpdates;
            int _FormID = -1;
            try
            {
                if (SPContext.Current.FormContext.FormMode != SPControlMode.Display)
                {
                    if (SPContext.Current.FormContext.FormMode != SPControlMode.New)
                    {
                        _FormID = SPContext.Current.ItemId;
                    }
                    // Save Whole Item
                    SaveButton.SaveItem(SPContext.Current, false, "");

                    if (SPContext.Current.FormContext.FormMode == SPControlMode.New)
                    {
                        _FormID = SPContext.Current.Item.ID;

                        using (SPSite elvatedSite = new SPSite(currentWeb.Url, SPUserToken.SystemAccount))
                        {
                            using (SPWeb elevatedWeb = elvatedSite.OpenWeb())
                            {
                                SPListItem currentItem = elevatedWeb.GetList(elevatedWeb.Url + "/Lists/GoodsMovementList").GetItemById(_FormID);
                                PerformActionUtility roleUtility = new PerformActionUtility();
                                PerformActionUtility actionUtility = new PerformActionUtility();

                                string currentFormState = currentItem["RequestState"].ToString();
                                // SendForApproval Action
                                if (currentFormState == "Nowy wniosek")
                                {
                                    currentItem["RequestState"] = "Oczekiwanie na akceptacje";

                                    // Set Signature
                                    if (currentItem.Fields.ContainsField("Signature") && currentItem["Signature"] == null)
                                    {
                                        string _signature = actionUtility.SetSignature(elevatedWeb, currentItem);
                                        currentItem["Signature"] = _signature;
                                        currentItem["CreationYear"] = _signature.Substring(0, 4);
                                    }
                                }
                                // Update related Data
                                UpdateFormData(currentWeb, currentItem, _FormID);

                                LogWriter.Log("GoodsMovWebPart started saving form with ID " + _FormID, lvlVerbose, categVerbose);
                                elevatedWeb.AllowUnsafeUpdates = true;
                                currentItem.Update();
                                elevatedWeb.AllowUnsafeUpdates = unsafeUpdates;
                                LogWriter.Log("GoodsMovWebPart successfully saved form with ID " + _FormID, lvlVerbose, categVerbose);

                                if (currentItem.Fields.ContainsField("Approver") && currentItem["Approver"] != null)
                                {
                                    SPFieldUserValue approverValue = new SPFieldUserValue(elevatedWeb, currentItem["Approver"].ToString());
                                    PerformActionUtility ActionUtility = new PerformActionUtility();
                                    ActionUtility.GetTemplateAndSendMail(elevatedWeb, currentItem, approverValue.User, currentWeb.CurrentUser, "SendForApprovalTemplate", "");
                                }

                                roleUtility.UniqueRoleAssignment(elevatedWeb, currentItem, "permEdit");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = "GoodsMovWebPart is unable to save data of form with ID " + _FormID + " due to: " + ex.Message;
                LogWriter.Unexpected(msg, categUnexp);
                SPUtility.TransferToErrorPage(msg);
            }

        }
        private void UpdateFormData(SPWeb currentWeb, SPListItem currentItem, int _FormID)
        {
            try
            {
                // Save data From GridView
                if (currentItem.Fields.ContainsField("RequestedGoods"))
                {
                   // XmlWriter writer = new XmlWriter();
                    DataTable dt = RequestedGoodsGridAddedData();
                    DataSet ds = new DataSet();
                    ds.Tables.Add(dt);
                    currentItem["RequestedGoods"] = ds.GetXml();
                    LogWriter.Log("GoodsMovWebPart saved RequestedGoods Gridiew data into SPField for form with ID " + _FormID, lvlVerbose, categVerbose);
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("GoodsMovWebPart is unable to save form with ID " + _FormID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
        }
        private void GetDropDownOptions(SPWeb currentWeb, SPListItem currentItem, bool filtered)
        {
            ListItemCollection dtfldApproverDistinct = new ListItemCollection();
            string currUserID = SPContext.Current.Web.CurrentUser.LoginName;
            SPUser currentApprover = currentItem["Approver"] != null ? new SPFieldUserValue(currentWeb, currentItem["Approver"].ToString()).User : null;
            try
            {
                LogWriter.Log("GoodsMovWebPart started getting dropDown options for Approvers list", lvlVerbose, categVerbose);
                SPQuery query = new SPQuery();
                if (!filtered)
                {
                    query.Query = string.Concat(@"<Where>
                                                <IsNotNull>
                                                    <FieldRef Name='Approver' />
                                                </IsNotNull>
                                            </Where>");
                }
                else
                {
                    string selectedCategory = fldCategoryLookup.Value.ToString();
                    SPFieldLookupValue lookupVal = new SPFieldLookupValue(selectedCategory);
                    query.Query = string.Concat(@"<Where>
                                                      <Contains>
                                                         <FieldRef Name='CategoriesLookup' />
                                                         <Value Type='LookupMulti'>" + lookupVal.LookupValue + @"</Value>
                                                      </Contains>
                                            </Where>");
                }
                query.ViewFields = string.Concat(
                                    "<FieldRef Name='ID' />",
                                    "<FieldRef Name='Approver' />",
                                    "<FieldRef Name='SKDApproverID' />",
                                    "<FieldRef = Name='CategoriesLookup' />");
                SPListItemCollection actualApprovers = currentWeb.GetList(currentWeb.Url + "/Lists/ApproversList").GetItems(query);
                if (actualApprovers.Count > 0)
                {
                    LogWriter.Log("GoodsMovWebPart got " + actualApprovers.Count + " Approvers", lvlVerbose, categVerbose);
                    DataTable approversDt = actualApprovers.GetDataTable();
                    DataView dataView = new DataView(approversDt);
                    DataTable dtDistinctList = dataView.ToTable(true, "Approver", "CategoriesLookup", "SKDApproverID", "ID");

                    if (fldApprover.Items.Count > 0)
                    {
                        fldApprover.Items.Clear();
                    }
                    for (int i = 0; i < dtDistinctList.Rows.Count; i++)
                    {
                        string valueToInsert = string.Empty;
                        string apprVal = dtDistinctList.Rows[i]["Approver"].ToString();
                        int SKDApproverID = Int32.Parse(dtDistinctList.Rows[i]["SKDApproverID"].ToString());
                        currentWeb.AllowUnsafeUpdates = true;
                        SPUser ApproverUser = currentWeb.EnsureUser(apprVal);
                        if (ApproverUser.LoginName.ToLower() != currUserID.ToLower())
                        {
                            // Get UserValue
                            SPFieldUserValue approverVal = new SPFieldUserValue(currentWeb, ApproverUser.ID, ApproverUser.Name);

                            // Get Approver presence                            
                            ListItem item = new ListItem(valueToInsert, dtDistinctList.Rows[i]["ID"].ToString());
                            if (currentApprover != null)
                            {
                                if (currentApprover.Name == approverVal.LookupValue)
                                {
                                    item.Selected = true;
                                }
                            }
                            fldApprover.Items.Add(item); 
                        }
                    }
                    currentWeb.AllowUnsafeUpdates = false;
                    LogWriter.Log("GoodsMovWebPart successfully inserted Approvers to fldApprover dropdown", lvlVerbose, categVerbose);
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("GoodsMovWebPart is unable to insert Approvers to fldApprover dropdown for form with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
                lblApproverDisplay.InnerText = "Nie udalo sie pobrać opcji wybor  do listy osób akceptujacych. Skontaktuj się z administratorem systemu";
            }
        }
        
        #endregion

        #region GridMethods

        protected DataTable RequestedGoodsGridNoData()
        {
            DataTable dt = new DataTable();
            DataColumn ObjectiveIdRequestedMaterialId = new DataColumn();
            ObjectiveIdRequestedMaterialId.ColumnName = "RequestedMaterialId";
            ObjectiveIdRequestedMaterialId.DataType = Type.GetType("System.Int32");
            ObjectiveIdRequestedMaterialId.AutoIncrement = true;
            ObjectiveIdRequestedMaterialId.AutoIncrementSeed = 1;
            ObjectiveIdRequestedMaterialId.AutoIncrementStep = 1;
            ObjectiveIdRequestedMaterialId.ReadOnly = true;
            ObjectiveIdRequestedMaterialId.Unique = true;
            dt.Columns.Add(ObjectiveIdRequestedMaterialId);
            dt.Columns.Add("MaterialName", typeof(string));
            dt.Columns.Add("UnitOfMeasure", typeof(string));
            dt.Columns.Add("Quantity", typeof(string));
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            LogWriter.Log("GoodsMovWebPart prepared DataTable draft for RequestedGoods DT", lvlVerbose, categVerbose);
            return dt;
        }
        protected DataTable RequestedGoodsGridData(SPWeb currentWeb, SPListItem currentItem)
        {
            DataTable dt = RequestedGoodsGridNoData();
            if (SPContext.Current.FormContext.FormMode != SPControlMode.New)
            {
                //Get values from SPListItem field and convert to DataTable
                if(currentItem.Fields.ContainsField("RequestedGoods") && currentItem["RequestedGoods"] != null)
                {
                    XDocument requestedGoodsXML = XDocument.Parse(currentItem["RequestedGoods"].ToString());
                    var booksQuery = from books in requestedGoodsXML.Elements("NewDataSet").Elements("Table1")
                                     select books;

                    dt = booksQuery.ToDataTable();
                    LogWriter.Log("GoodsMovWebPart get currentItem XML data for RequestedGoods DT", lvlVerbose, categVerbose);
                }
            }
            else
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
            return dt;
        }
        protected DataTable RequestedGoodsGridAddedData()
        {
            DataTable dt = RequestedGoodsGridNoData();
            DataRow dr;
            GridViewRowCollection rows = RequestedGoodsGrid.Rows;
            foreach (GridViewRow row in rows)
            {
                if (row.RowType != DataControlRowType.Header || row.RowType != DataControlRowType.Footer || row.RowType != DataControlRowType.EmptyDataRow)
                {
                    TextBox fldMaterialName = (TextBox)row.FindControl("fldMaterialName");
                    DropDownList fldUnitOfMeasure = (DropDownList)row.FindControl("fldUnitOfMeasure");
                    TextBox fldQuantity = (TextBox)row.FindControl("fldQuantity");

                    if (!string.IsNullOrEmpty(fldMaterialName.Text))
                    {
                        dr = dt.NewRow();
                        dr["MaterialName"] = fldMaterialName.Text;
                        dr["UnitOfMeasure"] = fldUnitOfMeasure.Text;
                        dr["Quantity"] = fldQuantity.Text;
                        dt.Rows.Add(dr); 
                    }
                }
            }
            LogWriter.Log("GoodsMovWebPart got updated DataTable for RequestedGoods save process", lvlVerbose, categVerbose);
            return dt;
        }
        protected void RequestedGoodsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SPWeb currentWeb = SPContext.Current.Web;
            int _FormID = SPContext.Current.ListItem.ID;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    DropDownList fldUnitOfMeasure = (DropDownList)e.Row.FindControl("fldUnitOfMeasure");

                    Label lblUnitOfMeasure = (Label)e.Row.FindControl("lblUnitOfMeasure");

                    if (SPContext.Current.FormContext.FormMode == SPControlMode.Edit || SPContext.Current.FormContext.FormMode == SPControlMode.New)
                    {                      
                        // Adjust UnitOfMeasure
                        if (!string.IsNullOrEmpty(lblUnitOfMeasure.Text))
                        {
                            fldUnitOfMeasure.Items.FindByText(lblUnitOfMeasure.Text).Selected = true;
                        }
                        fldUnitOfMeasure.Visible = true;
                        lblUnitOfMeasure.Visible = false;
                    }
                    else
                    {
                        fldUnitOfMeasure.Visible = false;
                        lblUnitOfMeasure.Visible = true;
                    }
                    if(!Page.IsPostBack)
                        LogWriter.Log("GoodsMovWebPart filled in field 'Jednostka miary' for form with ID " + _FormID, lvlVerbose, categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Unable to fill in field 'Jednostka miary' for form with ID " + _FormID + " due to: " + ex.Message, lvlUnexp, categUnexp);
                    RequestedGoodsGridOutputLabel.InnerText = "Wystąpił problem z zapisem danych w polu 'Jednostka miary'. Skontaktuj się z Administratorem";
                }
            }
        }
        protected void RequestedGoodsGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _FormID = SPContext.Current.ListItem.ID;
            if (e.CommandName == "Add")
            {
                try
                {
                    DataTable dt = RequestedGoodsGridAddedData();
                    DataRow dr = dt.NewRow();
                    dt.Rows.Add(dr);

                    RequestedGoodsGrid.DataSource = dt;
                    RequestedGoodsGrid.DataBind();
                    RequestedGoodsGrid.Rows[0].Visible = false;
                    if(!Page.IsPostBack)
                        LogWriter.Log("GoodsMovWebPart filled in field 'Jednostka miary' for form with ID " + _FormID, lvlVerbose, categVerbose);

                }
                catch (Exception ex)
                {
                    LogWriter.Log("Unable to insert new row for form with ID " + _FormID + " due to: " + ex.Message, lvlUnexp, categUnexp);
                    RequestedGoodsGridOutputLabel.InnerText = "Nie udalo sie nowego wiersza materiału do formularza wydania. Skontaktuj się z Administratorem";
                }
            }
        }
        protected void RequestedGoodsGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int _FormID = SPContext.Current.ListItem.ID;
            try
            {
                DataTable dt = RequestedGoodsGridAddedData();
                DataRow dr = dt.Rows[e.RowIndex];
                dr.Delete();
                RequestedGoodsGrid.DataSource = dt;
                RequestedGoodsGrid.DataBind();
                RequestedGoodsGrid.Rows[0].Visible = false;
                LogWriter.Log("GoodsMovWebPart successfully deleted row for form with ID " + _FormID, lvlVerbose, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to delete row for form with ID " + _FormID + " due to: " + ex.Message, lvlUnexp, categUnexp);
                RequestedGoodsGridOutputLabel.InnerText = "Nie udalo sie usunąć wiersza materiału do formularza wydania o ID " + _FormID + " z powodu: " + ex.Message;
            }
        }

        #endregion

    }
}
