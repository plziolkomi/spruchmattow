﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GoodsMovFormWebPartUserControl.ascx.cs" Inherits="Incodix.Wedel.SPRuchMatTow.WebParts.GoodsMovFormWebPart.GoodsMovFormWebPartUserControl" %>
<link href="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.css" rel="stylesheet" />
<SharePoint:CssRegistration ID="SPRuchMatTowFormCSS" Name="~sitecollection/AppAssets/SPRuchMatTowForm.css" runat="server" />
<SharePoint:CssRegistration ID="SPRuchMatTowResponsive" Name="~sitecollection/AppAssets/SPRuchMatTowResponsive.css" runat="server" />
<script src="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/jquery.js"></script>
<SharePoint:ScriptLink ID="SPRuchMatTowFormScripts" Name="~sitecollection/AppAssets/SPRuchMatTowFormScriptsPL.js" runat="server" />
<script src="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        HideDefaultWebPartForm();

        GetInitialData();

        InjectBootstrapStyles();

        InsertScriptToDropDown();

        $('div.fldApproverProgressBar').hide();

    });
</script>
<div id="form" class="container form-group">
    <!-- Form Header -->
    <div class="row">
        <div class="col-md-10">
            <div class="section-title page-header">
                <h2>Kontrola Ruchu Materiałowo-Towarowego</h2>
            </div>
        </div>
        <div class="col-md-2">
            <div class="section-signature page-header text-right">
                    <h2><span id="lblSignature" runat="server" class="label label-default">Nowy formularz</span></h2>
            </div>
        </div>
    </div>
    <!-- 1st Row -->
    <div class="row">
        <div class="col-md-3">
            <label id="lblCreated" runat="server">Data utworzenia</label>
        </div>
        <div class="col-md-3">
            <label id="fldCreated" runat="server" class="form-control"></label>
        </div>
        <div class="col-md-3">
            <label id="lblCategoryLookup" runat="server">Kategoria</label>
        </div>
        <div class="col-md-3 fldCategoryLookup">
            <SharePoint:FormField ID="fldCategoryLookup" runat="server" FieldName="CategoryLookup" CssClass="form-control" />
        </div>
    </div>
    <!-- 2nd Row -->
    <div class="row">

        <div class="col-md-3">
            <label id="lblRequestor" runat="server" for="fldRequestor">Wnioskodawca</label>
        </div>
        <div class="col-md-3">
            <label id="fldRequestor" runat="server" class="form-control"></label>
        </div>
        <div class="col-md-3">
            <label id="lblApprover" runat="server" for="fldApprover">Akceptujący</label>
        </div>
        <div class="col-md-3">
            <asp:DropDownList ID="fldApprover" CssClass="fldApprover form-control" runat="server">
            </asp:DropDownList>
            <div class="progress fldApproverProgressBar">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                </div>
            </div>
            <label id="lblApproverDisplay" runat="server" class="lblApproverDisplay form-control"></label>
        </div>
    </div>
    <!-- 3rd Row -->
    <div class="row">
        <div class="col-md-4">
            <label id="lblRequestorVehicle" runat="server" for="fldRequestorVehicle">Samochód (marka/nr rej.):</label>
        </div>
        <div class="col-md-8 RequestorVehicle">
            <SharePoint:FormField ID="fldRequestorVehicle" runat="server" FieldName="RequestorVehicle" CssClass="form-control" />
        </div>
    </div>
    <!-- 4st Row -->
    <div class="row">
        <div class="col-md-12">
            <label id="lblRequestedGoods" runat="server">Wnioskuję o wyniesienie/wywiezienie następujących przedmiotów:</label>
        </div>
    </div>
    <!-- 5st Row -->
    <div class="row">
        <div class="col-md-12 center-block">
            <asp:UpdatePanel ID="ObjectivesPanel" runat="server" UpdateMode="Conditional" EnableViewState="true">
                    <ContentTemplate>
                        <asp:GridView ID="RequestedMaterialsGrid" runat="server"  AutoGenerateColumns="False" OnRowDataBound="RequestedMaterialsGrid_RowDataBound" DataKeyNames="RequestedMaterialId" ShowFooter="true" OnRowCommand="RequestedMaterialsGrid_RowCommand" OnRowDeleting="RequestedMaterialsGrid_RowDeleting" EnableModelValidation="True"                      
                            CssClass="table table-striped table-bordered table-hover table-condensed" Width="100%">
                            <HeaderStyle />
                                <Columns>
                                <asp:TemplateField>
                                   <HeaderTemplate>
                                        <h3><asp:Label runat="server" ID="headerButtons" /></h3>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="lbtnDelete" runat="server" CssClass="lbtnDelete" CommandName="Delete" ImageAlign="Middle" ImageUrl="../../_layouts/15/images/Incodix.Wedel.SPRuchMatTow/Delete_Doc16x16.gif"
                                        OnClientClick="javascript: return RowDeleter();" CausesValidation="false" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:ImageButton ID="lbtnAdd" CssClass="lbtnAdd" runat="server" OnClientClick="javascript:SmallProgressBarEnabler();" 
                                            CommandName="Add" ImageAlign="Middle" ImageUrl="../../_layouts/15/images/Incodix.Wedel.SPRuchMatTow/AddNew16x16.png" />
                                            <div id="SmallProgressBarContainer" class="hiddenSection">
                                                   <div id="SmallProgressBar"></div>
                                            </div>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle  Width="10%" HorizontalAlign="Center" />
                                    <FooterStyle Width="10%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5><asp:Label runat="server" ID="hdrMaterialName" Text="Nazwa" CssClass="label label-default" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="fldMaterialName" runat="server" CssClass="MaterialName form-control" BorderStyle="None" Text='<%#Eval("MaterialName") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="60%" HorizontalAlign="Center" />
                                    <FooterStyle Width="60%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5><asp:Label runat="server" ID="hdrUnitOfMeasure" Text="j.m." CssClass="label label-default" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUnitOfMeasure" Visible="false" runat="server" Text='<%#Eval("UnitOfMeasure") %>'></asp:Label>
                                        <asp:DropDownList ID="fldUnitOfMeasure" CssClass="UnitOfMeasure form-control" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="KG">KG</asp:ListItem>
                                            <asp:ListItem Value="L">L</asp:ListItem>
                                            <asp:ListItem Value="Sztuka">Sztuka</asp:ListItem>
                                            <asp:ListItem Value="Karton">Katron</asp:ListItem>
                                        </asp:DropDownList>                                    
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                    <FooterStyle Width="15%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <h5><asp:Label runat="server" ID="hdrQuantity" Text="Ilość" CssClass="label label-default" /></h5>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="fldQuantity" runat="server" CssClass="Quantity form-control" BorderStyle="None" Text='<%#Eval("Quantity") %>'>
                                        </asp:TextBox>
                                    </ItemTemplate>
                                    <FooterTemplate />
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                    <FooterStyle Width="15%" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ID" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="txtID" runat="server" Text='<%#Eval("ID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                               </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            <br />
            <label id="RequestedMaterialsGridOutputLabel" runat="server" class="label label-default" />
        </div>
    </div>
    <br />
    <!-- 6st Row -->
    <div class="row">
        <div class="col-md-4">
            <label id="lblPurposeOfGoodsMovement" runat="server" for="fldPurposeOfGoodsMovement">Cel wyniesienia/wywiezienia:</label>
        </div>
        <div class="col-md-8 PurposeOfGoodsMovement">
            <SharePoint:FormField ID="fldPurposeOfGoodsMovement" runat="server" FieldName="PurposeOfGoodsMovement" CssClass="form-control" />
        </div>
    </div>
    <br />
    <!-- 8nd Row -->
    <div class="row">
        <div class="col-md-3">
            <label id="lblRequestState" runat="server" for="fldRequestState">Status wniosku: </label>
        </div>
        <div class="col-md-3 fldRequestState">
            <label id="fldRequestState" runat="server" class="form-control"></label>
        </div>
        <div class="col-md-3">
            <label id="lblApprovalDate" runat="server" for="fldApprovalDate">Data zatwierdzenia</label>
        </div>
        <div class="col-md-3">
            <label id="fldApprovalDate" runat="server" class="form-control"></label>
        </div>
    </div>
    <!-- Footer Row -->
    <div class="row paddingRow">
        <div class="col-md-12">
            <SharePoint:CreatedModifiedInfo ID="CreatedModifiedInfo" runat="server" ControlMode="Display" />
        </div>
    </div>
</div>