﻿using Incodix.Wedel.SPRuchMatTow.Classes;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.WebControls;
using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Incodix.Wedel.SPRuchMatTow.WebParts.GoodsMovFormWebPart
{
    public partial class GoodsMovFormWebPartUserControl : UserControl
    {
        #region Properties
        private static uint language;
        static Logger LogWriter = Logger.Create("kategoria", "GoodsMovFormWebPartUserControl");
        static LogLevel levelVerbose = LogLevel.Verbose;
        static SPDiagnosticsCategory categoryVerbose = new SPDiagnosticsCategory("GoodsMovFormWebPartUserControl", TraceSeverity.VerboseEx, EventSeverity.Verbose);
        static LogLevel levelUnexpected = LogLevel.Unexpected;
        static SPDiagnosticsCategory categoryUnexpected = new SPDiagnosticsCategory("GoodsMovFormWebPartUserControl", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        private static bool hasPermissions;

        #endregion

        #region Overrides
        protected override void OnInit(EventArgs e)
        {

            base.OnInit(e);
            int _FormID = SPContext.Current.ListItem.ID;
            //if (SPContext.Current.FormContext.FormMode != SPControlMode.New && SPContext.Current.FormContext.FormMode != SPControlMode.Invalid)
            //{
            //    hasPermissions = SPContext.Current.ListItem.DoesUserHavePermissions(SPContext.Current.Web.CurrentUser, SPBasePermissions.FullMask);
            //}
            if (SPContext.Current.FormContext.FormMode == SPControlMode.New || SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
                SPContext.Current.FormContext.OnSaveHandler = new EventHandler(SaveItemHandler);

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!Page.IsPostBack)
            {
                SPWeb currentWeb = SPContext.Current.Web;
                // Add CSS url to page
                SPRuchMatTowFormCSS.Name = currentWeb.Url + "/AppAssets/SPRuchMatTowForm.css";
                SPRuchMatTowFormScripts.Name = currentWeb.Url + "/AppAssets/SPRuchMatTowFormScriptsPL.css";
                SPRuchMatTowResponsive.Name = currentWeb.Url + "/AppAssets/SPRuchMatTowResponsive.css";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (SPContext.Current.FormContext.FormMode == SPControlMode.New) {
                    // Fill in CreateDate label
                    fldCreated.InnerText = DateTime.Now.ToString("yyyy-MM-dd hh:mm");

                    // Fill in Requestor label
                    fldRequestor.InnerText = SPContext.Current.Web.CurrentUser.Name;

                    fldRequestState.InnerText = "Złożenie wniosku";

                    lblApproverDisplay.Visible = false;
                }
                else
                {
                    SPListItem currentItem = SPContext.Current.ListItem;
                    if (currentItem.Fields.ContainsField("Created") && currentItem["Created"] != null)
                    {
                        DateTime _createdDate = DateTime.Parse(currentItem["Created"].ToString());
                        fldCreated.InnerText = _createdDate.ToString("yyyy-MM-dd hh:mm");
                    }
                    if (currentItem.Fields.ContainsField("Author") && currentItem["Author"] != null)
                    {
                        SPFieldUserValue userValue = new SPFieldUserValue(SPContext.Current.Web, currentItem["Author"].ToString());
                        fldRequestor.InnerText = userValue.LookupValue;
                    }
                    if (currentItem.Fields.ContainsField("RequestState") && currentItem["RequestState"] != null)
                    {
                        fldRequestState.InnerText = currentItem["RequestState"].ToString();
                    }
                    if (currentItem.Fields.ContainsField("ApprovalDate") && currentItem["ApprovalDate"] != null)
                    {
                        fldApprovalDate.InnerText = currentItem["ApprovalDate"].ToString();
                    }
                    if (currentItem.Fields.ContainsField("Signature") && currentItem["Signature"] != null)
                    {
                        lblSignature.InnerHtml = currentItem["Signature"].ToString();
                    }
                    if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
                    {
                        if (currentItem.Fields.ContainsField("Approver") && currentItem["Approver"] != null)
                        {
                            SPFieldUserValue _ApproverValue = new SPFieldUserValue(SPContext.Current.Web, currentItem["Approver"].ToString());
                            lblApproverDisplay.InnerText = _ApproverValue.LookupValue;
                            fldApprover.Visible = false;
                        }
                    }
                    else if(SPContext.Current.FormContext.FormMode == SPControlMode.Edit)
                    {
                        lblApproverDisplay.Visible = false;
                    }
                }
                // Fill in Approvers List
                GetDropDownOptions(SPContext.Current.Web, SPContext.Current.ListItem, false);

                // Prepare KeyObjectives Grid View
                try
                {
                    RequestedMaterialsGrid.DataSource = RequestedMaterialsGridData(SPContext.Current.Web, SPContext.Current.ListItem.ID);
                    RequestedMaterialsGrid.DataBind();
                    RequestedMaterialsGrid.Rows[0].Visible = false;

                    if (SPContext.Current.FormContext.FormMode == SPControlMode.Display || SPContext.Current.FormContext.FormMode == SPControlMode.Invalid)
                    {
                        RequestedMaterialsGrid.Columns[0].Visible = false;
                        RequestedMaterialsGrid.Enabled = false;
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Nie udalo sie wygenerowac tabeli Kluczowych Celów i Wyników dla dokumentu o ID " + SPContext.Current.ListItem.ID + " z powodu: " + ex.Message, levelUnexpected, categoryUnexpected);
                    //ObjectivesOutputLabel.Text = "Nie udalo sie wygenerowac tabeli Kluczowych Celów i Wyników dla dokumentu o ID " + currentItem.ID + " z powodu: " + ex.Message;
                }
            }
        }
        #endregion

        #region PrivateMethods

        private void SaveItemHandler(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                SaveItem();
            }
        }
        private void SaveItem()
        {
            int _FormID = -1;
            try
            {
                if (SPContext.Current.FormContext.FormMode != SPControlMode.Display)
                {
                    if (SPContext.Current.FormContext.FormMode != SPControlMode.New)
                    {
                        _FormID = SPContext.Current.ItemId;
                    }
                    // Save Whole Item
                    SaveButton.SaveItem(SPContext.Current, false, "");

                    if (SPContext.Current.FormContext.FormMode == SPControlMode.New)
                    {
                        _FormID = SPContext.Current.Item.ID;
                    }
                }
            }
            catch (Exception ex)
            { }
            // Update SubsidPositions
            UpdateGridData(SPContext.Current.Web, _FormID);
        }
        private void UpdateGridData(SPWeb currentweb, int _FormID)
        {
            try
            {
                SPListItem currentItem = SPContext.Current.List.GetItemById(_FormID);
                currentweb.AllowUnsafeUpdates = true;

                // Save selected Approver
                if (currentItem.Fields.ContainsField("Approver"))
                {
                    string _selectedApprover = fldApprover.SelectedValue;
                    SPFieldUserValue userValue = new SPFieldUserValue(currentweb, _selectedApprover);
                    currentItem["Approver"] = userValue;
                }

                currentItem.Update();

                currentweb.AllowUnsafeUpdates = false;
            }
            catch (Exception ex)
            {
                LogWriter.Log("Nie udalo sie pobrać opcji wyboru do listy akceptujących o ID " + _FormID + " z powodu: " + ex.Message, levelUnexpected, categoryUnexpected);
            }
        }
        private void GetDropDownOptions(SPWeb currentWeb, SPListItem currentItem, bool filtered)
        {
            ListItemCollection dtfldApproverDistinct = new ListItemCollection();
            SPUser currentApprover = currentItem["Approver"] != null ? new SPFieldUserValue(currentWeb, currentItem["Approver"].ToString()).User : null;
            try
            {
                SPQuery query = new SPQuery();
                if (!filtered)
                {
                    query.Query = string.Concat(@"<Where>
                                                <IsNotNull>
                                                    <FieldRef Name='Approver' />
                                                </IsNotNull>
                                            </Where>");
                }
                else
                {
                    string selectedCategory = fldCategoryLookup.Value.ToString();
                    SPFieldLookupValue lookupVal = new SPFieldLookupValue(selectedCategory);
                    query.Query = string.Concat(@"<Where>
                                                      <Contains>
                                                         <FieldRef Name='CategoriesLookup' />
                                                         <Value Type='LookupMulti'>" + lookupVal.LookupValue + @"</Value>
                                                      </Contains>
                                            </Where>");
                }
                query.ViewFields = string.Concat(
                                    "<FieldRef Name='ID' />",
                                    "<FieldRef Name='Approver' />",
                                    "<FieldRef = Name='CategoriesLookup' />");
                SPListItemCollection actualApprovers = currentWeb.GetList(currentWeb.Url + "/Lists/ApproversList").GetItems(query);
                if (actualApprovers.Count > 0)
                {
                    DataTable approversDt = actualApprovers.GetDataTable();
                    DataView dataView = new DataView(approversDt);
                    DataTable dtDistinctList = dataView.ToTable(true, "Approver", "CategoriesLookup", "ID");

                    if (fldApprover.Items.Count > 0)
                    {
                        fldApprover.Items.Clear();
                    }
                    for (int i = 0; i < dtDistinctList.Rows.Count; i++)
                    {
                        string apprVal = dtDistinctList.Rows[i]["Approver"].ToString();
                        SPUser ApproverUser = currentWeb.EnsureUser(apprVal);
                        SPFieldUserValue approverVal = new SPFieldUserValue(currentWeb, ApproverUser.ID, ApproverUser.Name);
                        ListItem item = new ListItem(approverVal.LookupValue, dtDistinctList.Rows[i]["ID"].ToString());
                        if (currentApprover != null)
                        {
                            if (currentApprover.Name == approverVal.LookupValue)
                            {
                                item.Selected = true;
                            } 
                        }
                        fldApprover.Items.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("Nie udalo sie pobrać opcji wyboru do listy akceptujących o ID " + currentItem.ID + " z powodu: " + ex.Message, levelUnexpected, categoryUnexpected);
                lblApproverDisplay.InnerText = "Nie udalo sie pobrać opcji wybor  do listy osób akceptujacych. Skontaktuj się z administratorem systemu";
            }
        }

        #endregion

        #region GridMethods

        protected DataTable RequestedMaterialsGridNoData()
        {
            DataTable dt = new DataTable();
            DataColumn ObjectiveIdRequestedMaterialId = new DataColumn();
            ObjectiveIdRequestedMaterialId.ColumnName = "RequestedMaterialId";
            ObjectiveIdRequestedMaterialId.DataType = System.Type.GetType("System.Int32");
            ObjectiveIdRequestedMaterialId.AutoIncrement = true;
            ObjectiveIdRequestedMaterialId.AutoIncrementSeed = 1;
            ObjectiveIdRequestedMaterialId.AutoIncrementStep = 1;
            ObjectiveIdRequestedMaterialId.ReadOnly = true;
            ObjectiveIdRequestedMaterialId.Unique = true;
            dt.Columns.Add(ObjectiveIdRequestedMaterialId);
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("MaterialName", typeof(string));
            dt.Columns.Add("UnitOfMeasure", typeof(string));
            dt.Columns.Add("Quantity", typeof(string));
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            return dt;
        }
        protected DataTable RequestedMaterialsGridData(SPWeb currentWeb, int _FormID)
        {
            DataTable dt = RequestedMaterialsGridNoData();
            if (SPContext.Current.FormContext.FormMode != SPControlMode.New)
            {
                //Get values fro SPList and convert to DataTable
                
            }
            else
            {
                for (int i = 0; i < 4; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["ID"] = -1;
                    dt.Rows.Add(dr);
                }
            }
            return dt;
        }
        protected DataTable RequestedMaterialsGridAddedData()
        {
            DataTable dt = RequestedMaterialsGridNoData();
            DataRow dr;
            GridViewRowCollection rows = RequestedMaterialsGrid.Rows;
            foreach (GridViewRow row in rows)
            {
                if (row.RowType != DataControlRowType.Header || row.RowType != DataControlRowType.Footer || row.RowType != DataControlRowType.EmptyDataRow)
                {
                    TextBox fldMaterialName = (TextBox)row.FindControl("fldMaterialName");
                    DropDownList fldUnitOfMeasure = (DropDownList)row.FindControl("fldUnitOfMeasure");
                    TextBox fldQuantity = (TextBox)row.FindControl("fldQuantity");
                    Label txtID = (Label)row.FindControl("txtID");

                    if (!string.IsNullOrEmpty(txtID.Text))
                    {
                        dr = dt.NewRow();
                        dr["ID"] = Convert.ToInt32(txtID.Text);
                        dr["MaterialName"] = fldMaterialName.Text;
                        dr["UnitOfMeasure"] = fldUnitOfMeasure.Text;
                        dr["Quantity"] = fldQuantity.Text;
                        dt.Rows.Add(dr);
                    }
                }
            }
            return dt;
        }
        protected void RequestedMaterialsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SPWeb currentWeb = SPContext.Current.Web;
            int _ReviewFormID = SPContext.Current.ListItem.ID;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    DropDownList fldUnitOfMeasure = (DropDownList)e.Row.FindControl("fldUnitOfMeasure");
                    Label lblUnitOfMeasure = (Label)e.Row.FindControl("lblUnitOfMeasure");
                    if (SPContext.Current.FormContext.FormMode == SPControlMode.Edit || SPContext.Current.FormContext.FormMode == SPControlMode.New)
                    {
                        if (!string.IsNullOrEmpty(lblUnitOfMeasure.Text))
                        {
                            fldUnitOfMeasure.Items.FindByText(lblUnitOfMeasure.Text).Selected = true;
                        }
                        fldUnitOfMeasure.Visible = true;
                        lblUnitOfMeasure.Visible = false;
                    }
                    else
                    {
                        fldUnitOfMeasure.Visible = false;
                        lblUnitOfMeasure.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Nie udalo sie uzupełnić pola 'Jednostka miary' dla dokumentu o ID " + _ReviewFormID + " z powodu: " + ex.Message, levelUnexpected, categoryUnexpected);
                    RequestedMaterialsGridOutputLabel.InnerText = "Nie udalo sie uzupełnić pola 'Jednostka miary' dla dokumentu o ID " + _ReviewFormID + " z powodu: " + ex.Message;
                }
            }
        }

        protected void RequestedMaterialsGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int _FormID = SPContext.Current.ListItem.ID;
            if (e.CommandName == "Add")
            {
                try
                {
                    DataTable dt = RequestedMaterialsGridAddedData();
                    DataRow dr = dt.NewRow();
                    dr["ID"] = -1;
                    dt.Rows.Add(dr);

                    RequestedMaterialsGrid.DataSource = dt;
                    RequestedMaterialsGrid.DataBind();
                    RequestedMaterialsGrid.Rows[0].Visible = false;
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Nie udalo sie nowego wiersza materiału do formularza wydania o ID " + _FormID + " z powodu: " + ex.Message, levelUnexpected, categoryUnexpected);
                    RequestedMaterialsGridOutputLabel.InnerText = "Nie udalo sie nowego wiersza materiału do formularza wydania o ID " + _FormID + " z powodu: " + ex.Message;
                }
            }
        }

        protected void RequestedMaterialsGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int _FormID = SPContext.Current.ListItem.ID;
            try
            {
                DataTable dt = RequestedMaterialsGridAddedData();
                DataRow dr = dt.Rows[e.RowIndex];
                if (dr["ID"].ToString() != "-1")
                    // tu usuwamy wiersz z kolekcji - KeyObjectivesDelete += dr["ID"].ToString() + ";";
                dr.Delete();
                RequestedMaterialsGrid.DataSource = dt;
                RequestedMaterialsGrid.DataBind();
                RequestedMaterialsGrid.Rows[0].Visible = false;
            }
            catch (Exception ex)
            {
                LogWriter.Log("Nie udalo sie usunąć wiersza materiału do formularza wydania o ID " + _FormID + " z powodu: " + ex.Message, levelUnexpected, categoryUnexpected);
                RequestedMaterialsGridOutputLabel.InnerText = "Nie udalo sie usunąć wiersza materiału do formularza wydania o ID " + _FormID + " z powodu: " + ex.Message;
            }
        }

        #endregion
    }
}
