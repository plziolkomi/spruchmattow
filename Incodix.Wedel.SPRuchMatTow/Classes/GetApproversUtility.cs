﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace Incodix.Wedel.SPRuchMatTow.Classes
{
    class GetApproversUtility
    {
        public GetApproversUtility()
        {
        }
        public Approver GetApproverPresenceById(string webUrl, int approverID)
        {
            string url = webUrl + "/_vti_bin/AccessControlSystemConnector.svc/GetApproverPresenceById/" + approverID;
            string response = CallService(url);
            if (!string.IsNullOrEmpty(response))
            {
                Approver _approver = JsonHelper.Deserialize<Approver>(response);
                return _approver;
            }
            else
            {
                return null;
            }
        }
        private string CallService(string serviceUrl)
        {
            WebClient client = new WebClient();
            client.UseDefaultCredentials = true;
            client.Headers["Content-type"] = "application/json";
            client.Encoding = Encoding.UTF8;
            string response = response = client.DownloadString(serviceUrl);

            return response;
        }
    }

    public class JsonHelper
    {
        public static string Serialize<T>(T obj)
        {
            MemoryStream stream = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            serializer.WriteObject(stream, obj);
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }

        public static T Deserialize<T>(string data)
        {
            if (string.IsNullOrWhiteSpace(data)) return default(T);
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(data));
            return (T)serializer.ReadObject(stream);
        }
    }

}
