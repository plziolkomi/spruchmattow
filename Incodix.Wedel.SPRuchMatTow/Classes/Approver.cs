﻿using System.Runtime.Serialization;

namespace Incodix.Wedel.SPRuchMatTow.Classes
{
    [DataContract]
    public class Approver
    {
        [DataMember]
        public string Id { get; set; }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string IsAvailable { get; set; }
    }
}
