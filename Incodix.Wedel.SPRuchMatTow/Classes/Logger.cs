﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Administration;

namespace Incodix.Wedel.SPRuchMatTow.Classes
{
    /// <summary>
    /// Class responsible for logging various events to SharePoint log
    /// </summary>
    [Serializable]
    public class Logger : IDisposable
    {
        #region Fields

        private readonly string _category;
        private bool _disposed;
        private SPDiagnosticsService _diagnosticsService;
        private bool _evetSourcesRegistered = false;

        #endregion Fields


        #region Properties

        /// <summary>
        /// Gets or sets value indicating whether event sources were registered or not
        /// </summary>
        internal bool EventSourcesRegistered
        {
            get
            {
                return _evetSourcesRegistered;
            }

            set
            {
                _evetSourcesRegistered = value;
            }
        }

        #endregion Properties


        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        /// <param name="category"></param>
        private Logger(string category)
        {
            _category = category;
            _diagnosticsService = new SPDiagnosticsService();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="productName">Name of the product.</param>
        private Logger(string category, string productName)
            : this(category)
        {

        }

        #endregion Constructors


        #region Create methods

        /// <summary>
        /// Methods that creates new instance of a logger.
        /// </summary>
        /// <param name="category"></param>
        /// <param name="productName"></param>
        /// <returns></returns>
        public static Logger Create(string category, string productName)
        {
            return new Logger(category, productName);
        }

        #endregion Create methods


        #region Private

        private static string CreateExceptionLogMessage(string message, Exception exception)
        {
            message = message + string.Format(" ==> Message: {0}, StackTrace: {1}, Source: {2}", exception.Message, exception.StackTrace, exception.Source);
            return message;
        }

        /// <summary>
        /// Gets trace severity
        /// </summary>
        /// <param name="level"></param>
        /// <returns>TraceSeverity</returns>
        private TraceSeverity GetTraceSeverity(LogLevel level)
        {
            TraceSeverity tr = TraceSeverity.Verbose;
            switch (level)
            {
                default:
                case LogLevel.Verbose:
                    tr = TraceSeverity.Verbose;
                    break;

                case LogLevel.Medium:
                    tr = TraceSeverity.Medium;
                    break;

                case LogLevel.Monitorable:
                    tr = TraceSeverity.Monitorable;
                    break;

                case LogLevel.High:
                    tr = TraceSeverity.High;
                    break;

                case LogLevel.Unexpected:
                    tr = TraceSeverity.Unexpected;
                    break;

                case LogLevel.Unassigned:
                    tr = TraceSeverity.None;
                    break;
            }

            return tr;
        }

        /// <summary>
        /// Get event severity
        /// </summary>
        /// <param name="level"></param>
        /// <returns>EventSeverity</returns>
        private EventSeverity GetEventSeverity(LogLevel level)
        {
            EventSeverity ev = EventSeverity.Verbose;
            switch (level)
            {
                default:
                case LogLevel.Verbose:
                    ev = EventSeverity.Verbose;
                    break;

                case LogLevel.Medium:
                    ev = EventSeverity.Warning;
                    break;

                case LogLevel.Monitorable:
                    ev = EventSeverity.Information;
                    break;

                case LogLevel.High:
                    ev = EventSeverity.Error;
                    break;

                case LogLevel.Unexpected:
                    ev = EventSeverity.ErrorCritical;
                    break;

                case LogLevel.Unassigned:
                    ev = EventSeverity.None;
                    break;
            }

            return ev;
        }

        #endregion Private


        #region Log Level Methods

        /// <summary>
        /// Logs the specified message on the given log level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="level">The level.</param>
        /// <param name="category">Category.</param>
        public void Log(string message, LogLevel level, SPDiagnosticsCategory category)
        {
            TraceSeverity tr = GetTraceSeverity(level);
            EventSeverity er = GetEventSeverity(level);
            category.TraceSeverity = tr;
            category.EventSeverity = er;

            _diagnosticsService.WriteTrace((uint)AppDomain.CurrentDomain.Id, category, tr, message, null);

            if (_evetSourcesRegistered)
            {
                // TODO: Check for category in Event Log
                _diagnosticsService.WriteEvent((ushort)AppDomain.CurrentDomain.Id, category, er, message, null);
            }
        }

        /// <summary>
        /// Logs the specified message on the given log level. Parameters are added to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="level">The level.</param>
        /// <param name="category">Category.</param>
        /// <param name="parameters">The parameters.</param>
        public void Log(string message, LogLevel level, SPDiagnosticsCategory category, params object[] parameters)
        {
            message = string.Format(message, parameters);
            this.Log(message, level, category);
        }

        /// <summary>
        /// Logs the specified message on the given log level. The following expetion properties: Message, StackTrace and Source are added to the log message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="level">The level.</param>
        /// <param name="category">Category.</param>
        /// <param name="exception">The exception.</param>
        public void Log(string message, LogLevel level, SPDiagnosticsCategory category, Exception exception)
        {
            message = CreateExceptionLogMessage(message, exception);
            this.Log(message, level, category);
        }

        /// <summary>
        /// Logs the specified message on the given log level. The both expetion properties (Message, StackTrace and Source) and parameters are added to the log message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="level">The level.</param>
        /// <param name="category">Category.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="parameters">The parameters.</param>
        public void Log(string message, LogLevel level, SPDiagnosticsCategory category, Exception exception, params object[] parameters)
        {
            message = string.Format(message, parameters);
            message = CreateExceptionLogMessage(message, exception);
            this.Log(message, level, category);
        }

        /// <summary>
        /// Logs the specified message as the Unexpected level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        public void Unexpected(string message, SPDiagnosticsCategory category)
        {
            this.Log(message, LogLevel.Unexpected, category);
        }

        /// <summary>
        /// Logs the specified message as the Unexpected level. Parameters are added to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        /// <param name="parameters">The parameters.</param>
        public void Unexpected(string message, SPDiagnosticsCategory category, params object[] parameters)
        {
            this.Log(message, LogLevel.Unexpected, category, parameters);
        }

        /// <summary>
        /// Logs the specified message as the Unexpected level. The following expetion properties: Message, StackTrace and Source are added to the log message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Unexpected(string message, SPDiagnosticsCategory category, Exception exception)
        {
            this.Log(message, LogLevel.Unexpected, category, exception);
        }

        /// <summary>
        /// Logs the specified message as the Unexpected level. The both expetion properties (Message, StackTrace and Source) and parameters are added to the log message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        /// <param name="exception">The exception.</param>
        /// <param name="parameters">The parameters.</param>
        public void Unexpected(string message, SPDiagnosticsCategory category, Exception exception, params object[] parameters)
        {
            this.Log(message, LogLevel.Unexpected, category, exception, parameters);
        }

        /// <summary>
        /// Logs the specified message as the Monitorable level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        public void Monitorable(string message, SPDiagnosticsCategory category)
        {
            this.Log(message, LogLevel.Monitorable, category);
        }

        /// <summary>
        /// Logs the specified message as the Monitorable level. Parameters are added to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        /// <param name="parameters">The parameters.</param>
        public void Monitorable(string message, SPDiagnosticsCategory category, params object[] parameters)
        {
            this.Log(message, LogLevel.Monitorable, category, parameters);
        }

        /// <summary>
        /// Logs the specified message as the High level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        public void High(string message, SPDiagnosticsCategory category)
        {
            this.Log(message, LogLevel.High, category);
        }

        /// <summary>
        /// Logs the specified message as the High level. Parameters are added to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        /// <param name="parameters">The parameters.</param>
        public void High(string message, SPDiagnosticsCategory category, params object[] parameters)
        {
            this.Log(message, LogLevel.High, category, parameters);
        }

        /// <summary>
        /// Logs the specified message as the Medium level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        public void Medium(string message, SPDiagnosticsCategory category)
        {
            this.Log(message, LogLevel.Medium, category);
        }

        /// <summary>
        /// Logs the specified message as the Medium level. Parameters are added to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        /// <param name="parameters">The parameters.</param>
        public void Medium(string message, SPDiagnosticsCategory category, params object[] parameters)
        {
            this.Log(message, LogLevel.Medium, category, parameters);
        }

        /// <summary>
        /// Logs the specified message as the Verbose level.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        public void Verbose(string message, SPDiagnosticsCategory category)
        {
            this.Log(message, LogLevel.Verbose, category);
        }

        /// <summary>
        /// Logs the specified message as the Verbose level. Parameters are added to the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="category">Category.</param>
        /// <param name="parameters">The parameters.</param>
        public void Verbose(string message, SPDiagnosticsCategory category, params object[] parameters)
        {
            this.Log(message, LogLevel.Verbose, category, parameters);
        }

        #endregion Log Level Methods


        #region Dispose

        public void Dispose()
        {
            this.Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool calledByUser)
        {
            if (this._disposed)
            {
                return;
            }

            //TraceProvider.UnregisterTraceProvider();
            this._disposed = true;
        }

        ~Logger()
        {
            this.Dispose(false);
        }

        #endregion Dispose
    }
}