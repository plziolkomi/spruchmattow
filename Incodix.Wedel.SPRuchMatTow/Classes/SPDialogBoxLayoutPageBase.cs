﻿using Microsoft.SharePoint.WebControls;
using System;
using System.Globalization;

namespace Incodix.Wedel.SPRuchMatTow.Classes.UI.SharePoint.WebControls
{
    public abstract class SPDialogBoxLayoutPageBase : LayoutsPageBase
    {
        protected void EndOperation(int result, string returnValue)
        {
            string closeModal = String.Format(CultureInfo.InvariantCulture, "<script type=\"text/javascript\">window.frameElement.commonModalDialogClose({0}, '{1}');</script>", new object[] { result, returnValue });
            this.Page.ClientScript.RegisterStartupScript(base.GetType(), "CreatePopup", closeModal, false);
        }

    }
}
