﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incodix.Wedel.SPRuchMatTow.Classes
{
    public delegate void AddEventHandlerToSPDialogEvent(object sender, SPDialogEventHandler e);

    public class SPDialogEventHandler : EventArgs
    {
        public int dialogResult { get; set; } // 0 or 1
        public string ReturnValues { get; set; } // can be url or any success/error message
        public SPDialogEventHandler(int result, string list)
        {
            ReturnValues = list;
            dialogResult = result;
        }
    }
}
