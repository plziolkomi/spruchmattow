﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Incodix.Wedel.SPRuchMatTow.Classes
{
    /// <summary>
    /// Specify the log level
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// The Unassigned log level. Use this level to log initialize LogLevel enum. 
        /// </summary>
        Unassigned = 0,
        /// <summary>
        /// The Unexpected log level. Use this level to log the unepected or error cases. 
        /// </summary>
        Unexpected = 10,
        /// <summary>
        /// The Monitorable log level. Use this level to log the monitarable or warning cases.
        /// </summary>
        Monitorable = 15,
        /// <summary>
        /// The High log level. Use this level to log the important info.
        /// </summary>
        High = 20,
        /// <summary>
        /// The Medium log level. Use this level to log the average important info.
        /// </summary>
        Medium = 50,
        /// <summary>
        /// The Verbose log level. Use this level to log an info.
        /// </summary>
        Verbose = 100
    }
}