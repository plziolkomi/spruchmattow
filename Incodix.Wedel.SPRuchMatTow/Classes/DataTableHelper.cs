﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml.Linq;

namespace Incodix.Wedel.SPRuchMatTow.Classes
{
    public static class DataTableHelper
    {
        public static DataTable ToDataTable(this XElement element)
        {
            DataSet ds = new DataSet();
            string rawXml = element.ToString();
            ds.ReadXml(new StringReader(rawXml));
            return ds.Tables[0];
        }


        public static DataTable ToDataTable(this IEnumerable<XElement> elements)
        {
            return ToDataTable(new XElement("Root", elements));
        }
    }
}
