﻿using System;
using System.Collections.Generic;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Navigation;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.WebPartPages;
using Microsoft.SharePoint.Administration;
using System.Xml;
using System.IO;

namespace Incodix.Wedel.SPRuchMatTow.Classes
{
    public class FeatureDeploymentHelper
    {
        #region Properties 

        static Logger LogWriter = Logger.Create("kategoria", "FeatureDeploymentHelper");
        static LogLevel lvlUnexp = LogLevel.Unexpected;
        static LogLevel lvlVerbose = LogLevel.Verbose;
        static SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("FeatureDeploymentHelper", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        static SPDiagnosticsCategory categVerbose = new SPDiagnosticsCategory("FeatureDeploymentHelper", TraceSeverity.Verbose, EventSeverity.Verbose);
        private static SPNavigationNode nodeHeader;
        private static SPNavigationNode nodeItem;

        #endregion

        #region Constructors

        public FeatureDeploymentHelper()
        {
        }

        #endregion Constructors

        #region PrivateMethods
        public static void AssignGroup(SPWeb currentWeb, SPList list, string GroupName, string RoleDefinition)
        {
            try
            {
                currentWeb.AllowUnsafeUpdates = true;
                SPRoleDefinition Role = currentWeb.RoleDefinitions[RoleDefinition];
                SPGroup groupToAssign = currentWeb.SiteGroups[GroupName];
                SPRoleAssignment roleAssingment = new SPRoleAssignment(groupToAssign);
                roleAssingment.RoleDefinitionBindings.Add(Role);
                list.RoleAssignments.Add(roleAssingment);
                currentWeb.AllowUnsafeUpdates = false;
                LogWriter.Log("Role definition " + RoleDefinition + " assigned to List with Url: " + list.DefaultViewUrl, lvlVerbose, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to assign role definition " + RoleDefinition + " to List with Url: " + list.DefaultViewUrl + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
        }
        public void CreateGroup(SPWeb currentWeb, string groupname, string groupdescription, string roledefinition)
        {
            List<string> groupsinweb = new List<string>();
            foreach (SPGroup group in currentWeb.SiteGroups)
            {
                try
                {
                    groupsinweb.Add(group.Name);
                    LogWriter.Log("SPGroup with name " + group.Name + " successfully added to <List>", lvlVerbose, categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Unable to add group: " + group.Name + " to currentWeb due to: " + ex.Message, lvlUnexp, categUnexp);
                }
            }
            if (!groupsinweb.Contains(groupname))
            {
                try
                {
                    currentWeb.AllowUnsafeUpdates = true;
                    currentWeb.SiteGroups.Add(groupname, currentWeb.CurrentUser as SPMember, currentWeb.CurrentUser, groupdescription);
                    LogWriter.Log("SPGroup with name " + groupname + " successfully added to Site", lvlVerbose, categVerbose);
                    SPGroup newgroup = currentWeb.SiteGroups[groupname];
                    SPRoleDefinition role = currentWeb.RoleDefinitions[roledefinition];
                    SPRoleAssignment roleAssignment = new SPRoleAssignment(newgroup);
                    roleAssignment.RoleDefinitionBindings.Add(role);
                    currentWeb.RoleAssignments.Add(roleAssignment);
                    currentWeb.AllowUnsafeUpdates = true;
                    currentWeb.Update();
                    LogWriter.Log("SPGroup with name " + groupname + " successfully configured to Site", lvlVerbose, categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Unable to assign role def " + roledefinition + " to group " + groupname + " due to: " + ex.Message, lvlUnexp, categUnexp);
                }
            }
        }
        public void AddQuickLaunchItem(SPWeb currentWeb, string header, string headerUrl, string item, string url)
        {
            try
            {
                currentWeb.AllowUnsafeUpdates = true;
                nodeHeader = null;
                nodeItem = null;
                SPNavigationNodeCollection quickLaunch = currentWeb.Navigation.QuickLaunch;
                // try to get quick launch header
                foreach (SPNavigationNode node in quickLaunch)
                {
                    if (node.Title == header)
                    {
                        nodeHeader = node;
                    }
                }
                //if header not found create it
                if (nodeHeader == null)
                {
                    nodeHeader = quickLaunch.AddAsLast(new SPNavigationNode(header, currentWeb.Url));
                }
                nodeHeader.Update();
                LogWriter.Log("QuickLaunch Panel " + header + " successfully added to Quicklaunch", lvlVerbose, categVerbose);

                //try to get node item under header
                SPNavigationNodeCollection childNodes = nodeHeader.Children;
                foreach (SPNavigationNode childNode in childNodes)
                {
                    if (childNode.Title == header)
                    {
                        nodeItem = childNode;
                    }
                }
                //If item not found under heading then create it
                if (nodeItem == null)
                {
                    nodeItem = nodeHeader.Children.AddAsLast(new SPNavigationNode(item, url));
                }
                else
                {
                    nodeItem.Url = url;
                }
                nodeItem.Update();
                nodeHeader.Update();
                currentWeb.AllowUnsafeUpdates = false;
                LogWriter.Log("QuickLaunch link " + item + " successfully added to Panel " + header, lvlVerbose, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to add QuickLaunch Link with name " + item + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
        }
        public void QuickLaunchDeleter(SPWeb currentWeb, string[] QuickLaunchPanels)
        {
            uint language = currentWeb != null ? currentWeb.Language : 1045;
            SPNavigationNodeCollection quickLaunch = currentWeb.Navigation.QuickLaunch;
            for (int i = quickLaunch.Count - 1; i >= 0; i--)
            {
                try
                {
                    if (Array.IndexOf(QuickLaunchPanels, quickLaunch[i].Title) > -1)
                    {
                        currentWeb.AllowUnsafeUpdates = true;
                        currentWeb.Navigation.QuickLaunch[i].Delete();
                        currentWeb.AllowUnsafeUpdates = false;
                        LogWriter.Log("QuickLaunch Panel " + quickLaunch[i].Title + " deleted.", lvlVerbose, categVerbose);
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Unable to delete QuickLaunch Links due to: " + ex.Message, lvlUnexp, categUnexp);
                }
            }
            currentWeb.Update();
        }
        public void SetUniquePermissionsForList(SPWeb currentWeb, SPList list, bool inherit, string[,] groupsAndPerms)
        {
            currentWeb.AllowUnsafeUpdates = true;
            list.BreakRoleInheritance(inherit);
            for (int i = 0; i < groupsAndPerms.GetLength(0); i++)
            {
                try
                {
                    currentWeb.AllowUnsafeUpdates = true;
                    AssignGroup(currentWeb, list, groupsAndPerms[i, 0], groupsAndPerms[i, 1]);
                    LogWriter.Log("Unique role assignment  set up properly for SPList " + list.Title, lvlVerbose, categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Log("Unable to Assign SPGroup to List: " + list.Title + " due to: " + ex.Message, lvlUnexp, categUnexp);
                }
            }
            currentWeb.AllowUnsafeUpdates = false;
        }
        public void SetFormCSR(SPWeb currentWeb, SPList list, String FormName, string linkToJS)
        {
            string listUrl = list.RootFolder.Url;
            string fileURL = listUrl + "/" + FormName;
            SPFile page = currentWeb.GetFile(fileURL);
            using (SPLimitedWebPartManager lwpm = page.GetLimitedWebPartManager(PersonalizationScope.Shared))
            {
                SPFile file = lwpm.Web.GetFile(fileURL);
                try
                {
                    // Enable the Update                                        
                    currentWeb.AllowUnsafeUpdates = true;
                    // Check out the file, if not checked out                                        
                    //if (file.CheckOutType == SPFile.SPCheckOutType.None)
                    file.CheckOut();
                    // Find the ListFormWebPart and Update the Template Name Property                                        
                    foreach (System.Web.UI.WebControls.WebParts.WebPart wp in lwpm.WebParts)
                    {
                        if (wp is Microsoft.SharePoint.WebPartPages.ListFormWebPart)
                        {
                            Microsoft.SharePoint.WebPartPages.ListFormWebPart ListFormwp =
                                (Microsoft.SharePoint.WebPartPages.ListFormWebPart)wp.WebBrowsableObject;
                            ListFormwp.JSLink = currentWeb.Url + linkToJS;
                            ListFormwp.CSRRenderMode = CSRRenderMode.StandardLayout;
                            lwpm.SaveChanges(ListFormwp);
                            file.CheckIn("CSR registered");
                        }
                    }
                    currentWeb.AllowUnsafeUpdates = false;
                }
                catch
                {
                    file.UndoCheckOut();
                }
                finally
                {
                    if (file.CheckOutType != SPFile.SPCheckOutType.None)
                    {
                        file.CheckIn("CSR registered");

                    }
                }
            }
        }
        public void SetViewCSR(SPWeb currentWeb, SPList list, string linktoJS)
        {
            foreach (SPView view in list.Views)
            {
                SPFile viewPage = currentWeb.GetFile(view.Url);
                using (SPLimitedWebPartManager lwpm = viewPage.GetLimitedWebPartManager(PersonalizationScope.Shared))
                {
                    SPFile file = lwpm.Web.GetFile(view.Url);
                    try
                    {
                        currentWeb.AllowUnsafeUpdates = true;
                        // Check out the file, if not checked out                                        
                        //if (file.CheckOutType == SPFile.SPCheckOutType.None)
                        file.CheckOut();
                        // Find the ListFormWebPart and Update the Template Name Property                                        
                        foreach (System.Web.UI.WebControls.WebParts.WebPart wp in lwpm.WebParts)
                        {
                            if (wp is Microsoft.SharePoint.WebPartPages.XsltListViewWebPart)
                            {
                                Microsoft.SharePoint.WebPartPages.XsltListViewWebPart ListFormwp =
                                    (Microsoft.SharePoint.WebPartPages.XsltListViewWebPart)wp.WebBrowsableObject;
                                ListFormwp.JSLink = currentWeb.Url + linktoJS;
                                lwpm.SaveChanges(ListFormwp);
                                file.CheckIn("CSR registered");
                            }
                        }
                        currentWeb.AllowUnsafeUpdates = false;
                    }
                    catch
                    {
                        file.UndoCheckOut();
                    }
                    finally
                    {
                        if (file.CheckOutType != SPFile.SPCheckOutType.None)
                        {
                            file.CheckIn("CSR registered");

                        }
                    }
                }
            }
        }
        public void CreateListInstances(SPWeb currentWeb, string[,] listsToCreate, uint language)
        {
            SPListCollection _lists = currentWeb.Lists;
            for (int i = 0; i < listsToCreate.GetLength(0); i++)
            {
                try
                {
                    string listTitle = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow," + listsToCreate[i, 0], null, language);
                    SPList currentList = _lists.TryGetList(listTitle);
                    if (currentList == null)
                    {
                        currentWeb.AllowUnsafeUpdates = true;
                        SPListTemplate _Templ = currentWeb.ListTemplates[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow," + listsToCreate[i, 1], null, language)];
                        Guid newListGuid = currentWeb.Lists.Add(listsToCreate[i, 0], "", _Templ);
                        LogWriter.Log("FeatureDeploymentHelper created new List with Guid: " + newListGuid, lvlVerbose, categVerbose);

                        SPList newList = currentWeb.GetList(currentWeb.Url + "/Lists/" + listsToCreate[i, 0]);
                        newList.Title = listTitle;
                        newList.Update();
                        currentWeb.AllowUnsafeUpdates = false;
                        LogWriter.Log("FeatureDeploymentHelper updated List Title", lvlVerbose, categVerbose);
                    }
                }
                catch (Exception ex)
                {
                    LogWriter.Log("FeatureDeploymentHelper is unable to create List due to: " + ex.Message, lvlUnexp, categUnexp);
                }
            }
        }

        public void AddWebPartToForm(SPWeb currentWeb, SPList list, String WebPartLink, String FormName)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate ()
            {
                string listUrl = list.RootFolder.Url;
                string fileURL = listUrl + "/" + FormName;
                SPFile page = currentWeb.GetFile(fileURL);
                using (SPLimitedWebPartManager lwpm = page.GetLimitedWebPartManager(PersonalizationScope.Shared))
                {
                    SPFile file = lwpm.Web.GetFile(fileURL);
                    try
                    {
                        // Enable the Update                                        
                        lwpm.Web.AllowUnsafeUpdates = true;
                        // Check out the file, if not checked out                                                               
                        if (file.CheckOutType != SPFile.SPCheckOutType.None)
                        {
                            file.UndoCheckOut();
                            file.CheckOut();
                        }
                        else
                        {
                            file.CheckOut();
                        }
                        // Dodaje nowy WebPart 
                        string err = string.Empty;
                        string WebPartUrl = currentWeb.Url + WebPartLink;
                        XmlTextReader reader = new XmlTextReader(new StringReader(currentWeb.GetFileAsString(WebPartUrl)));
                        System.Web.UI.WebControls.WebParts.WebPart NewWebPart = lwpm.ImportWebPart(reader, out err);

                        foreach (System.Web.UI.WebControls.WebParts.WebPart wp in lwpm.WebParts)
                        {
                            if (wp is WebParts.GoodsMovWebPart.GoodsMovWebPart)
                            {
                                lwpm.CloseWebPart(wp);
                                lwpm.SaveChanges(wp);
                            }
                        }
                        lwpm.AddWebPart(NewWebPart, "Main", 0);
                        // Update the file                                        
                        file.Update();
                        file.CheckIn("System Update", SPCheckinType.OverwriteCheckIn);
                        // Disable the Unsafe Update                                        
                        lwpm.Web.AllowUnsafeUpdates = false;
                    }
                    catch
                    {

                    }
                    finally
                    {
                        if (file.CheckOutType != SPFile.SPCheckOutType.None)
                        {
                            file.CheckIn("System Update", SPCheckinType.OverwriteCheckIn);
                        }
                    }
                }
            });
        }

        public void CreateLookupField(SPWeb currentWeb, SPList destinationList, SPList lookupList, string fieldName, string fieldGroup, string lookupField, bool mult)
        {
            try
            {
                if (!destinationList.Fields.ContainsField(fieldName))
                {
                    currentWeb.AllowUnsafeUpdates = true;
                    string fieldID = destinationList.Fields.AddLookup(fieldName, lookupList.ID, true);
                    SPFieldLookup lookupFld = (SPFieldLookup)destinationList.Fields[fieldID];
                    lookupFld.Required = true;
                    lookupFld.Title = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow," + fieldName, null, currentWeb.Language);
                    lookupFld.LookupField = lookupList.Fields[SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow," + lookupField, null, currentWeb.Language)].InternalName;
                    if (mult)
                    {
                        lookupFld.AllowMultipleValues = true;
                    }
                    lookupFld.Group = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow" + fieldGroup, null, currentWeb.Language);
                    lookupFld.Update();
                    currentWeb.AllowUnsafeUpdates = false;
                    LogWriter.Log("FeatureDeploymentHelper created lookup Field " + fieldName + " on SPList " + destinationList.Title, lvlVerbose, categVerbose);
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("FeatureDeploymentHelper is unable to create lookup Field " + fieldName + " on SPList " + destinationList.Title + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
        }
        public void DisableTitleField(SPWeb currentWeb, SPList currentList, string fieldDefaultValue)
        {
            try
            {
                currentWeb.AllowUnsafeUpdates = true;
                SPField fldTitle = currentList.Fields.GetFieldByInternalName("Title");
                fldTitle.DefaultValue = fieldDefaultValue;
                fldTitle.Update();

                currentList.Update();
                currentWeb.AllowUnsafeUpdates = false;
                LogWriter.Log("FeatureDeploymentHelper updated Field 'Title' on SPList " + currentList.Title, lvlVerbose, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("FeatureDeploymentHelper is unable to update Field 'Title' on SPList " + currentList.Title + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
        }
        #endregion
    }

}
