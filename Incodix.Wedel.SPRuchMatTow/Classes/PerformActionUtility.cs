﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Utilities;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Security.Cryptography;

namespace Incodix.Wedel.SPRuchMatTow.Classes
{
    class PerformActionUtility
    {
        #region Properties 

        static Logger LogWriter = Logger.Create("kategoria", "PerformActionUtility");
        static LogLevel lvlUnexp = LogLevel.Unexpected;
        static LogLevel lvlVerbose = LogLevel.Verbose;
        static SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("PerformActionUtility", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        static SPDiagnosticsCategory categVerbose = new SPDiagnosticsCategory("PerformActionUtility", TraceSeverity.Verbose, EventSeverity.Verbose);

        #endregion

        #region Constructors
        public PerformActionUtility()
        {
        }

        #endregion

        #region Public Methods

        public void UniqueRoleAssignment(SPWeb currentWeb, SPListItem currentItem, string roleSet)
        {
            uint language = currentWeb != null ? currentWeb.Language : 1045;
            string userString = string.Empty;

            // Get permissions levels
            string permAdmin = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,permAdmin", null, language);
            string permEdit = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,permEdit", null, language);
            string permDisp = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,permDisp", null, language);
            SPRoleDefinition AdminRole = currentWeb.RoleDefinitions[permAdmin];
            SPRoleDefinition editRole = currentWeb.RoleDefinitions[permEdit];
            SPRoleDefinition dispRole = currentWeb.RoleDefinitions[permDisp];

            SPPrincipal Requestor;
            SPPrincipal Approver;

            // Get Group Names
            string groupRequestors = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Requestors", null, language);
            string groupApprovers = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Approvers", null, language);
            string groupReceptionists = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Receptionists", null, language);
            string groupAdministrators = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Administrators", null, language);
            string groupMailApprovers = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,MailApprovers", null, language);

            currentWeb.AllowUnsafeUpdates = true;
            if (currentItem.HasUniqueRoleAssignments == true)
            {
                currentItem.ResetRoleInheritance();
                currentWeb.AllowUnsafeUpdates = true;
                currentItem.BreakRoleInheritance(false);
                currentWeb.AllowUnsafeUpdates = true;
            }
            else
            {
                currentItem.BreakRoleInheritance(false);
                currentWeb.AllowUnsafeUpdates = true;
            }
            LogWriter.Verbose("Unique permissions set up for item with ID " + currentItem.ID, categVerbose);

            // Add Administrator Group to Item
            try
            {
                SPGroup groupToAssign = currentWeb.SiteGroups[groupAdministrators];
                SPRoleAssignment GroupRoleAssign = new SPRoleAssignment(groupToAssign);
                GroupRoleAssign.RoleDefinitionBindings.Add(AdminRole);
                currentItem.RoleAssignments.Add(GroupRoleAssign);
                LogWriter.Verbose("Group " + groupAdministrators + " is successfully added to unique permissions of item with ID " + currentItem.ID, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("Group " + groupAdministrators + " is not added to unique permissions of item with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
            // Add Mail Approvers Group to Item
            try
            {
                SPGroup groupToAssign = currentWeb.SiteGroups[groupMailApprovers];
                SPRoleAssignment GroupRoleAssign = new SPRoleAssignment(groupToAssign);
                GroupRoleAssign.RoleDefinitionBindings.Add(editRole);
                currentItem.RoleAssignments.Add(GroupRoleAssign);
                LogWriter.Verbose("Group " + groupMailApprovers + " is successfully added to unique permissions of item with ID " + currentItem.ID, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("Group " + groupMailApprovers + " is not added to unique permissions of item with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
            // Add Recepcionist Group to Item
            try
            {
                SPGroup groupToAssign = currentWeb.SiteGroups[groupReceptionists];
                SPRoleAssignment GroupRoleAssign = new SPRoleAssignment(groupToAssign);
                GroupRoleAssign.RoleDefinitionBindings.Add(dispRole);
                currentItem.RoleAssignments.Add(GroupRoleAssign);
                LogWriter.Verbose("Group " + groupReceptionists + " is successfully added to unique permissions of item with ID " + currentItem.ID, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("Group " + groupReceptionists + " is not added to unique permissions of item with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
            // Add Requestor User To Item
            try
            {
                if (currentItem.Fields.ContainsField("Author") && currentItem["Author"] != null)
                {
                    userString = currentItem["Author"].ToString();
                    SPFieldUserValue EmployeeValue = new SPFieldUserValue(currentWeb, userString);
                    if (EmployeeValue != null)
                    {
                        Requestor = EmployeeValue.User;
                        SPRoleAssignment UserRoleAssignment = new SPRoleAssignment(Requestor);
                        if (roleSet == "editRole")
                        {
                            UserRoleAssignment.RoleDefinitionBindings.Add(editRole);
                        }
                        else
                        {
                            UserRoleAssignment.RoleDefinitionBindings.Add(dispRole);
                        }
                        currentItem.RoleAssignments.Add(UserRoleAssignment);
                        currentWeb.Groups[groupRequestors].AddUser(EmployeeValue.User);
                        LogWriter.Verbose("Requestor is successfully added to unique permissions of item with ID " + currentItem.ID, categVerbose);
                    }
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("Requestor is not added to unique permissions of item with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
            // Add Approver User To Item
            try
            {
                if (currentItem.Fields.ContainsField("Approver") && currentItem["Approver"] != null)
                {
                    userString = currentItem["Approver"].ToString();
                    SPFieldUserValue ApproverValue = new SPFieldUserValue(currentWeb, userString);
                    if (ApproverValue != null)
                    {
                        Approver = ApproverValue.User;
                        SPRoleAssignment UserRoleAssignment = new SPRoleAssignment(Approver);
                        UserRoleAssignment.RoleDefinitionBindings.Add(dispRole);
                        currentItem.RoleAssignments.Add(UserRoleAssignment);
                        currentWeb.Groups[groupApprovers].AddUser(ApproverValue.User);
                        LogWriter.Verbose("Approver is successfully added to unique permissions of item with ID " + currentItem.ID, categVerbose);
                    }
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("Requestor is not added to unique permissions of item with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
        }
        public void RemoveMailApproverGroup(SPWeb currentWeb, SPListItem currentItem)
        {
            uint language = currentWeb != null ? currentWeb.Language : 1045;
            string groupMailApprovers = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,MailApprovers", null, language);
            bool unsafeUpdates = currentWeb.AllowUnsafeUpdates;
            // Add Mail Approvers Group to Item
            try
            {
                currentWeb.AllowUnsafeUpdates = true;
                SPPrincipal mailApproversSPGroup = currentWeb.SiteGroups[groupMailApprovers];
                currentItem.RoleAssignments.Remove(mailApproversSPGroup);
                
                LogWriter.Verbose("Group " + groupMailApprovers + " is successfully added to unique permissions of item with ID " + currentItem.ID, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("Group " + groupMailApprovers + " is not added to unique permissions of item with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
            finally
            {
                currentWeb.AllowUnsafeUpdates = unsafeUpdates;
            }
        }
        public void GetTemplateAndSendMail(SPWeb currentWeb, SPListItem currentItem, SPUser mailTo, SPUser PerformedBy, string templateName, string customComments)
        {
            try
            {
                // Get template Title and Body
                SPQuery query = new SPQuery();
                query.Query = string.Concat(@"<Where>
                                                <Eq>
                                                    <FieldRef Name='Title' />
                                                    <Value Type='Text'>" + templateName + @"</Value>
                                                </Eq>
                                            </Where>");
                query.ViewFields = string.Concat(
                                    "<FieldRef Name='ID' />",
                                    "<FieldRef Name='MessageTitle' />",
                                    "<FieldRef = Name='MailMessageBody' />");
                SPListItemCollection availTemplates = currentWeb.GetList(currentWeb.Url + "/Lists/MailTemplatesList").GetItems(query);
                foreach(SPListItem template in availTemplates)
                {
                    LogWriter.Verbose("GetTemplateAndSendMail is preparing Email based on template " + templateName + " for item with ID " + currentItem.ID, categVerbose);
                    // Get data from template and send email
                    string finalMailTitle = template["MessageTitle"].ToString();
                    // Replace Signature Token in Mail title
                    if (currentItem.Fields.ContainsField("Signature") && currentItem["Signature"] != null)
                    {
                        finalMailTitle = finalMailTitle.Replace("$Signature$", currentItem["Signature"].ToString()); 
                    }
                    else
                    {
                        finalMailTitle = finalMailTitle.Replace("nr $Signature$ ", "");
                    }
                    // Prapare values for Mail Body
                    string Created = currentItem[SPBuiltInFieldId.Created] != null ? DateTime.Parse(currentItem[SPBuiltInFieldId.Created].ToString()).ToString("yyyy-MM-dd hh:mm") : DateTime.Now.ToString("yyyy-MM-dd hh:mm");
                    string RequestFor = currentItem["RequestFor"] != null ? currentItem["RequestFor"].ToString() : string.Empty;
                    string CategoryLookup = GetLookupFieldValue(currentWeb, currentItem, "CategoryLookup");
                    string RequestorVehicle = currentItem["RequestorVehicle"] != null ? currentItem["RequestorVehicle"].ToString() : "n/a";
                    string RequestedGoods = PrepareHTMLTable(currentWeb, currentItem);
                    string PurposeOfGoodsMovement = currentItem["PurposeOfGoodsMovement"] != null ? currentItem["PurposeOfGoodsMovement"].ToString() : string.Empty;
                    string LinkToApprove = !string.IsNullOrEmpty(GenerateActionLink(currentWeb, currentItem, "a")) ? "<a href='" + GenerateActionLink(currentWeb, currentItem, "a") + "'>Kliknij, aby zatwierdzić przepustkę</a>" : "";
                    string LinkToReject = !string.IsNullOrEmpty(GenerateActionLink(currentWeb, currentItem, "r")) ? "<a href='" + GenerateActionLink(currentWeb, currentItem, "r") + "'>Kliknij, aby odrzucić przepustkę</a>" : "";
                    string UserComments = (customComments != "Dodatkowy komentarz ..." && customComments != "") ? " z komentarzem: " + customComments + "<br />" : " ";

                    // Replace HTML Body Tokens
                    string finalMailBody = template["MailMessageBody"].ToString();
                    finalMailBody = finalMailBody.Replace("$PerformedBy$", PerformedBy.Name)
                                 .Replace("$UserComments$", UserComments)
                                 .Replace("$Created$", Created)
                                 .Replace("$RequestFor$", RequestFor)
                                 .Replace("$CategoryLookup$", CategoryLookup)
                                 .Replace("$RequestorVehicle$", RequestorVehicle)
                                 .Replace("$RequestedGoods$", RequestedGoods)
                                 .Replace("$PurposeOfGoodsMovement$", PurposeOfGoodsMovement)
                                 .Replace("$LinkToApprove$", LinkToApprove)
                                 .Replace("$LinkToReject$", LinkToReject)
                                 .Replace("$LinkToForm$", "<a href='" + currentWeb.Url + "/Lists/" + currentItem.ParentList.RootFolder.Name + "/DispForm.aspx?ID=" + currentItem.ID + "'>Link do formularza wniosku</a>")
                                 .Replace("$LinkToSite$", "<a href='" + currentWeb.Url + "'>Link do modułu przepustek materiałowo-towarowych</a>");

                    //SPUtility.SendEmail(currentWeb, true, false, mailTo.Email, finalMailTitle, finalMailBody);
                    SPUtility.SendEmail(currentWeb, true, false, mailTo.Email, finalMailTitle, finalMailBody);
                    LogWriter.Verbose("GetTemplateAndSendMail successfully sent email for item with ID " + currentItem.ID, categVerbose);
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to Send EMail based on template " + templateName + " for ListItem with ID " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
        }
        private string GetLookupFieldValue(SPWeb currentWeb, SPListItem currentItem, string lookupFieldName)
        {
            string lookupValue = string.Empty;
            try
            {
                if (currentItem.Fields.ContainsField(lookupFieldName) && currentItem[lookupFieldName] != null)
                {
                    SPFieldLookupValue userValue = new SPFieldLookupValue(currentItem[lookupFieldName].ToString());
                    lookupValue = userValue.LookupValue;
                    LogWriter.Verbose("PerformActionUtility successfully extracted lookupFieldValue from column " + lookupFieldName + " for item with ID: " + currentItem.ID, categVerbose);
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("PerformActionUtility is unable to extract lookupFieldValue from column " + lookupFieldName + " for item with ID: " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
            return lookupValue;
        }
        public string SetSignature(SPWeb currentweb, SPListItem currentItem)
        {
            string result = string.Empty;
            try
            {
                // Generate signature
                bool exist = true;
                string _agreementID = string.Empty;
                int _incrementator = 1;
                do
                {
                    if (currentItem.Fields.ContainsField("Signature") && currentItem["Signature"] == null)
                    {
                        // Get listitem data
                        DateTime _DateCreated = DateTime.Parse(currentItem[SPBuiltInFieldId.Created].ToString());

                        SPQuery query = new SPQuery();
                        query.Query = @"<Where>
                                             <Eq>
                                                <FieldRef Name='CreationYear' />
                                                <Value Type='Text'>" + _DateCreated.Year + @"</Value>
                                             </Eq>
                                       </Where>";
                        SPListItemCollection formsThisYear = currentItem.ParentList.GetItems(query);
                        string AgreementCount = formsThisYear.Count > 0 ? Convert.ToString(formsThisYear.Count + _incrementator) : _incrementator.ToString();

                        // add zeros to ID to get 5 number string
                        while (AgreementCount.Length < 5)
                        {
                            AgreementCount = "0" + AgreementCount;
                        }

                        _agreementID = _DateCreated.Year + "/" + AgreementCount;

                        SPQuery existitemQuery = new SPQuery();
                        existitemQuery.Query = @"<Where>
                                              <Eq>
                                                 <FieldRef Name='Signature' />
                                                 <Value Type='Text'>" + _agreementID + @"</Value>
                                              </Eq>
                                           </Where>";
                        SPListItemCollection existingProjects = currentItem.ParentList.GetItems(existitemQuery);
                        if (existingProjects.Count == 0)
                        {
                            exist = false;
                        }
                        else
                        {
                            _incrementator++;
                        }
                    }
                } while (exist);
                LogWriter.Log("Field 'Signature' for form with ID " + currentItem.ID + " on list " + currentItem.ParentList.Title + " is successfully updated", lvlVerbose, categVerbose);
                result = _agreementID;
            }
            catch (Exception ex)
            {
                LogWriter.Log("Field 'Signature' for form with ID " + currentItem.ID + " on list " + currentItem.ParentList.Title + " failes saving process due to " + ex.Message, lvlUnexp, categUnexp);
            }
            return result;
        }
        public void PerformWorkflowAction(SPWeb currentWeb, SPWeb elevatedWeb, SPFieldUserValue userVal, SPListItem currentItem, string FormAction, string AdditionalComments)
        {
            string currentFormState = currentItem["RequestState"].ToString();
            // Approve Action
            if (currentFormState == "Oczekiwanie na akceptacje" && FormAction == "Approve")
            {
                currentItem["RequestState"] = "Wniosek zaakceptowany. Do potwierdzenia na recepcji";

                if (currentItem.Fields.ContainsField("ApprovalDate"))
                    currentItem["ApprovalDate"] = DateTime.Now.ToString("yyyy-MM-dd hh:mm");

                if (currentItem.Fields.ContainsField("ApproverComments"))
                    currentItem["ApproverComments"] = AdditionalComments;

                if (currentItem.Fields.ContainsField("Approver"))
                    currentItem["Approver"] = userVal;

                //SPFieldUserValue _Requestor = new SPFieldUserValue(elevatedWeb, currentItem["Author"].ToString());
                //GetTemplateAndSendMail(elevatedWeb, currentItem, _Requestor.User, userVal.User, "RequestApprovedTemplate", AdditionalComments);

                //RemoveMailApproverGroup(elevatedWeb, currentItem);
            }
            // Reject Action
            else if (currentFormState == "Oczekiwanie na akceptacje" && FormAction == "Reject")
            {
                currentItem["RequestState"] = "Wniosek odrzucony";

                if (currentItem.Fields.ContainsField("ApproverComments"))
                    currentItem["ApproverComments"] = AdditionalComments;

                //SPFieldUserValue _Requestor = new SPFieldUserValue(elevatedWeb, currentItem["Author"].ToString());
                //GetTemplateAndSendMail(elevatedWeb, currentItem, _Requestor.User, userVal.User, "RequestRejectedTemplate", AdditionalComments);

                //RemoveMailApproverGroup(elevatedWeb, currentItem);
            }
            //ConfirmRelease Action
            else if (currentFormState == "Wniosek zaakceptowany. Do potwierdzenia na recepcji" && FormAction == "ConfirmRelease")
            {
                if (bool.Parse(currentItem["ToReturn"].ToString()))
                {
                    currentItem["RequestState"] = "Przedmioty wyniesione";
                }
                else
                {
                    currentItem["RequestState"] = "Wniosek zrealizowany";
                }

                if (currentItem.Fields.ContainsField("Receptionist"))
                    currentItem["Receptionist"] = userVal;

                if (currentItem.Fields.ContainsField("ReceptionistComments"))
                    currentItem["ReceptionistComments"] = AdditionalComments;

                if (currentItem.Fields.ContainsField("ClosureDate"))
                    currentItem["ClosureDate"] = DateTime.Now;

            }
            // ConfirmReturn Action
            else if (currentFormState == "Przedmioty wyniesione" && FormAction == "ConfirmReturn")
            {
                currentItem["RequestState"] = "Wniosek zrealizowany";
                if (currentItem.Fields.ContainsField("Receptionist"))
                    currentItem["Receptionist"] = userVal;

                if (currentItem.Fields.ContainsField("ReceptionistComments"))
                    currentItem["ReceptionistComments"] = AdditionalComments;

                if (currentItem.Fields.ContainsField("GoodsReturnDate"))
                    currentItem["GoodsReturnDate"] = DateTime.Now;

            }
            // ConfirmReleaseWithoutApproval Action
            else if (currentFormState == "Oczekiwanie na akceptacje" && FormAction == "ConfirmReleaseWithoutApproval")
            {
                currentItem["RequestState"] = "Zweryfikowane przez recepcję";
                if (currentItem.Fields.ContainsField("Receptionist"))
                    currentItem["Receptionist"] = userVal;

                if (currentItem.Fields.ContainsField("ReceptionistComments"))
                    currentItem["ReceptionistComments"] = AdditionalComments;

                if (currentItem.Fields.ContainsField("ClosedWithoutApproval"))
                    currentItem["ClosedWithoutApproval"] = true;

                if (currentItem.Fields.ContainsField("ClosureDate"))
                    currentItem["ClosureDate"] = DateTime.Now;

                SPFieldUserValue _Approver = new SPFieldUserValue(elevatedWeb, currentItem["Approver"].ToString());
                GetTemplateAndSendMail(elevatedWeb, currentItem, _Approver.User, userVal.User, "ReleasedWithoutApprovalTemplate", AdditionalComments);
            }
        }
        private string PrepareHTMLTable(SPWeb currentWeb, SPListItem currentItem)
        {
            string goodsTable = string.Empty;
            try
            {
                DataTable dt = RequestedGoodsGridNoData();
                if (currentItem.Fields.ContainsField("RequestedGoods") && currentItem["RequestedGoods"] != null)
                {
                    XDocument requestedGoodsXML = XDocument.Parse(currentItem["RequestedGoods"].ToString());
                    var booksQuery = from books in requestedGoodsXML.Elements("NewDataSet").Elements("Table1")
                                     select books;

                    dt = booksQuery.ToDataTable();
                    LogWriter.Log("PerformActionUtility get currentItem XML data for RequestedGoods DT", lvlVerbose, categVerbose);
                }
                if(dt.Rows.Count > 0)
                {
                    LogWriter.Log("PerformActionUtility started creating HTML table based on RequestedGoods DT", lvlVerbose, categVerbose);
                    StringBuilder rgTable = new StringBuilder();
                    rgTable.Append("<table style='width:60%;padding:3px;border:1px solid;'>" +
                                       "<tr style='background-color:#f9f9f9'>" +
                                           "<th style='text-align:left;width:70%'>Nazwa:</th>" +
                                           "<th style='text-align:left;width:15%'>j.m.</th>" +
                                           "<th style='text-align:left;width:15%'>Ilość</th>" +
                                       "<tr>" +
                                       "%tableRows%" +                                     
                                   "</table>");
                    string tableRows = string.Empty;                    
                    foreach(DataRow dr in dt.Rows)
                    {
                        string tableRow = "<tr>" +
                                            "<td>%MaterialName%</td>" +
                                            "<td>%UnitOfMeasure%</td>" +
                                            "<td>%Quantity%</td>" +
                                          "<tr>";
                        if (!string.IsNullOrEmpty(dr["MaterialName"].ToString()))
                        {
                            tableRow = tableRow.Replace("%MaterialName%", dr["MaterialName"].ToString())
                                    .Replace("%UnitOfMeasure%", dr["UnitOfMeasure"].ToString())
                                    .Replace("%Quantity%", dr["Quantity"].ToString());

                            tableRows += tableRow; 
                        }
                    }
                    goodsTable = rgTable.Replace("%tableRows%", tableRows).ToString();
                }
                else
                {
                    goodsTable = "Brak danych do wyświetlenia";
                }
                LogWriter.Log("PerformActionUtility finished creating HTML " + dt.Rows.Count + " rows table based on RequestedGoods DT", lvlVerbose, categVerbose);
            }
            catch (Exception ex)
            {
                LogWriter.Log("PerformActionUtility is unable to extract gridView Table for item with ID: " + currentItem.ID + " due to: " + ex.Message, lvlUnexp, categUnexp);
            }
            return goodsTable;
        }
        private DataTable RequestedGoodsGridNoData()
        {
            DataTable dt = new DataTable();
            DataColumn ObjectiveIdRequestedMaterialId = new DataColumn();
            ObjectiveIdRequestedMaterialId.ColumnName = "RequestedMaterialId";
            ObjectiveIdRequestedMaterialId.DataType = Type.GetType("System.Int32");
            ObjectiveIdRequestedMaterialId.AutoIncrement = true;
            ObjectiveIdRequestedMaterialId.AutoIncrementSeed = 1;
            ObjectiveIdRequestedMaterialId.AutoIncrementStep = 1;
            ObjectiveIdRequestedMaterialId.ReadOnly = true;
            ObjectiveIdRequestedMaterialId.Unique = true;
            dt.Columns.Add(ObjectiveIdRequestedMaterialId);
            dt.Columns.Add("MaterialName", typeof(string));
            dt.Columns.Add("UnitOfMeasure", typeof(string));
            dt.Columns.Add("Quantity", typeof(string));
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            LogWriter.Log("PerformActionUtility prepared DataTable draft for RequestedGoods DT", lvlVerbose, categVerbose);
            return dt;
        }
        private string GenerateActionLink(SPWeb currentWeb, SPListItem currentItem, string typeOfDecision)
        {
            string actionLink = string.Empty;
            if (currentWeb.Properties.ContainsKey("spruchmattowdecisionsupportservice") && currentWeb.AllProperties["spruchmattowdecisionsupportservice"] != null)
            {
                string linkToService = currentWeb.AllProperties["spruchmattowdecisionsupportservice"].ToString();

                //Konieczne zabezpieczenie tego kodu
                if (currentItem.Fields.ContainsField("Modified") && currentItem[SPBuiltInFieldId.Modified] != null)
                {
                    DateTime itemModified = (DateTime)(currentItem[SPBuiltInFieldId.Modified]);
                    using (MD5 md5Hash = MD5.Create())
                    {
                        string hashSource = currentItem.ID.ToString("00000000") + currentItem.UniqueId.ToString().ToLower() + itemModified.ToUniversalTime().ToString("yyyyMMdd-HHmm");
                        //string hashSource = currentItem.ID.ToString("00000000") + currentItem.UniqueId.ToString() + itemModified.Ticks.ToString("00000000000000000000000");
                        string hashKey = GetMd5Hash(md5Hash, hashSource);
                        actionLink = linkToService + "/" + currentItem.ID + "/" + hashKey + "/" + typeOfDecision;
                    }

                    LogWriter.Verbose("PerformActionUtility successfully obtained decision support service address for item with ID " + currentItem.ID, categVerbose);
                }
            }
            else
            {
                LogWriter.Log("PerformActionUtility is unable obtain decision support service address for item with ID " + currentItem.ID, lvlUnexp, categUnexp);
            }
            return actionLink;
        }
        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        #endregion
    }
}
