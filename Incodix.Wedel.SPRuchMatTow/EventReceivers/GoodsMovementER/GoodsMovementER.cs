﻿using System;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using Microsoft.SharePoint.Workflow;
using Microsoft.SharePoint.Administration;
using Incodix.Wedel.SPRuchMatTow.Classes;

namespace Incodix.Wedel.SPRuchMatTow.EventReceivers.GoodsMovementER
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class GoodsMovementER : SPItemEventReceiver
    {
        #region Properties
        Logger LogWriter = Logger.Create("Configuration", "GoodsMovementER");
        SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("GoodsMovementER", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        SPDiagnosticsCategory categVerbose = new SPDiagnosticsCategory("GoodsMovementER", TraceSeverity.VerboseEx, EventSeverity.Verbose);
        private SPFieldUserValue _Requestor;
        private SPFieldUserValue _Approver;

        #endregion

        #region Overrides

        public override void ItemAdded(SPItemEventProperties properties)
        {
            base.ItemAdded(properties);
            //using (DisabledEventsScope zakres = new DisabledEventsScope())
            //{
            //    if(properties.CurrentUserId != properties.Site.SystemAccount.ID)
            //    {
            //        //if (properties.ListItem.Fields.ContainsField("Approver") && properties.ListItem["Approver"] != null)
            //        //{
            //        //    SPFieldUserValue approverValue = new SPFieldUserValue(properties.Web, properties.ListItem["Approver"].ToString());
            //        //    PerformActionUtility ActionUtility = new PerformActionUtility();
            //        //    ActionUtility.GetTemplateAndSendMail(properties.Web, properties.ListItem, approverValue.User, properties.Web.CurrentUser, "SendForApprovalTemplate", "");

            //        //}
            //    }
            //}
        }

        public override void ItemUpdated(SPItemEventProperties properties)
        {
            base.ItemUpdated(properties);
            using (DisabledEventsScope zakres = new DisabledEventsScope())
            {

                if (properties.List.RootFolder.Name == "GoodsMovementList")
                {
                    SPListItem currentItem = properties.ListItem;
                    PerformActionUtility actionUtility = new PerformActionUtility();
                    bool _exist = false;
                    string AdditionalComments = string.Empty;
                    string currentFormState = currentItem["RequestState"].ToString();
                    uint language = properties.Web != null ? properties.Web.Language : 1045;
                    // Approve Action
                    if (currentFormState == "Wniosek zaakceptowany. Do potwierdzenia na recepcji" || currentFormState == "Wniosek odrzucony")
                    {
                        string groupMailApprovers = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,MailApprovers", null, language);
                        foreach (SPGroup group in currentItem.RoleAssignments.Groups)
                        {
                            if (group.Name == groupMailApprovers)
                                _exist = true;
                        }
                        if (_exist)
                        {
                            LogWriter.Verbose("GoodsMovementER ItemUpdated started sending email to requestor of item with ID: " + currentItem.ID, categVerbose);

                            using (SPSite currentSite = new SPSite(properties.Site.Url, SPUserToken.SystemAccount))
                            {
                                using (SPWeb elevatedWeb = currentSite.OpenWeb())
                                {
                                    if (currentItem.Fields.ContainsField("ApproverComments") && currentItem["ApproverComments"] != null)
                                    {
                                        AdditionalComments = currentItem["ApproverComments"].ToString();
                                    }
                                    if (currentItem.Fields.ContainsField("Approver") && currentItem["Approver"] != null)
                                        _Approver = new SPFieldUserValue(elevatedWeb, currentItem["Approver"].ToString());

                                    if (currentItem.Fields.ContainsField("Author") && currentItem["Author"] != null)
                                        _Requestor = new SPFieldUserValue(elevatedWeb, currentItem["Author"].ToString());

                                    LogWriter.Verbose("GoodsMovementER ItemUpdated collected all required data to send email to requestor of item with ID: " + currentItem.ID, categVerbose);
                                    if (_Requestor != null && _Approver != null)
                                    {
                                        if (currentFormState == "Wniosek zaakceptowany. Do potwierdzenia na recepcji")
                                        {
                                            actionUtility.GetTemplateAndSendMail(elevatedWeb, currentItem, _Requestor.User, _Approver.User, "RequestApprovedTemplate", AdditionalComments);

                                        }
                                        else if (currentFormState == "Wniosek odrzucony")
                                        {
                                            actionUtility.GetTemplateAndSendMail(elevatedWeb, currentItem, _Requestor.User, _Approver.User, "RequestRejectedTemplate", AdditionalComments);
                                        }
                                        LogWriter.Verbose("GoodsMovementER ItemUpdated succesfully sent mail for item with ID: " + currentItem.ID, categVerbose);
                                    }
                                    else
                                    {
                                        LogWriter.Unexpected("GoodsMovementER ItemUpdated ER is unable to get Requestor and Approver of request with ID: " + currentItem.ID, categUnexp);
                                    }
                                    // Remove MailApprovers from listitem unique role assignments
                                    actionUtility.RemoveMailApproverGroup(elevatedWeb, currentItem);
                                }
                            }
                        }
                    }
                }
            }
        }


        #endregion
    }
}