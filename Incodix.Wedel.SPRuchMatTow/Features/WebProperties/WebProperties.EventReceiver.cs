using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Incodix.Wedel.SPRuchMatTow.Classes;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Utilities;

namespace Incodix.Wedel.SPRuchMatTow.Features.WebProperties
{
    [Guid("eaf135c3-788e-4a8b-ac9b-ee23a6bdea7c")]
    public class WebPropertiesEventReceiver : SPFeatureReceiver
    {
        #region Properties

        static Logger LogWriter = Logger.Create("kategoria", "WebPropertiesEventReceiver");
        static SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("WebPropertiesEventReceiver", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        static SPDiagnosticsCategory categVerbose = new SPDiagnosticsCategory("WebPropertiesEventReceiver", TraceSeverity.Verbose, EventSeverity.Verbose);

        static string _key = "spruchmattowdecisionsupportservice";
        static string linkToService = @"http://sp2013-dev/sites/GoodsMovementSite3";

        #endregion

        #region Overrides
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb currentWeb = properties.Feature.Parent as SPWeb;
            bool currUnsafeUpdates = currentWeb.AllowUnsafeUpdates;

            using (SPSite elevatedSite = new SPSite(currentWeb.Url, SPUserToken.SystemAccount))
            {
                using (SPWeb elevatedWeb = elevatedSite.OpenWeb())
                {
                    elevatedWeb.AllowUnsafeUpdates = true;
                    if (!elevatedWeb.AllProperties.ContainsKey(_key))
                        elevatedWeb.Properties.Add(_key, linkToService);

                    // update the properties
                    elevatedWeb.Properties.Update();
                    elevatedWeb.AllowUnsafeUpdates = currUnsafeUpdates;

                    // Create permissions and SPGroups
                    try
                    {
                        FeatureDeploymentHelper featureHelper = new FeatureDeploymentHelper();
                        uint language = currentWeb != null ? currentWeb.Language : 1045;
                        string LimitedAccess = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,LimitedAccess", null, language);
                        string groupMailApprovers = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,MailApprovers", null, language);
                        featureHelper.CreateGroup(currentWeb, groupMailApprovers, "", LimitedAccess);
                        LogWriter.Verbose("WebProperties featurereceiver finished Groups creation", categVerbose);
                    }
                    catch (Exception ex)
                    {
                        LogWriter.Unexpected("WebProperties featurereceiver is unable to create Groups due to: " + ex.Message, categUnexp);
                    }
                }
            }
        }
        #endregion

    }
}
