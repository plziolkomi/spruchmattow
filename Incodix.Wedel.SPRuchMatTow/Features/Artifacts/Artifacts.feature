﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="26f47473-60c9-4db9-b8a8-9747981046d4" alwaysForceInstall="true" description="$Resources:FeatureDesc" featureId="26f47473-60c9-4db9-b8a8-9747981046d4" imageUrl="" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.2529146b-bd3b-469b-b4d3-b586fa5a3260.FullName$" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="$Resources:FeatureTitle" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="c5fdbf96-5191-451d-b1dd-0bc3f54639c3" />
    <projectItemReference itemId="cfaa089e-db14-4ed8-91d5-00b716ac49df" />
    <projectItemReference itemId="8c8d1b3e-c8f8-4880-9d0e-37afbd019b9f" />
    <projectItemReference itemId="c203e275-0274-4939-a5e7-a33c2f10c49f" />
    <projectItemReference itemId="d5015325-5e5e-44b0-aa2a-98917319964a" />
    <projectItemReference itemId="c92a6e02-44fa-447b-a780-66e44f37e35e" />
    <projectItemReference itemId="599c8796-3720-458a-a4be-78557bd12a52" />
    <projectItemReference itemId="b087ee6f-e1ef-4ebd-8833-ba1125e71ea5" />
    <projectItemReference itemId="599b2067-da45-4b30-a7f8-b46787725825" />
    <projectItemReference itemId="86f2311e-b168-4b3b-8e63-8c2266e995fc" />
    <projectItemReference itemId="44366eaf-7a04-46ba-a78e-78b8276ee7fd" />
    <projectItemReference itemId="2b222a66-8123-4075-adc5-ab031f147dc5" />
    <projectItemReference itemId="d473be51-87dc-41d7-9840-03c7f7e1a19c" />
    <projectItemReference itemId="449f4f26-8d96-41aa-ae3a-61f7c4333a8c" />
    <projectItemReference itemId="4bff6eb1-69be-4b51-ab3b-e4006397bc63" />
    <projectItemReference itemId="0bf9a851-48a6-40cb-8652-a413694b31de" />
    <projectItemReference itemId="69f49cae-5052-4795-ac5d-50fa6b80c268" />
    <projectItemReference itemId="ed0657ca-622a-4020-8ca7-6f0142804587" />
  </projectItems>
</feature>