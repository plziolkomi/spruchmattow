﻿using System;
using System.Runtime.InteropServices;
using Microsoft.SharePoint;
using Incodix.Wedel.SPRuchMatTow.Classes;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Utilities;

namespace Incodix.Wedel.SPRuchMatTow.Features.Artifacts
{
    [Guid("2529146b-bd3b-469b-b4d3-b586fa5a3260")]
    public class ArtifactsEventReceiver : SPFeatureReceiver
    {
        #region Properties

        static Logger LogWriter = Logger.Create("kategoria", "ArtifactsEventReceiver");
        static SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("ArtifactsEventReceiver", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        static SPDiagnosticsCategory categVerbose = new SPDiagnosticsCategory("ArtifactsEventReceiver", TraceSeverity.Verbose, EventSeverity.Verbose);

        #endregion

        #region Overrides
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            if (properties.Feature.Parent is SPSite)
            {
                SPSite currentSite = properties.Feature.Parent as SPSite;
                SPWeb currentWeb = currentSite.OpenWeb();
                FeatureDeploymentHelper featureHelper = new FeatureDeploymentHelper();
                uint language = currentWeb != null ? currentWeb.Language : 1045;
                string permAdmin = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,permAdmin", null, language);
                string permEdit = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,permEdit", null, language);
                string permDisp = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,permDisp", null, language);

                string groupRequestors = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Requestors", null, language);
                string groupApprovers = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Approvers", null, language);
                string groupReceptionists = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Receptionists", null, language);
                string groupAdministrators = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,Administrators", null, language);
                // Check for required lists and create if does not exist
                string[,] listsToCreate = new string[,] { { "ApproversList", "ApproversListDef" },
                                                      { "CategoriesList", "CategoriesListDef" },
                                                      { "GoodsMovementList", "GoodsMovListDef" },
                                                      { "MailTemplatesList","MailTemplatesListDef" }
                                                    };
                try
                {
                    featureHelper.CreateListInstances(currentWeb, listsToCreate, language);
                    LogWriter.Verbose("Artifact featurereceiver finished list instances creation", categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Unexpected("Artifact featurereceiver is unable to create list instances due to: " + ex.Message, categUnexp);
                }

                // Create permissions and SPGroups
                try
                {
                    featureHelper.CreateGroup(currentWeb, groupRequestors, "", permDisp);
                    featureHelper.CreateGroup(currentWeb, groupApprovers, "", permDisp);
                    featureHelper.CreateGroup(currentWeb, groupReceptionists, "", permDisp);
                    featureHelper.CreateGroup(currentWeb, groupAdministrators, "", permAdmin);
                    LogWriter.Verbose("Artifact featurereceiver finished Groups creation", categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Unexpected("Artifact featurereceiver is unable to create Groups due to: " + ex.Message, categUnexp);
                }

                // Get Application Lists
                SPList ApproversList = currentWeb.GetList(currentWeb.Url + "/Lists/ApproversList");
                SPList CategoriesList = currentWeb.GetList(currentWeb.Url + "/Lists/CategoriesList");
                SPList GoodsMovementList = currentWeb.GetList(currentWeb.Url + "/Lists/GoodsMovementList");
                SPList MailTemplatesList = currentWeb.GetList(currentWeb.Url + "/Lists/MailTemplatesList");

                try
                {
                    // Create lookupField for GoodsMovment List
                    featureHelper.CreateLookupField(currentWeb, GoodsMovementList, CategoriesList, "CategoryLookup", "GoodsMovement", "Category", false);

                    // Create lookupField for Apporovers List
                    featureHelper.CreateLookupField(currentWeb, ApproversList, CategoriesList, "CategoriesLookup", "ApproversGroup", "Category", true);

                    // Update Title field in GoodsMovement list
                    featureHelper.DisableTitleField(currentWeb, GoodsMovementList, "Przepustka Materiałowo-Towarowa");

                    // Update Title field in CategoriesList list
                    featureHelper.DisableTitleField(currentWeb, CategoriesList, "Kategoria");

                    // Update Title field in ApproversList list
                    featureHelper.DisableTitleField(currentWeb, ApproversList, "Akceptujący");

                    // Break Role Inheritance for Each List and add Specific Permissions
                    featureHelper.SetUniquePermissionsForList(currentWeb, ApproversList, false, new string[,] {
                                                                            { groupRequestors, permDisp },
                                                                            { groupApprovers, permDisp },
                                                                            { groupReceptionists, permDisp },
                                                                            { groupAdministrators, permAdmin } });
                    featureHelper.SetUniquePermissionsForList(currentWeb, CategoriesList, false, new string[,] {
                                                                            { groupRequestors, permDisp },
                                                                            { groupApprovers, permDisp },
                                                                            { groupReceptionists, permDisp },
                                                                            { groupAdministrators, permAdmin } });
                    featureHelper.SetUniquePermissionsForList(currentWeb, MailTemplatesList, false, new string[,] {
                                                                            { groupRequestors, permDisp },
                                                                            { groupApprovers, permDisp },
                                                                            { groupReceptionists, permDisp },
                                                                            { groupAdministrators, permAdmin } });
                    featureHelper.SetUniquePermissionsForList(currentWeb, GoodsMovementList, false, new string[,] {
                                                                            { groupRequestors, permEdit },
                                                                            { groupApprovers, permDisp },
                                                                            { groupReceptionists, permDisp },
                                                                            { groupAdministrators, permAdmin } });

                    LogWriter.Verbose("Artifact featurereceiver finished list instances customization", categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Unexpected("Artifact featurereceiver is unable to customize list instances due to: " + ex.Message, categUnexp);
                }

                // CreateQuickLaunch
                try
                {
                    string RequestorsPanel = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestorsPanel", null, language);
                    string RequestForGoods = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestForGoods", null, language);
                    string MyRequests = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,MyRequests", null, language);
                    string ApproversPanel = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ApproversPanel", null, language);
                    string RequestsForApproval = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestsForApproval", null, language);
                    string RequestsCompleted = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestsCompleted", null, language);
                    string RequestsRejected = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestsRejected", null, language);
                    string ReceptionistsPanel = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ReceptionistsPanel", null, language);
                    string RequestsReadyToRelease = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestsReadyToRelease", null, language);
                    string RequestsWithoutApproval = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestsWithoutApproval", null, language);
                    string WaitingForReturn = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,WaitingForReturn", null, language);
                    string AdministratorsPanel = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,AdministratorsPanel", null, language);
                    string ManageAccess = SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ManageAccess", null, language);

                    string[,] QuickLaunchLinks = new string[,] {    {RequestorsPanel, RequestForGoods, GoodsMovementList.DefaultNewFormUrl },
                                                            {RequestorsPanel, MyRequests, GoodsMovementList.DefaultViewUrl },
                                                            {ApproversPanel, RequestsForApproval, GoodsMovementList.RootFolder.Url + "/RequestsForApproval.aspx" },
                                                            {ApproversPanel, RequestsCompleted, GoodsMovementList.RootFolder.Url + "/RequestsCompleted.aspx" },
                                                            {ApproversPanel, RequestsRejected, GoodsMovementList.RootFolder.Url + "/RequestsRejected.aspx" },
                                                            {ReceptionistsPanel, RequestsReadyToRelease, GoodsMovementList.RootFolder.Url + "/RequestsReadyToRelease.aspx" },
                                                            {ReceptionistsPanel, WaitingForReturn, GoodsMovementList.RootFolder.Url + "/WaitingForReturn.aspx" },
                                                            {ReceptionistsPanel, RequestsWithoutApproval, GoodsMovementList.RootFolder.Url + "/RequestsWithoutApproval.aspx" },
                                                            {AdministratorsPanel, ApproversList.Title, ApproversList.DefaultViewUrl},
                                                            {AdministratorsPanel, CategoriesList.Title, CategoriesList.DefaultViewUrl},
                                                            {AdministratorsPanel, MailTemplatesList.Title, MailTemplatesList.DefaultViewUrl},
                                                            {AdministratorsPanel, ManageAccess, currentWeb.Url + "/_layouts/15/user.aspx"}};

                    for (int i = 0; i < QuickLaunchLinks.GetLength(0); i++)
                    {
                        featureHelper.AddQuickLaunchItem(currentWeb, QuickLaunchLinks[i, 0], "", QuickLaunchLinks[i, 1], QuickLaunchLinks[i, 2]);
                    }
                    LogWriter.Verbose("Artifact featurereceiver finished QuickLaunch creation", categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Unexpected("Artifact featurereceiver is unable to create QuickLaunch Links due to: " + ex.Message, categUnexp);
                } 
            }
        }
        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            if (properties.Feature.Parent is SPSite)
            {
                SPSite currentSite = properties.Feature.Parent as SPSite;
                SPWeb currentWeb = currentSite.OpenWeb();
                uint language = currentWeb != null ? currentWeb.Language : 1045;
                try
                {
                    FeatureDeploymentHelper featureHelper = new FeatureDeploymentHelper();
                    string[] QuickLaunchPanels = new string[] { SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,RequestorsPanel", null, language),
                                               SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ApproversPanel", null, language),
                                               SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,ReceptionistsPanel", null, language),
                                               SPUtility.GetLocalizedString("$Resources:Incodix.Wedel.SPRuchMatTow,AdministratorsPanel", null, language)
                                              };

                    // Clear QuickLaunch Panel
                    featureHelper.QuickLaunchDeleter(currentWeb, QuickLaunchPanels);
                    LogWriter.Verbose("Artifact featurereceiver finished QuickLaunch removal", categVerbose);
                }
                catch (Exception ex)
                {
                    LogWriter.Unexpected("Artifact featurereceiver is unable to remove QuickLaunch Links due to: " + ex.Message, categUnexp);
                } 
            }
        }

        #endregion
    }
}
