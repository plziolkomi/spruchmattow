﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="b553fd9c-7f4f-47ce-a346-a7590bd36b6c" alwaysForceInstall="true" description="$Resources:FeatureDesc" featureId="b553fd9c-7f4f-47ce-a346-a7590bd36b6c" imageUrl="" receiverAssembly="$SharePoint.Project.AssemblyFullName$" receiverClass="$SharePoint.Type.7a40fe12-9710-468b-a219-4eb8780562ab.FullName$" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="$Resources:FeatureTitle" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <activationDependencies>
    <referencedFeatureActivationDependency minimumVersion="" itemId="9c0fae57-87f6-47bf-a053-d48415842357" />
  </activationDependencies>
  <projectItems>
    <projectItemReference itemId="3e7c9346-63d6-4054-ab0a-80b58bdc16de" />
    <projectItemReference itemId="7468879d-701e-4d99-8f73-2cb22789ebc1" />
    <projectItemReference itemId="6900ec1c-b413-46d9-9ecb-d118b19f5b52" />
  </projectItems>
</feature>