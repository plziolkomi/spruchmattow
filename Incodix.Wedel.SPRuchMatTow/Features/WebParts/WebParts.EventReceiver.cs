using System;
using System.Runtime.InteropServices;
using Microsoft.SharePoint;
using Incodix.Wedel.SPRuchMatTow.Classes;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Utilities;

namespace Incodix.Wedel.SPRuchMatTow.Features.WebParts
{

    [Guid("7a40fe12-9710-468b-a219-4eb8780562ab")]
    public class WebPartsEventReceiver : SPFeatureReceiver
    {
        #region Properties
        private static string webPartLink = "/_catalogs/wp/Incodix.Wedel.SPRuchMatTow_GoodsMovWebPart.webpart";
        #endregion

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            if (properties.Feature.Parent is SPSite)
            {
                SPSite currentSite = properties.Feature.Parent as SPSite;
                SPWeb currentWeb = currentSite.OpenWeb();
                FeatureDeploymentHelper featureHelper = new FeatureDeploymentHelper();
                //language = currentWeb != null ? currentWeb.Language : 1045;

                // Set CSR for ListFormView webParts
                SPList GoodsMovementList = currentWeb.GetList(currentWeb.Url + "/Lists/GoodsMovementList");
                // Update forms     
                if (GoodsMovementList != null)
                {
                    foreach (SPForm spForm in GoodsMovementList.Forms)
                    {
                        if (spForm.Url.Contains("NewForm.aspx"))
                        {
                            featureHelper.AddWebPartToForm(currentWeb, GoodsMovementList, webPartLink, "NewForm.aspx");
                        }
                        if (spForm.Url.Contains("EditForm.aspx"))
                        {
                            featureHelper.AddWebPartToForm(currentWeb, GoodsMovementList, webPartLink, "EditForm.aspx");
                        }
                        if (spForm.Url.Contains("DispForm.aspx"))
                        {
                            featureHelper.AddWebPartToForm(currentWeb, GoodsMovementList, webPartLink, "DispForm.aspx");
                        }
                    }
                } 
            }
        }
    }
}
