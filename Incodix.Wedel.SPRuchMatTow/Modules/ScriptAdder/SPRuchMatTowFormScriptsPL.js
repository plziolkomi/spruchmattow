﻿// Initial Form scripts //
var availableApproverIDsAvailable = false;
function InsertScriptToDropDown() {
    var listUrl = _spPageContextInfo.serverRequestPath.toString();
    var dispForm = listUrl.search(/(dispform)/ig);
    var newForm = listUrl.search(/(newform)/ig);
    if (newForm > 0) {
        $("select[title='Powiązana Kategoria']").on("change", function () {
            var selectedVal = $(this).find('option:selected').html();
            RefreshApproversList(selectedVal);
        });
        RefreshApproversList($("select[title='Powiązana Kategoria']").find('option:first').html());
        setTimeout(function ()
        {
            if (availableApproverIDsAvailable) {
                var selectedCategory = $("select[title='Powiązana Kategoria']").find('option:selected').html() == undefined ? $("select[title='Powiązana Kategoria']").find('option:first').html() : $("select[title='Powiązana Kategoria']").find('option:selected').html();
                RefreshApproversList(selectedCategory);
            }
        }, 10000);

        $("select.fldApprover").on("change", function () {
            var selectedVal = $(this).find('option:selected').html();
            SetAndResolvePeoplePicker("Akceptujący", selectedVal.replace(" [Brak danych o dostępności]", "").replace(" [Na terenie zakładu]", "").replace(" [Poza zakładem]", ""));
        });
    }
}
function HideDefaultWebPartForm() {
    $('#WebPartWPQ2').hide();
    $('#contentBox').removeClass("hiddenSection");
    $('#onetIDListForm').attr("style", "width:80%");
}
function InjectBootstrapStyles(){
    $("select[title='Powiązana Kategoria']").addClass("form-control").addClass("formControlExtended");
    $("input[title='Samochód (marka/nr rej.)']").addClass("form-control").addClass("formControlExtended");
    $("textarea[title='Cel wyniesienia/wywiezienia']").addClass("form-control");
    $("input[title='Przepustka dla']").addClass("form-control").addClass("formControlExtended");
    $("input[title='Do ponownego wniesienia/zwrotu']").addClass("ToReturn");

    var listUrl = _spPageContextInfo.serverRequestPath.toString();
    var dispForm = listUrl.search(/(dispform)/ig);
    if (dispForm > 0) {
        $('div.fldCategoryLookup').find('a').addClass("form-control").addClass("formControlExtended");
        $('div.fldRequestState').find('label').addClass("formControlExtended");
        $('div.fldApproverProgressBar').hide();
        $('.lblApproverDisplay').addClass("formControlExtended");
        $('label[id^="fldApprovalDate"]').addClass("formControlExtended");
        
        var ToReturn = $('.ToReturnRow').html();
        $('.ToReturnRow').html('<label id="ToReturn" class="form-control">' + ToReturn + '</label>');

        var RequestFor = $('.RequestFor').html();
        $('.RequestFor').html('<label id="RequestFor" class="form-control formControlExtended">' + RequestFor + '</label>');

        var purposeVal = $('.PurposeOfGoodsMovement').html();
        $('.PurposeOfGoodsMovement').html('<label class="form-control formControlExtended">' + purposeVal + '</label>');

        var vehicleVal = $('.RequestorVehicle').html();
        $('.RequestorVehicle').html('<label id="RequestorVehicle" class="form-control formControlExtended">' + vehicleVal + '</label>');
    }
    else{
        $('input[id$="diidIOGoBack"]').addClass('btn');        
    }
}
function InjectCurrentUser() {
    'use strict'
    var currUser = $('label[id$="fldRequestor"]').html();
    $('input[title="Przepustka dla"]').val(currUser);
}
function ResizeTextAreas() {
    $('textarea').each(function () {
        resizeTextArea(this);
    }).on('input', function () {
        resizeTextArea(this);
    });
}
function GridFunctionStarter() {
    $(document).ready(function () {
        ResizeTextAreas();
    });
}
function resizeTextArea(e) {
    $(e).css({ 'height': 'auto', 'overflow-y': 'hidden' }).height(e.scrollHeight);
}
// Initial Form scripts //


// Refresh Approvers List Functions //

// Properties
var _approvers;
var approversStatusBarID;
var quantityStatusID;
var availableApproverIDs;

function RefreshApproversList(selectedVal) {
    var listUrl = _spPageContextInfo.serverRequestPath.toString();
    var dispForm = listUrl.search(/(dispform)/ig);
    if (dispForm < 0) {
        $('div.fldApproverProgressBar').show();
        $("select.fldApprover").hide();
    }
    else {
        $('div.fldApproverProgressBar').hide();
        $("select.fldApprover").hide();
    }

    ExecuteOrDelayUntilScriptLoaded(function () {
        if (approversStatusBarID != undefined) {
            SP.UI.Status.removeStatus(approversStatusBarID);
        }

        // Get Approvers
        var ApproversCtx = new SP.ClientContext.get_current();
        var oListApprovers = ApproversCtx.get_web().get_lists().getByTitle("Lista osób akceptujących");
        var ApproversQuery = new SP.CamlQuery();
        ApproversQuery.set_viewXml('<View><Query><Where>' +
                                                      '<Contains>' +
                                                         '<FieldRef Name="CategoriesLookup" />' +
                                                         '<Value Type="LookupMulti">' + selectedVal + '</Value>' +
                                                      '</Contains>' +
                                                 '</Where>' +
                                          '</Query>' +
                                         '<ViewFields>' +
                                           '<FieldRef Name="ID" />' +
                                           '<FieldRef Name="Approver" />' +
                                           '<FieldRef Name="SKDApproverID" />' +
                                        '</ViewFields>' +
                                    '</View>');
        _approvers = oListApprovers.getItems(ApproversQuery);
        ApproversCtx.load(_approvers);
        ApproversCtx.executeQueryAsync(Function.createDelegate(this, this.RefreshApproversListSuccess), Function.createDelegate(this, this.RefreshApproversListFail));
    }, 'SP.js');
}
function RefreshApproversListSuccess() {
    'use strict'
    var optionsToInsert = "";
    var itemsCount = 0;
    var fldApprover = $("select.fldApprover");
    var resultLabel = "Brak danych o dostępności";

    var listUrl = _spPageContextInfo.serverRequestPath.toString();
    var newForm = listUrl.search(/(newform)/ig);
    var selectedApprover = fldApprover.find('option:selected').html() == undefined ? fldApprover.find('option:first').html() : fldApprover.find('option:selected').html();
    if (selectedApprover != undefined) {
        selectedApprover = selectedApprover.replace(" [Brak danych o dostępności]", "").replace(" [Na terenie zakładu]", "").replace(" [Poza zakładem]", "");
    }
    var listItemEnumerator = _approvers.getEnumerator();
    while (listItemEnumerator.moveNext()) {
        // Get User details with available days
        _approvers = listItemEnumerator.get_current();
        // Get User Emplyment Type
        var approverID = _approvers.get_id();
        var approverLookup = _approvers.get_item('Approver');
        var approverSKDId = _approvers.get_item('SKDApproverID');
        var approverValue = approverLookup.get_lookupValue();

        // do not insert user same as requestor
        if (approverValue != $("label.fldRequestor").html()) {

            // Get Approver presence
            if (availableApproverIDs != undefined) {
                var isPresent = $.inArray(approverSKDId, availableApproverIDs) > -1;
                if (isPresent) {
                    resultLabel = "Na terenie zakładu";
                }
                else if (!isPresent) {
                    resultLabel = "Poza zakładem";
                }
            }
            // Insert option to dropdown
            if (newForm > 0) {
                if (selectedApprover != approverValue) {
                    optionsToInsert += "<option value='" + approverID + "'>" + approverValue + " [" + resultLabel + "]</option>";
                }
                else {
                    optionsToInsert += "<option selected='selected' value='" + approverID + "'>" + approverValue + " [" + resultLabel + "]</option>";
                }
            }
            else {
                if (selectedApprover != approverValue) {
                    optionsToInsert += "<option value='" + approverID + "'>" + approverValue + "</option>";
                }
                else {
                    optionsToInsert += "<option selected='selected' value='" + approverID + "'>" + approverValue + "</option>";
                }
            }
            itemsCount++;
        }
    }
    if (itemsCount > 0) {
        fldApprover.html(optionsToInsert);
        $('div.fldApproverProgressBar').hide();
        fldApprover.show();

        var selectedVal = $("select.fldApprover").find('option:selected').html();
        SetAndResolvePeoplePicker("Akceptujący", selectedVal.replace(" [Brak danych o dostępności]", "").replace(" [Na terenie zakładu]", "").replace(" [Poza zakładem]", ""));
    }
    else {
        var msg = "Brak akceptujących do wyświetlenia. Zmień kategorię, aby odświeżyć listę. Jeśli problem będzie występował skontaktuj się z Administratorem systemu";
        approversStatusBarID = SP.UI.Status.addStatus(msg);
        SP.UI.Status.setStatusPriColor(approversStatusBarID, "red");
    }
}
function RefreshApproversListFail(sender, args) {
    'use strict'
    var msg = "Nie udało się listy akceptujących dla danej kategorii. Skontaktuj się z Administratorem systemu";
    approversStatusBarID = SP.UI.Status.addStatus(msg);
    SP.UI.Status.setStatusPriColor(approversStatusBarID, "red");
}
function getApproversPresence() {
    'use strict'
    // Show progressBar when getting data
    $('.fldApproverProgressBar').show();

    var listUrl = _spPageContextInfo.serverRequestPath.toString();
    var dispForm = listUrl.search(/(dispform)/ig);
    if (dispForm < 0) {
        var serviceUri = _spPageContextInfo.webAbsoluteUrl +
        "/_vti_bin/AccessControlSystemConnector.svc/getPresenceAll";
        $.ajax({
            type: "GET",
            contentType: "application/json",
            url: serviceUri,
            dataType: "json",
            success:
                function (response) {
                    availableApproverIDs = response;
                    $('.fldApproverProgressBar').hide();
                    availableApproverIDsAvailable = true;
                }
        });
    }
}
function SetAndResolvePeoplePicker(fieldName, userAccountName) {
    'use strict'
    ExecuteOrDelayUntilScriptLoaded(function () {
        ExecuteOrDelayUntilScriptLoaded(function () {
            var controlName = fieldName;
            var peoplePickerDiv = $("[id$='ClientPeoplePicker'][title='" + controlName + "']");
            var peoplePickerEditor = peoplePickerDiv.find("[title='" + controlName + "']");
            var spPeoplePicker = SPClientPeoplePicker.SPClientPeoplePickerDict[peoplePickerDiv[0].id];

            spPeoplePicker.DeleteProcessedUser();

            peoplePickerEditor.val(userAccountName);
            spPeoplePicker.AddUnresolvedUserFromEditor(true);
        }, 'clientpeoplepicker.js');
    }, 'clienttemplates.js');
}
// Refresh Approvers List Functions //

// Grid View Functions //

function SmallProgressBarEnabler() {
    'use strict'
    $('input.lbtnAdd').addClass('hiddenSection');
    $('#SmallProgressBarContainer').removeClass('hiddenSection');
    //$('#SmallProgressBar').progressbar({
    //    value: false
    //});
    //$('#ProgressBar').progressbar("enable");
}
function SmallProgressBarDisabler() {
    'use strict'
    $('input.lbtnAdd').removeClass('hiddenSection');
    $('#SmallProgressBarContainer').addClass('hiddenSection');
}
function RowDeleter() {
    'use strict'
    var result = confirm('Czy chcesz usunąć ten wiersz?');
    if (result) {
        RowCounter();
    }
    return result;
}
// Grid View Functions //

// Validation Functions
function PreSaveAction() {
    'use strict'
    var ApproverResult = false;
    var QuantityResult = false;
    var listUrl = _spPageContextInfo.serverRequestPath.toString();
    var editForm = listUrl.search(/(editform)/ig);
    var newForm = listUrl.search(/(newform)/ig);
    if (newForm > 0 || editForm > 0) {
        ApproverResult = checkForApprover();
        QuantityResult = QuantityValidator();
    }
    if (ApproverResult && QuantityResult) {
        return true;
    }
    else {
        return false;
    }
}
var sameApproverStatusID;
function checkForApprover() {
    'use strict'
    var currRequestor = $("label.fldRequestor").html();
    var selectedApprover = $('select.fldApprover').find('option:selected').html() == undefined ? $('select.fldApprover').find('option:first').html() : $('select.fldApprover').find('option:selected').html();

    // compare
    if (currRequestor == selectedApprover) {
        if (sameApproverStatusID != undefined) {
            SP.UI.Status.removeStatus(sameApproverStatusID);
        }
        var msg = "Wnioskodawca nie może być jednoczesnie akceptującym. Wybierz inną osobę";
        sameApproverStatusID = SP.UI.Status.addStatus(msg);
        SP.UI.Status.setStatusPriColor(sameApproverStatusID, "red");
        return false;
    }
    else {
        return true;
    }
}
function QuantityValidator() {
    'use strict'
    var result = false;
    var sum = 0;
    var msg = "";
    var intReg = new RegExp(/\D+/ig);
    var nonIntValue = false;
    var noMaterial = false;
    var tbl = $('table.RequestedGoodsGrid');
    tbl.find('input.Quantity').each(function () {
        var stringVal = $(this).val();
        if (stringVal != "") {
            nonIntValue = intReg.test(stringVal);
            msg = "W polu 'Ilość' dozwolone są tylko liczby. Popraw wartość i zapisz ponownie formularz";
        }
        else {
            nonIntValue = true;
            msg = "Uzupełnij pole 'Ilość'  w każdym wierszu tabeli i zapisz ponownie formularz";
        }
    });
    tbl.find('textarea.MaterialName').each(function () {
        var stringVal = $(this).val();
        if (stringVal == "") {
            noMaterial = true;
            msg = "Uzupełnij pole 'Nazwa materiału'  w każdym wierszu tabeli i zapisz ponownie formularz";
        }
    });

    if (nonIntValue || noMaterial) {
        if (quantityStatusID != undefined) {
            SP.UI.Status.removeStatus(quantityStatusID);
        }
        quantityStatusID = SP.UI.Status.addStatus(msg);
        SP.UI.Status.setStatusPriColor(quantityStatusID, "yellow");
        result = false;
    }
    else {
        if (quantityStatusID != undefined) {
            SP.UI.Status.removeStatus(quantityStatusID);
        }
        result = true;
    }
    return result;
}
function TriggerPostback() {
    'use strict'
    if (PreSaveAction()) {
        var saveButtonName = $('input[value$="Save"]').attr('name');
        WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(saveButtonName, "", true, "", "", false, true));

    }
}
