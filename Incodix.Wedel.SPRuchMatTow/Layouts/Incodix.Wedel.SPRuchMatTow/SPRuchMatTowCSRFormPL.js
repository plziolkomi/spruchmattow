﻿/** --------- Tabs Functions -------- **/
var currentFormUniqueId;
var currentFormWebPartId;
var tabsObj = [
    ["Kontrola r", "Edit32x32.png", ["Signature", "CategoryLookup", "Approver", "RequestorVehicle",
        "RequestedGoods", "PurposeOfGoodsMovement", "RequestState", "ApprovalDate", "ApproverComments",
        "ClosedWithoutApproval", "Receptionist", "ReceptionistComments", "ClosureDate", "CreationYear"]],
];
(function () {
    (window.jQuery || document.write('<script src="../../../../_layouts/15/Incodix.Wedel.SPRuchMatTow/jquery.js"></script>'));
    document.write('<link href="../../../AppAssets/SPRuchMatTowForm.css" rel="stylesheet" />')
    document.write('<script src="/_layouts/15/form.debug.js"></script>')
    var tabsContext = {};
    tabsContext.OnPreRender = TabsOnPreRender;
    tabsContext.OnPostRender = TabsOnPostRender;
    tabsContext.Templates = {};

    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(tabsContext);
})();

function TabsOnPreRender(ctx) {
    if (!currentFormUniqueId) {

        currentFormUniqueId = ctx.FormUniqueId;
        currentFormWebPartId = "WebPart" + ctx.FormUniqueId;

        $(document).ready(function () {
            $("#" + currentFormWebPartId).prepend("<div id='FormContainer'></div>");
            $("#FormContainer").append("<div id='LeftFormContainer' class='LeftFormContainer'></div>");
            $("#LeftFormContainer").append("<div id='TabsFormContainer' class='tabs'></div>");

            var tabHTMLTemplate = "<li class='{class}'><a href='#{Index}'><span><img id='img{Title}' " +
            "src='/_layouts/15/images/Incodix.Wedel.SPRuchMatTow/{Image}' />&nbsp;{Title}</span></a></li>";
            var tabClass;
            var tabsHTML = "";

            for (var i = 0; i < tabsObj.length; i++) {
                tabClass = "";
                if (i == 0) { tabClass = "active"; }
                tabsHTML += tabHTMLTemplate.replace(/{Index}/g, i).replace(/{Title}/g, tabsObj[i][0]).replace(/{class}/g, tabClass).replace(/{Image}/g, tabsObj[i][1])
            }

            $("#TabsFormContainer").append("<ul class='tabs'>" + tabsHTML + "</ul>");

            $('.tabs .tab-links a').on('click', function (e) {
                var currentAttrValue = $(this).attr('href');
                // Show/Hide Tabs
                $('.tabs ' + currentAttrValue).show().siblings().hide();
                // Change/remove current tab to active
                $(this).parent('li').addClass('active').siblings().removeClass('active');
                e.preventDefault();
            });
            $('.tabs li a').on('click', function (e) {
                var currentIndex = $(this).attr('href').replace("#", "");
                showTabControls(ctx, currentIndex);
                $(this).parent('li').addClass('active').siblings().removeClass('active');
                e.preventDefault();
            });

            showTabControls(ctx, 0);

            // Find Container for SPFields by using fieldTitle and append to TabsFormContainer
            $("#tr_Title").closest("table").appendTo("#TabsFormContainer");


            $("#" + currentFormWebPartId).prepend("<!--LeaveRequestsCustomForm-->");

            // Insert Right Part of Form
            $("#FormContainer").append("<div id='RightFormContainer' class='RightFormContainer'></div>");

            $("#FormContainer").append("<div id='FooterContainer' class='FooterContainer'></div>");
            // Move Save Button etc between SPFields and Comments Section
            $("#part1").appendTo("#LeftFormContainer");

            // Find each date picker and add custom Button to set currentDate
            $("#tr_ArrivalDate").find('table').find('tr:first').append('<td><input id="ArrivalDate" type="button" value="Dzisiejsza data" onclick="SetCurrentDate(&quot;tr_ArrivalDate&quot;,&quot;Przyjazd&quot;)" /></td>');
            $("#tr_VehicleEntryForUnloadingOrRedirectDate").find('table').find('tr:first').append('<td><input id="VehicleEntryForUnloadingOrRedirectDate" type="button" value="Dzisiejsza data" onclick="SetCurrentDate(&quot;tr_VehicleEntryForUnloadingOrRedirectDate&quot;,&quot;Rozładunek&quot;)" /></td>');
            $("#tr_VehicleDepartureDate").find('table').find('tr:first').append('<td><input id="VehicleDepartureDate" type="button" value="Dzisiejsza data" onclick="SetCurrentDate(&quot;tr_VehicleDepartureDate&quot;,&quot;Wyjazd&quot;)" /></td>');

            $('<tr style="display: table-row;" id="RowForEquipment">' +
                '<td width="113" class="ms-formlabel" nowrap="true" valign="top">' +
                    '<h3 class="ms-standardheader"><nobr>Środki ochrony indywidualnej</nobr></h3></td>' +
                '<td width="350" class="ms-formbody" valign="top"><table id="EquipmentTable"></table></td></tr>').insertAfter('#tr_VendorName');
            // Create table for equipment
            //$('table.ms-formtable').append();

            // inject new row
            $("#tr_PersonalProtectionShoes").appendTo('#EquipmentTable');
            $('#tr_PersonalProtectionVest').appendTo('#EquipmentTable');
            $('#tr_PersonalProtectionProvideEquipment').appendTo('#EquipmentTable');
        });
    }
}
function TabsOnPostRender(ctx) {
    var ctrl;
    var controlId;
    var listUrl = _spPageContextInfo.serverRequestPath.toString();
    if (ctx.BaseViewID == "DisplayForm") {
        controlId = 'SPBookmark_' + ctx.ListSchema.Field[0].Name;
        ctrl = $("[name^='" + controlId + "']");
    }
    else {
        controlId = ctx.ListSchema.Field[0].Name + "_" + ctx.ListSchema.Field[0].Id;
        ctrl = $("[id^='" + controlId + "']");
        if (ctrl.length == 0) {
            ctrl = null;
            controlId = 'SPBookmark_' + ctx.ListSchema.Field[0].Name;
            ctrl = $("[name^='" + controlId + "']");
        }
    }
    ctrl.closest("tr").attr('id', 'tr_' + ctx.ListSchema.Field[0].Name).hide();
}
function showTabControls(ctx, index) {
    $("#" + currentFormWebPartId + " [id^='tr_']").hide();
    for (var i = 0; i < tabsObj[index][2].length; i++) {
        $("[id^='tr_" + tabsObj[index][2][i] + "']").show();
    }
}