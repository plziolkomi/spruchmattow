﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.UI.WebControls;
using Incodix.Wedel.SPRuchMatTow.Classes;
using System.Web.UI;
using Microsoft.SharePoint.Administration;

namespace Incodix.Wedel.SPRuchMatTow.Layouts.Incodix.Wedel.SPRuchMatTow
{
    public partial class SPRuchMatTowPrintPage : LayoutsPageBase
    {
        #region Properties
        ContentPlaceHolder cpHolder = null;
        UserControl uc = null;
        static Logger LogWriter = Logger.Create("kategoria", "SPRuchMatTowPrintPage");
        static LogLevel lvlUnexp = LogLevel.Unexpected;
        static SPDiagnosticsCategory categUnexp = new SPDiagnosticsCategory("SPRuchMatTowPrintPage", TraceSeverity.Unexpected, EventSeverity.ErrorCritical);
        #endregion
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            cpHolder = (ContentPlaceHolder)this.Master.FindControl("PlaceHolderMain");
            EnsureChildControls();
        }
        protected override void CreateChildControls()
        {
            try
            {
                base.CreateChildControls();
                string userControlName = Request.QueryString["UserControlFileName"].ToString();

                string completePathName = "~/_controltemplates/15/Incodix.Wedel.SPRuchMatTow/" + userControlName;
                if (!String.IsNullOrEmpty(completePathName))
                {
                    uc = (UserControl)LoadControl(completePathName);
                    uc.ID = "userControlPopupID";
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to load Application Page SPRuchMatTowPrintPage.aspx due to " + ex.Message, lvlUnexp, categUnexp);
            }

        }
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                PlaceHolder ucHolder = (PlaceHolder)cpHolder.FindControl("UserControlPlaceHolder");
                ucHolder.Controls.Clear();
                ucHolder.Controls.Add(uc);

                SPWeb currentWeb = SPContext.Current.Web;
                uint language = currentWeb != null ? currentWeb.Language : 1045;
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to load UserControl on ApplicationPage SPRuchMatTowPrintPage.aspx due to " + ex.Message, lvlUnexp, categUnexp);
            }
        }
    }
}
