﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Incodix.Wedel.SPRuchMatTow.Classes.UI.SharePoint.WebControls;
using System.Web.UI.WebControls;
using System.Web.UI;
using Incodix.Wedel.SPRuchMatTow.Classes;
using Microsoft.SharePoint.Administration;

namespace Incodix.Wedel.SPRuchMatTow.Layouts.Incodix.Wedel.SPRuchMatTow
{
    public partial class DialogBoxAppPage : SPDialogBoxLayoutPageBase
    {
        ContentPlaceHolder cpHolder = null;
        UserControl uc = null;
        Logger LogWriter = Logger.Create("Configuration", "SPRuchMatTow-DialogBoxAppPage");
        LogLevel poziomHigh = LogLevel.High;
        static TraceSeverity traceseverity = new TraceSeverity();
        static EventSeverity eventseverity = new EventSeverity();
        SPDiagnosticsCategory kategoria = new SPDiagnosticsCategory("SPRuchMatTow-DialogBoxAppPage", traceseverity, eventseverity);

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            cpHolder = (ContentPlaceHolder)this.Master.FindControl("PlaceHolderMain");
            EnsureChildControls();
        }
        protected override void CreateChildControls()
        {
            try
            {
                base.CreateChildControls();
                string userControlName = Request.QueryString["UserControlFileName"].ToString();

                string completePathName = "~/_controltemplates/15/Incodix.Wedel.SPRuchMatTow/" + userControlName;
                if (!String.IsNullOrEmpty(completePathName))
                {
                    uc = (UserControl)LoadControl(completePathName);
                    uc.ID = "userControlPopupID";
                }
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to load ApplicationPage DialogBoxAppPage.aspx due to: " + ex.Message, poziomHigh, kategoria);
            }

        }
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                PlaceHolder ucHolder = (PlaceHolder)cpHolder.FindControl("UserControlPlaceHolder");
                ucHolder.Controls.Clear();
                ucHolder.Controls.Add(uc);

                IDialogBox userControlEvent = uc as IDialogBox;
                userControlEvent.ResultOk += new AddEventHandlerToSPDialogEvent(AddControlID_EndProcess);
            }
            catch (Exception ex)
            {
                LogWriter.Log("Unable to load UserControl on DialogBoxAppPage.aspx due to " + ex.Message, poziomHigh, kategoria);
            }
        }

        void AddControlID_EndProcess(object sender, SPDialogEventHandler e)
        {
            EndOperation(e.dialogResult, e.ReturnValues);
        }
    }
}
