﻿// ******** Ribbon Form buttons ******** //
var parametry;
var titleGlobal;
var ReturnUrl;
var oListItem;
var oGoBackList;

function VerifyState(itemID, formAction) {
    'use strict'
    var result = false;
    var formState = $('div.fldRequestState').find('label.form-control').html();
    var userid = _spPageContextInfo.userId;
    var requestorID = $('.fldRequestorHidden').html();
    var approverID = $('.fldApproverHidden').html();
    var recepcionistsIDs = $('.fldReceptionistsHidden').html();
    var rcptArray = recepcionistsIDs.split(";");
    switch (formAction) {
        case "SendForApproval":
            if (formState == "Nowy wniosek" && userid == requestorID)
                result = true;
            break;
        case "Approve":
            if (formState == "Oczekiwanie na akceptacje" && userid == approverID)
                result = true;
            break;
        case "Reject":
            if (formState == "Oczekiwanie na akceptacje" && userid == approverID)
                result = true;
            break;
        case "ConfirmRelease":
            if (formState == "Wniosek zaakceptowany. Do potwierdzenia na recepcji" && isInArray(rcptArray, userid))
                result = true;
            break;
        case "ConfirmReleaseWithoutApproval":
            if (formState == "Oczekiwanie na akceptacje" && isInArray(rcptArray, userid))
                result = true;
            break;
        case "ConfirmReturn":
            if (formState == "Przedmioty wyniesione" && isInArray(rcptArray, userid))
                result = true;
            break;
        case "":
            if (formState != "Wniosek zrealizowany" && formState != "Wniosek odrzucony" && formState != "Zweryfikowane przez recepcję")
                result = true;
            break;
    }
    return result;
}
function isInArray(array, value) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == value) {
            return true;
        }
    }
    return false;
}
function verifyRoles(itemID) {
    'use strict'
    ExecuteOrDelayUntilScriptLoaded(function () {
        var listID = _spPageContextInfo.pageListId;
        var rolesCtx = new SP.ClientContext.get_current();
        var oList = rolesCtx.get_web().get_lists().getById(listID);
        rolesCtx.load(oList, 'DefaultViewUrl');
        rolesCtx.executeQueryAsync(GoBackToListQuerySuccess, GoBackToListQueryFailure);
    }, 'SP.js');
}
function GoBackToList() {
    'use strict'
    ExecuteOrDelayUntilScriptLoaded(function () {
        var listID = _spPageContextInfo.pageListId;
        var ctx = new SP.ClientContext.get_current();
        oGoBackList = ctx.get_web().get_lists().getById(listID);
        ctx.load(oGoBackList, 'DefaultViewUrl');
        ctx.executeQueryAsync(GoBackToListQuerySuccess, GoBackToListQueryFailure);
    }, 'SP.js');
}
function GoBackToListQuerySuccess() {
    var listUrl = oGoBackList.get_defaultViewUrl();
    location.href = listUrl;
}
function GoBackToListQueryFailure(sender, args) {
    var msg = "Nie udało się przekierować na stronę listy formularzy. Skontaktuj się z Administratorem dokumentu" + adminDisplayName + " w celu nadania dostępu";
    statusID = SP.UI.Status.addStatus(msg);
    SP.UI.Status.setStatusPriColor(statusID, 'red');
}
function OpenSPDialogBox(calledUCName, docID, formAction, modaltitle, modalWidth, modalHeight, CallBackFunctionName, returnPath) {
    'use strict'
    titleGlobal = modaltitle;
    ReturnUrl = returnPath;
    ExecuteOrDelayUntilScriptLoaded(function () {
    var options =
    {
        title: modaltitle,
        url: SP.Utilities.Utility.getLayoutsPageUrl("/Incodix.Wedel.SPRuchMatTow/DialogBoxAppPage.aspx?UserControlFileName=" + calledUCName + "&DocID=" + docID + "&FormAction=" + formAction),
        width: modalWidth,
        height: modalHeight,
        showScroll: false,
        showClose: false,
        dialogReturnValueCallback: window[CallBackFunctionName]
    };

    var listUrl = _spPageContextInfo.serverRequestPath.toString();
    var editForm = listUrl.search(/(editform)/ig);
    if (editForm > 0) {
        var result = confirm('Czy zapisać zmiany na formularzu?');
        if (result) {
            TriggerPostback();
            SP.UI.ModalDialog.showModalDialog(options);
        }
        else {
            SP.UI.ModalDialog.showModalDialog(options);
        }
    }
    else {
        SP.UI.ModalDialog.showModalDialog(options);
    }
}, "sp.js");
}
function MainScreenSample_CallBack(result, returnValue) {
    SP.UI.Status.removeAllStatus(true);
    var msg = returnValue;
    if (result == 1) {
        $('input[id=ctl00_PlaceHolderMain_userControlPopupID_ConfirmButton]').trigger('click');
        //var listUrl = _spPageContextInfo.serverRequestPath.toString();
        //var editForm = listUrl.search(/(editform)/ig);
        //if (editForm < 0) {
        window.location.href = ReturnUrl;
        //}
    }
    else if (result == 2) {
        // Status bar
        var statusID;
        var notificationID;
        statusID = SP.UI.Status.addStatus(msg);
        SP.UI.Status.setStatusPriColor(statusID, 'green');
    }
    else if (result == 0 && returnValue != undefined && returnValue != null) {
        // status bar
        var statusID;
        var notificationID;
        statusID = SP.UI.Status.addStatus(msg);
        SP.UI.Status.setStatusPriColor(statusID, 'red');
    }
}
function ResizeTextAreas() {
    $('textarea').each(function () {
        resizeTextArea(this);
    }).on('input', function () {
        resizeTextArea(this);
    });
}
function GridFunctionStarter() {
    $(document).ready(function () {
        ResizeTextAreas();
    });
}
function resizeTextArea(e) {
    $(e).css({ 'height': 'auto', 'overflow-y': 'hidden' }).height(e.scrollHeight);
}
function VerifyFormStatus() {
    'use strict'
    var formStatus = $("label[id$='fldRequestState']").html();
    if (formStatus == "Wniosek zaakceptowany. Do potwierdzenia na recepcji" || formStatus == "Wniosek zrealizowany")
        return true;
    else
        return false;
}
// ******** Ribbon Form buttons ******** //

// ******** Ribbon List buttons ******** //

var noReqStateColStatusID;
var errReqStateColStatusID;

function VerifySelection() {
    'use strict'
    var ctx = SP.ClientContext.get_current();
    var items = SP.ListOperation.Selection.getSelectedItems(ctx); 
    if (CountDictionary(items) == 1) {
        return true;
    }
    else {
        return false;
    }
}
function VerifyStateOnList(formAction) {
    'use strict'
    if (noReqStateColStatusID != undefined) {
        SP.UI.Status.removeStatus(noReqStateColStatusID);
    }
    if (errReqStateColStatusID != undefined) {
        SP.UI.Status.removeStatus(errReqStateColStatusID);
    }
    var result = false;
    var formState;
    var listID = _spPageContextInfo.pageListId;
    var items = SP.ListOperation.Selection.getSelectedItems(ctx);
    var itemID = items[0].id;

    // find position of RequestStateColumn
    try 
    {
        var ReqStatePos = -1;
        $('table.ms-listviewtable').find('th').each(function () 
        {
            var _headerTitle = $(this).find('div.ms-vh-div').find('a.ms-headerSortTitleLink').html();
            if (_headerTitle == "Status wniosku") 
            {
                ReqStatePos = this.cellIndex;
            }
        });
        if (ReqStatePos > -1) 
        {
            // Get selectedRow 
            var selectedRow = $('table.ms-listviewtable').find('tr.s4-itm-selected');
            selectedRow.find('td').each(function() 
            {
                if (this.cellIndex == ReqStatePos) 
                {
                    formState = $(this).html();
                }
            });
            // Verify State 
            switch (formAction) 
            {
                case "SendForApproval":
                    if (formState == "Nowy wniosek")
                        result = true;
                    break;
                case "Approve":
                    if (formState == "Oczekiwanie na akceptacje")
                        result = true;
                    break;
                case "Reject":
                    if (formState == "Oczekiwanie na akceptacje")
                        result = true;
                    break;
                case "ConfirmRelease":
                    if (formState == "Wniosek zaakceptowany. Do potwierdzenia na recepcji")
                        result = true;
                    break;
                case "ConfirmReleaseWithoutApproval":
                    if (formState == "Oczekiwanie na akceptacje")
                        result = true;
                    break;
                case "":
                    if (formState != "Wniosek zrealizowany" && formState != "Wniosek odrzucony" && formState != "Zweryfikowane przez recepcję")
                        result = true;
                    break;
            }
        }
        else 
        {
            var msg = "System nie odnalazł kolumny 'Status dokumentu' na liście. Dodaj kolumnę do widoku i spróbuj ponownie. W razie problemów skontaktuj się z Administratorem systemu";
            noReqStateColStatusID = SP.UI.Status.addStatus(msg);
            SP.UI.Status.setStatusPriColor(noReqStateColStatusID, 'red');
            result = false;
        }
    } 
    catch (e) 
    {
        var msg = "Nie udało się zweryfikować uprawnień do akcji przepływu dla wybranego elementu. Skontaktuj się z Administratorem systemu";
        errReqStateColStatusID = SP.UI.Status.addStatus(msg);
        SP.UI.Status.setStatusPriColor(errReqStateColStatusID, 'red');
    }
    return result;
}
function PrintForm(siteUrl, _ItemId) {
    'use strict'
    var itemID = 0;
    if (_ItemId == undefined) {
        var ctx = SP.ClientContext.get_current();
        var items = SP.ListOperation.Selection.getSelectedItems(ctx);
        if (CountDictionary(items) == 1) {
            itemID = items[0].id;
        }
    }
    else {
        itemID = _ItemId;
    }
    OpenPrintPage(siteUrl + '/_Layouts/Incodix.Wedel.SPRuchMatTow/SPRuchMatTowPrintPage.aspx?UserControlFileName=SPRuchMatTowPrintForm.ascx', itemID);
}
function OpenPrintPage(url, itemID) {
    var url = url + "&isdlg=0&DocID=" + itemID;
    var windowReference = window.open(url, "Header", 'width=980,height=1100,header=no,footer=no,toolbar=no,resizable=yes,scrollbars=no,menubar=no,status=no,titlebar=no');
    if (windowReference.print) {
        var done = false;
        if (windowReference.document && windowReference.document.readyState) {
            var rs = windowReference.document.readyState;
            if ((rs === 'complete') || (rs === 'loaded')) {
                done = true;
                windowReference.print();
            }
        }
        if (!done) {
            if (windowReference.addEventListener) {
                windowReference.addEventListener('load', function () { this.print(); });
            } else {
                windowReference.onload = function () { this.print(); };
            }
        }
    }
}

// ******** Ribbon List buttons ******** //