﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Incodix.Wedel.SPRuchMatTow.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Incodix_Wedel_SPRuchMatTow {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Incodix_Wedel_SPRuchMatTow() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Incodix.Wedel.SPRuchMatTow.Resources.Incodix.Wedel.SPRuchMatTow", typeof(Incodix_Wedel_SPRuchMatTow).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Administrators.
        /// </summary>
        internal static string Administrators {
            get {
                return ResourceManager.GetString("Administrators", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Administrators Panel.
        /// </summary>
        internal static string AdministratorsPanel {
            get {
                return ResourceManager.GetString("AdministratorsPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All Requests.
        /// </summary>
        internal static string AllRequests {
            get {
                return ResourceManager.GetString("AllRequests", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approval Date.
        /// </summary>
        internal static string ApprovalDate {
            get {
                return ResourceManager.GetString("ApprovalDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approve.
        /// </summary>
        internal static string Approve {
            get {
                return ResourceManager.GetString("Approve", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to Approve.
        /// </summary>
        internal static string ApproveDesc {
            get {
                return ResourceManager.GetString("ApproveDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click &apos;Approve&apos;, to confirm Goods Release.
        /// </summary>
        internal static string ApprovePrompt {
            get {
                return ResourceManager.GetString("ApprovePrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approver.
        /// </summary>
        internal static string Approver {
            get {
                return ResourceManager.GetString("Approver", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approver.
        /// </summary>
        internal static string Approver1 {
            get {
                return ResourceManager.GetString("Approver1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approver Comments.
        /// </summary>
        internal static string ApproverComments {
            get {
                return ResourceManager.GetString("ApproverComments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approver Display Name.
        /// </summary>
        internal static string ApproverDisplayName {
            get {
                return ResourceManager.GetString("ApproverDisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Related Approver.
        /// </summary>
        internal static string ApproverLookup {
            get {
                return ResourceManager.GetString("ApproverLookup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approvers.
        /// </summary>
        internal static string Approvers {
            get {
                return ResourceManager.GetString("Approvers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Columns related to Approvers list.
        /// </summary>
        internal static string ApproversGroup {
            get {
                return ResourceManager.GetString("ApproversGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approvers List.
        /// </summary>
        internal static string ApproversList {
            get {
                return ResourceManager.GetString("ApproversList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approvers List Definition.
        /// </summary>
        internal static string ApproversListDef {
            get {
                return ResourceManager.GetString("ApproversListDef", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Definition of Approvers List.
        /// </summary>
        internal static string ApproversListDefDesc {
            get {
                return ResourceManager.GetString("ApproversListDefDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approvers Panel.
        /// </summary>
        internal static string ApproversPanel {
            get {
                return ResourceManager.GetString("ApproversPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Categories Group.
        /// </summary>
        internal static string CategoriesGroup {
            get {
                return ResourceManager.GetString("CategoriesGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Categories List.
        /// </summary>
        internal static string CategoriesList {
            get {
                return ResourceManager.GetString("CategoriesList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Categories List Definition.
        /// </summary>
        internal static string CategoriesListDef {
            get {
                return ResourceManager.GetString("CategoriesListDef", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Definition of Categories List.
        /// </summary>
        internal static string CategoriesListDefDesc {
            get {
                return ResourceManager.GetString("CategoriesListDefDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Related Categories.
        /// </summary>
        internal static string CategoriesLookup {
            get {
                return ResourceManager.GetString("CategoriesLookup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Category.
        /// </summary>
        internal static string Category {
            get {
                return ResourceManager.GetString("Category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Related Category.
        /// </summary>
        internal static string CategoryLookup {
            get {
                return ResourceManager.GetString("CategoryLookup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Closed Without Approval.
        /// </summary>
        internal static string ClosedWithoutApproval {
            get {
                return ResourceManager.GetString("ClosedWithoutApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If &apos;True&apos; form were processed without Manager approval in the system.
        /// </summary>
        internal static string ClosedWithoutApprovalDesc {
            get {
                return ResourceManager.GetString("ClosedWithoutApprovalDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ClosureDate.
        /// </summary>
        internal static string ClosureDate {
            get {
                return ResourceManager.GetString("ClosureDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Release.
        /// </summary>
        internal static string ConfirmRelease {
            get {
                return ResourceManager.GetString("ConfirmRelease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to confirm goods release.
        /// </summary>
        internal static string ConfirmReleaseDesc {
            get {
                return ResourceManager.GetString("ConfirmReleaseDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Release (without acceptance).
        /// </summary>
        internal static string ConfirmReleaseDescWithoutApproval {
            get {
                return ResourceManager.GetString("ConfirmReleaseDescWithoutApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click &apos;Approve&apos;, to confirm goods release.
        /// </summary>
        internal static string ConfirmReleasePrompt {
            get {
                return ResourceManager.GetString("ConfirmReleasePrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to confirm goods release (without acceptance).
        /// </summary>
        internal static string ConfirmReleaseWithoutApproval {
            get {
                return ResourceManager.GetString("ConfirmReleaseWithoutApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click &apos;Approve&apos;, to release goods without approval.
        /// </summary>
        internal static string ConfirmReleaseWithoutApprovalPrompt {
            get {
                return ResourceManager.GetString("ConfirmReleaseWithoutApprovalPrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirm Goods Return.
        /// </summary>
        internal static string ConfirmReturn {
            get {
                return ResourceManager.GetString("ConfirmReturn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to confirm Goods return.
        /// </summary>
        internal static string ConfirmReturnDesc {
            get {
                return ResourceManager.GetString("ConfirmReturnDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click &apos;Confirm&apos;, to confirm goods return.
        /// </summary>
        internal static string ConfirmReturnPrompt {
            get {
                return ResourceManager.GetString("ConfirmReturnPrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rok.
        /// </summary>
        internal static string CreationYear {
            get {
                return ResourceManager.GetString("CreationYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Template.
        /// </summary>
        internal static string EMailTemplate {
            get {
                return ResourceManager.GetString("EMailTemplate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Form Actions.
        /// </summary>
        internal static string FormWorkflowFlyout {
            get {
                return ResourceManager.GetString("FormWorkflowFlyout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go Back To List.
        /// </summary>
        internal static string GoBackToList {
            get {
                return ResourceManager.GetString("GoBackToList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to go back to List.
        /// </summary>
        internal static string GoBackToListDesc {
            get {
                return ResourceManager.GetString("GoBackToListDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Goods Movement.
        /// </summary>
        internal static string GoodsMovement {
            get {
                return ResourceManager.GetString("GoodsMovement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Goods Movement Form.
        /// </summary>
        internal static string GoodsMovementForm {
            get {
                return ResourceManager.GetString("GoodsMovementForm", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Form of Goods Movement Control Form.
        /// </summary>
        internal static string GoodsMovementFormDesc {
            get {
                return ResourceManager.GetString("GoodsMovementFormDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Goods Movement Repository.
        /// </summary>
        internal static string GoodsMovList {
            get {
                return ResourceManager.GetString("GoodsMovList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Goods Movement Repository Definition.
        /// </summary>
        internal static string GoodsMovListDef {
            get {
                return ResourceManager.GetString("GoodsMovListDef", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Definition of Movement Repository.
        /// </summary>
        internal static string GoodsMovListDefDesc {
            get {
                return ResourceManager.GetString("GoodsMovListDefDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Goods Return Date.
        /// </summary>
        internal static string GoodsReturnDate {
            get {
                return ResourceManager.GetString("GoodsReturnDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to More Info.
        /// </summary>
        internal static string Info {
            get {
                return ResourceManager.GetString("Info", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to open instructions library.
        /// </summary>
        internal static string InfoDesc {
            get {
                return ResourceManager.GetString("InfoDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Creation Date.
        /// </summary>
        internal static string lblCreated {
            get {
                return ResourceManager.GetString("lblCreated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to I am requesting for the following materials:.
        /// </summary>
        internal static string lblRequestedGoods {
            get {
                return ResourceManager.GetString("lblRequestedGoods", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Limited Access.
        /// </summary>
        internal static string LimitedAccess {
            get {
                return ResourceManager.GetString("LimitedAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mail Approvers.
        /// </summary>
        internal static string MailApprovers {
            get {
                return ResourceManager.GetString("MailApprovers", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Body.
        /// </summary>
        internal static string MailMessageBody {
            get {
                return ResourceManager.GetString("MailMessageBody", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mail Templates.
        /// </summary>
        internal static string MailTemplates {
            get {
                return ResourceManager.GetString("MailTemplates", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mail Templates Group.
        /// </summary>
        internal static string MailTemplatesGroup {
            get {
                return ResourceManager.GetString("MailTemplatesGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mail Templates List.
        /// </summary>
        internal static string MailTemplatesList {
            get {
                return ResourceManager.GetString("MailTemplatesList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mail templates list Definition.
        /// </summary>
        internal static string MailTemplatesListDef {
            get {
                return ResourceManager.GetString("MailTemplatesListDef", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Definition of Mail definitions list.
        /// </summary>
        internal static string MailTemplatesListDefDesc {
            get {
                return ResourceManager.GetString("MailTemplatesListDefDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Manage Permissions.
        /// </summary>
        internal static string ManageAccess {
            get {
                return ResourceManager.GetString("ManageAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Title.
        /// </summary>
        internal static string MessageTitle {
            get {
                return ResourceManager.GetString("MessageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Requests.
        /// </summary>
        internal static string MyRequests {
            get {
                return ResourceManager.GetString("MyRequests", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to open list of my Requests.
        /// </summary>
        internal static string MyRequestsDesc {
            get {
                return ResourceManager.GetString("MyRequestsDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Action canceled.
        /// </summary>
        internal static string PerformActionCancelMessage {
            get {
                return ResourceManager.GetString("PerformActionCancelMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error occured when saving dialogBox form. Contact system Administrator.
        /// </summary>
        internal static string PerformActionSaveError {
            get {
                return ResourceManager.GetString("PerformActionSaveError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Full Control.
        /// </summary>
        internal static string permAdmin {
            get {
                return ResourceManager.GetString("permAdmin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Read.
        /// </summary>
        internal static string permDisp {
            get {
                return ResourceManager.GetString("permDisp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contribute.
        /// </summary>
        internal static string permEdit {
            get {
                return ResourceManager.GetString("permEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print Form.
        /// </summary>
        internal static string PrintButton {
            get {
                return ResourceManager.GetString("PrintButton", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to open form print page.
        /// </summary>
        internal static string PrintButtonDesc {
            get {
                return ResourceManager.GetString("PrintButtonDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Purpose of Goods Movement.
        /// </summary>
        internal static string PurposeOfGoodsMovement {
            get {
                return ResourceManager.GetString("PurposeOfGoodsMovement", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Receptionist.
        /// </summary>
        internal static string Receptionist {
            get {
                return ResourceManager.GetString("Receptionist", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Receptionist Comments.
        /// </summary>
        internal static string ReceptionistComments {
            get {
                return ResourceManager.GetString("ReceptionistComments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Receptionists.
        /// </summary>
        internal static string Receptionists {
            get {
                return ResourceManager.GetString("Receptionists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Receptionists Panel.
        /// </summary>
        internal static string ReceptionistsPanel {
            get {
                return ResourceManager.GetString("ReceptionistsPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reject.
        /// </summary>
        internal static string Reject {
            get {
                return ResourceManager.GetString("Reject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to Reject.
        /// </summary>
        internal static string RejectDesc {
            get {
                return ResourceManager.GetString("RejectDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click &apos;Approve&apos;, to reject request.
        /// </summary>
        internal static string RejectPrompt {
            get {
                return ResourceManager.GetString("RejectPrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requested Goods.
        /// </summary>
        internal static string RequestedGoods {
            get {
                return ResourceManager.GetString("RequestedGoods", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request For.
        /// </summary>
        internal static string RequestFor {
            get {
                return ResourceManager.GetString("RequestFor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request For Goods.
        /// </summary>
        internal static string RequestForGoods {
            get {
                return ResourceManager.GetString("RequestForGoods", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to open new Goods Movement Request Form.
        /// </summary>
        internal static string RequestForGoodsDesc {
            get {
                return ResourceManager.GetString("RequestForGoodsDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requestors.
        /// </summary>
        internal static string Requestors {
            get {
                return ResourceManager.GetString("Requestors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requestors Panel.
        /// </summary>
        internal static string RequestorsPanel {
            get {
                return ResourceManager.GetString("RequestorsPanel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vehicle (Producer / ID).
        /// </summary>
        internal static string RequestorVehicle {
            get {
                return ResourceManager.GetString("RequestorVehicle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requests Completed.
        /// </summary>
        internal static string RequestsCompleted {
            get {
                return ResourceManager.GetString("RequestsCompleted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requests For Approval.
        /// </summary>
        internal static string RequestsForApproval {
            get {
                return ResourceManager.GetString("RequestsForApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to open Goods Movement Request Form for Approval.
        /// </summary>
        internal static string RequestsForApprovalDesc {
            get {
                return ResourceManager.GetString("RequestsForApprovalDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requests Ready To Process.
        /// </summary>
        internal static string RequestsReadyToRelease {
            get {
                return ResourceManager.GetString("RequestsReadyToRelease", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requests Rejected.
        /// </summary>
        internal static string RequestsRejected {
            get {
                return ResourceManager.GetString("RequestsRejected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request State.
        /// </summary>
        internal static string RequestState {
            get {
                return ResourceManager.GetString("RequestState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requests Without Approval.
        /// </summary>
        internal static string RequestsWithoutApproval {
            get {
                return ResourceManager.GetString("RequestsWithoutApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send For Approval.
        /// </summary>
        internal static string SendForApproval {
            get {
                return ResourceManager.GetString("SendForApproval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click to send Request for approval.
        /// </summary>
        internal static string SendForApprovalDesc {
            get {
                return ResourceManager.GetString("SendForApprovalDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click &apos;Approve&apos;, to send form for approval.
        /// </summary>
        internal static string SendForApprovalPrompt {
            get {
                return ResourceManager.GetString("SendForApprovalPrompt", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sygnatura.
        /// </summary>
        internal static string Signature {
            get {
                return ResourceManager.GetString("Signature", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ID in Access Control System.
        /// </summary>
        internal static string SKDApproverID {
            get {
                return ResourceManager.GetString("SKDApproverID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start Page Links.
        /// </summary>
        internal static string StartPageLinks {
            get {
                return ResourceManager.GetString("StartPageLinks", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to List stored welcome page Tiles.
        /// </summary>
        internal static string StartPageLinksDesc {
            get {
                return ResourceManager.GetString("StartPageLinksDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Template Name.
        /// </summary>
        internal static string TemplateName {
            get {
                return ResourceManager.GetString("TemplateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Return.
        /// </summary>
        internal static string ToReturn {
            get {
                return ResourceManager.GetString("ToReturn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Waiting for Return.
        /// </summary>
        internal static string WaitingForReturn {
            get {
                return ResourceManager.GetString("WaitingForReturn", resourceCulture);
            }
        }
    }
}
